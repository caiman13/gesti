/**
 * Created by Houlder on 18/07/2017.
 */

$(document).ready(function () {

    $('.select2').select2();

    moment.locale("fr");

    /*$('.datepicker').datetimepicker({f
        format : 'YYYY-MM-DD',
        locale: 'fr',
    });*/

    /*Preloader*/
    $(window).load(function() {
        setTimeout(function() {
            /*$('#loader-wrapper').hide();*/
            $('body').addClass('loaded')
        }, 200);
    });

    $(document).on("click",".menu",function(e){
        e.preventDefault();
        var cible = "#contenue";
        var parent = $(this).attr('data-parent');
        $('.parent').removeClass('active');
        $('.parent').removeClass('open');
        if (undefined === parent) {
        } else {
            $(parent).addClass('active');
        }

        $(".menu").removeClass("active");
        $(this).addClass("active");
//                $("#content-ress").html("");
        /*        waitingDialog.show();*/
        startLoad();
        var link = $(this).find('a');
        var url = link.attr('href');
        var texte = "";
        $.get(url,function(data){
            texte = data;
        }).done(function(){
            $(cible).html(texte);
            endLoad();
            /*            waitingDialog.hide();*/
        });
    });

    /*
     * Modal pour Modifier Mot de passe
     */
    $(document).on('click', '#edit_mdp', function(e) {
        e.preventDefault();
        var modal = '#edit_mdp';
        $(modal + '_modal_save').hide();
        $(modal + '_modal_discard').hide();
        chargement({selecteur: modal + '_modal_body', modal: true});
        var url = $(this).attr('href');
        var texte = null;
        $.get(url, function(data) {
            texte = data;
        }).done(function() {
            $(modal + '_modal_save').show();
            $(modal + '_modal_discard').show();
            $(modal + '_modal_body').html(texte);
            $(modal + '_modal').modal('show');
        });
    });

    $(document).on('click', '#edit_mdp_modal_save', function() {
        var form = $('#edit_mdp_modal_body').find('form');
        var url = form.attr('action');

        var retour = null;

        if (valideForm(form)) {
            var dataform = getdataForm(form);
            chargement({selecteur: '#edit_mdp_modal_body', modal: false});
            $.post(url, dataform, function(data) {
                retour = data;
            }).done(function() {
                if (typeof retour === 'string') {
                    $('#edit_mdp_modal_body').html(retour);
                } else {
                    $('#edit_mdp_modal').modal('hide');
                    window.location = retour.cible;
                }
            });
        }
    });

    $(document).on('click', '.onglet2', function (e) {
       e.preventDefault();
       /*$('#cli_id').trigger('change');*/
        var cible = $(this).attr('data-cible');
        $(".onglet2").removeClass("active").removeClass("selected");
        /*$(".onglet2").removeClass("selected");*/
        $(this).addClass("active").addClass("selected");
        var link = $(this).find('a');
        var url = link.attr('href')+'/'+$('#cli_id').val();

        chargement({ selecteur: cible , modal: false });
        var texte = "";
        $.get(url,function(data){
            texte = data;
        }).done(function(){
            $(cible).html(texte);
            /*            waitingDialog.hide();*/
        });
    });

    $(document).on("change","#cli_id",function () {
        $(".selected").trigger("click");
    });

    $(document).on("click",".onglet",function(e){
        e.preventDefault();
        var cible = $(this).attr('data-cible');

/*        $(cible).show();*/
        $(".onglet").removeClass("active");
        $(this).addClass("active");
//                $("#content-ress").html("");
        /*        waitingDialog.show();*/
        chargement({ selecteur: cible , modal: false })
        var link = $(this).find('a');
        var url = link.attr('href');

        var texte = "";
        $.get(url,function(data){
            texte = data;
        }).done(function(){
            $(cible).html(texte);
            /*            waitingDialog.hide();*/
        });
    });

    $(document).on("click",".detail",function(e){
        e.preventDefault();
        var cible = $(this).attr('data-cible');

        $(".cible").hide();
        $(cible).show();
//                $("#content-ress").html("");
        /*        waitingDialog.show();*/
        startLoad();
        var link = $(this).find('a');
        var url = link.attr('href');
        var texte = "";
        $.get(url,function(data){
            texte = data;
        }).done(function(){
            $(cible).html(texte);
            endLoad();
            /*            waitingDialog.hide();*/
        });
    });

    $(document).on("click",'.retour',function(){
        var cible = $(this).data('cible');

        $(cible).trigger('click');
    });


    $(document).on("click",".ajout",function(e){
        e.preventDefault();
        var titre = $(this ).attr('data-formTitle');
        var modal = $(this ).attr('data-mod');
        $(modal+"_modal_title").html(titre);
        $(modal+"_modal_save").hide();
        $(modal+"_modal_discard").hide();
        chargement({ selecteur: modal+"_modal_body" , modal: true });
        var url = $(this).data("lien");
        var texte = null;
        $.get(url,function(data){
            texte = data;
        }).done(function(){
            $(modal+"_modal_save").show();
            $(modal+"_modal_discard").show();
            $(modal+"_modal_body").html(texte);
            $(modal+"_modal").modal("show");
        });
    });

    $(document).on("click",".termirner-bc",function(e){
        e.preventDefault();
        chargement({ selecteur: "#contenue" , modal: false });
        var url = $(this).data("lien");
        var texte = null;
        $.get(url,function(data){
            texte = data;
        }).done(function(){
            $("#contenue").html(texte);
        });
    });

    $(document).on("click","#gen_modal_save",function(){
        var form = $("#gen_modal_body").find("form");
        var url = form.attr("action");

        var texte = null;

        if (valideForm( form )) {

            var dataform = getdataForm(form);

            chargement({ selecteur: "#gen_modal_body", modal: false});
            $.post(url,dataform,function(data){
                texte = data;

            }).done(function() {

                if (typeof texte === "string") {
                    $("#gen_modal_body").html(texte);
                } else {
                    $("#gen_modal").modal("hide");

                    $(texte.cible).trigger("click");
                }
            });
        }
    });

    $(document).on('change','#TiersType_type', function () {
        var value = $(this).val();
        if(value === "societe"){
            $(".type-pers").parents(".form-group").hide();
            $(".type-soc").parents(".form-group").show();
            if($(".type-pers").hasClass('field')){
                $(".type-pers").toggleClass('field');
            }
            if(!$(".util-soc").hasClass('field')){
                $(".util-soc").addClass('field')
            }
        } else {
            $(".type-soc").parents(".form-group").hide();
            $(".type-pers").parents(".form-group").show();
            if($(".util-soc").hasClass('field')){
                $(".util-soc").toggleClass('field');
            }
            if(!$(".type-pers").hasClass('field')){
                $(".type-pers").addClass('field')
            }
            $('#TiersType_numCIF').val('').trigger('blur');
        }
    });

    $(document).on('blur','#TiersType_numCIF', function () {
       if ($(this).val() === ''){
           if($('#TiersType_dateCIF').hasClass('field')){
               //language=JQuery-CSS
               $("#TiersType_dateCIF").removeClass('field').removeClass('has-error');
           }
           /*$('#TiersType_dateCIF').ad*/
       } else {
           if(!$('#TiersType_dateCIF').hasClass('field')){
               //language=JQuery-CSS
               $("#TiersType_dateCIF").addClass('field');
           }
       }
    });

    $(document).on("click",".scan",function(e){
        e.preventDefault();
        var cible = "#contenue";

        startLoad();
        var url = $(this).data("lien");
        var texte = "";
        $.get(url,function(data){
            texte = data;
        }).done(function(){
            $(cible).html(texte);
            endLoad();
            /*            waitingDialog.hide();*/
        });
    });

    $(document).on("click",".scan2",function(e){
        e.preventDefault();
        var cible = "#list-mail";

        chargement({ selecteur: cible, modal: false});
        var url = $(this).data("lien");
        var texte = "";
        $.get(url,function(data){
            texte = data;
        }).done(function(){
            $(cible).html(texte);
            endLoad();
            /*            waitingDialog.hide();*/
        });
    });

    $(document).on('change','#facture_idLi', function () {
        var url = $(this).attr('data-url');
        if($(this).val() === '999999999'){
            $.get(url,function (data) {
                texte=data;
            }).done(function () {
                $('#cli_modal_body').html(texte);
                $('#cli_modal').modal('show');
            });
        }
    });

    $(document).on("click","#cli_modal_save",function(){
        var form = $("#cli_modal_body").find("form");
        var url = form.attr("action");

        if (valideForm( form )) {

            var dataform = getdataForm(form);

            chargement({ selecteur: "#cli_modal_body", modal: false});
            $.post(url,dataform,function(data){
                texte = data;
            }).done(function() {
                $("#cli_modal").modal("hide");

                $('#facture_idLi').html(texte);
            });
        }
    });

    $(document).on('change','#vente_fees', function () {
        var data_url = $(this).attr('data-url');
        var url = data_url.slice(1,-1)+$(this).val();
        var texte = '';
        $.get(url,function (data) {
            texte=data;
        }).done(function () {
            $('#vente_classes').html(texte);
        });
    });

    $(document).on('change', '#vente_typeBillet', function () {
        var value = $(this).val();
        if(value === "normal"){
            $("#vente_autreFees").parents(".form-group").hide();
            if($("#vente_autreFees").hasClass('field')){
                $("#vente_autreFees").removeClass('field').val('0');
            }
        } else {
            $("#vente_autreFees").parents(".form-group").show();
            if(!$("#vente_autreFees").hasClass('field')){
                $("#vente_autreFees").addClass('field').val('');
            }
        }
    });

    $(document).on('change','.fee-change', function () {
        var data_url = $(this).attr('data-url');
        var url = data_url + '/' + $(this).val();
        var ids = $(this).attr('id').split("_");
        ids[4] = 'classes';
        var cible = ids.join("_");
        var texte = '';
        $.get(url,function (data) {
            texte=data;
        }).done(function () {
            $('#'+cible).html(texte);
        });
    });

    $(document).on('click','#save_vente', function () {
        var form = $("#list-mail").find("form");
        var url = form.attr("action");
        var texte = '';
        if(valideForm(form)){

            var dataform = getFormData(form);

            $.ajax({
                url : url,
                type : 'POST',
                data : dataform,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success : function(data) {
                    $("#contenue").html(data);
                }
            });

        }
    });


    $(document).on('click','#valid_scan', function (e) {
        e.preventDefault();
        var form = $("#form_scan").find("form");
        var url = form.attr("action");
        var texte = '';
        if(valideForm(form)){

            var dataform = getFormData(form);

            dataform.append($('#scan_file').attr('name'),$('#scan_file')[0].files[0]);

            $.ajax({
                url : url,
                type : 'POST',
                data : dataform,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success : function(data) {
                    $("#contenue").html(data);
                },
                statusCode : {
                    500: function() {
                        $.notify({
                            // options
                            message: 'le format du pdf n\'est pas compatible, veuillez saisir manuelement!'
                        },{
                            // settings
                            type: 'danger'
                        });
                    }
                }
                /*function () {
                    */
            });
        }

    });


    $(document).on('click','.show_vente', function (e) {
        e.preventDefault();
        var url = $(this).data("lien");
        var texte = null;
        chargement({ selecteur: '#liste-vente' , modal: false });
        $.get(url,function (data) {
            texte=data;
        }).done(function () {
            $('#liste-vente').html(texte);
        });
    });

    $(document).on('click','.show_bc', function (e) {
        e.preventDefault();
        var url = $(this).data("lien");
        var texte = null;
        chargement({ selecteur: '#liste-bc' , modal: false });
        $.get(url,function (data) {
            texte=data;
        }).done(function () {
            $('#liste-bc').html(texte);
        });
    });

    $(document).on('click','.show-fact', function (e) {
        e.preventDefault();
        var url = $(this).data("lien");
        var texte = null;
        chargement({ selecteur: '#liste-facture' , modal: false });
        $.get(url,function (data) {
            texte=data;
        }).done(function () {
            $('#liste-facture').html(texte);
        });
    });

    $(document).on('click','.show-pay', function (e) {
        e.preventDefault();
        var url = $(this).data("lien");
        var texte = null;
        chargement({ selecteur: '#liste-paiement' , modal: false });
        $.get(url,function (data) {
            texte=data;
        }).done(function () {
            $('#liste-paiement').html(texte);
        });
    });

    $(document).on("click","#gen_md_modal_save",function(){
        var form = $("#gen_md_modal_body").find("form");
        var url = form.attr("action");
        var texte = null;
        if (valideForm( form )) {
            var dataform = getdataForm(form);

            $("#gen_md_modal_save").hide();
            $("#gen_md_modal_discard").hide();
            chargement({ selecteur: "#gen_md_modal_body", modal: false});
            $.post(url,dataform,function(data){
                texte = data;

            }).done(function(){
                $("#gen_md_modal_save").show();
                $("#gen_md_modal_discard").show();
                if (typeof texte === "string") {
                    $("#gen_md_modal_body").html(texte);
                } else {
                    $("#gen_md_modal").modal("hide");
                    $(texte.cible).trigger("click");
                }
            });
        }
    });


    $(document).on("change","#periode",function () {
        if($(this).val() === "perso"){
            $("#date-perso").show();
            /*$("#Debut").on("dp.change", function (e) {
                $('#Fin').data("DateTimePicker").minDate(e.date);
            });
            $("#Fin").on("dp.change", function (e) {
                $('#Debut').data("DateTimePicker").maxDate(e.date);
            });*/
        } else {
            $("#date-perso").hide();
            $("#cli_id").trigger("change");
        }
    });

    $(document).on("change", "#mod_id", function () {
        $("#cli_id").trigger("change");
    });

    $(document).on("change","#periode4",function () {
        if($(this).val() === "perso"){
            $("#date-perso2").show();

        } else {
            $("#date-perso2").hide();
            var url = $('#path_journal').val();
            var date = dateRetourner($(this).val());
            url = url + '/' + date.debut + '/' + date.fin;

            $.get(url, function (data) {
                $('#liste-journal').html(data);
            });
        }
    });

    $(document).on("change","#periode5",function () {
        if($(this).val() === "perso"){
            $("#date-perso").show();

        } else {
            $("#date-perso").hide();
            var url = $(this).data('path');
            var cible = $(this).data('cible');

            var date = dateRetourner($(this).val());
            url = url + '/' + date.debut + '/' + date.fin;
            $.get(url, function (data) {
                $(cible).html(data);
            });
        }
    });

    $(document).on("click","#findByPersoTDB",function () {
        var url = $(this).data('path');
        var cible = $(this).data('cible');
        var date = dateRetourner('perso');
        url = url + '/' + date.debut + '/' + date.fin;
        $.get(url, function (data) {
            $(cible).html(data);
        });
    });

    $(document).on("click","#findByPersoCompta",function () {
        var url = $('#path_journal').val();
        var date = dateRetourner('perso');
        url = url + '/' + date.debut + '/' + date.fin;

        $.get(url, function (data) {
            $('#liste-journal').html(data);
        });
    });

    $(document).on("click","#findByPerso",function () {
        $("#cli_id").trigger("change");
    });

    $(document).on("change","#periode2",function () {
        if($(this).val() === "perso"){
            $("#date-perso").show();
            /*$("#Debut").on("dp.change", function (e) {
             $('#Fin').data("DateTimePicker").minDate(e.date);
             });
             $("#Fin").on("dp.change", function (e) {
             $('#Debut').data("DateTimePicker").maxDate(e.date);
             });*/
        } else {
            $("#date-perso").hide();
            $(".selected").trigger("click");
        }
    });

    $(document).on("click","#findByPerso2",function () {
        $(".selected").trigger("click");
    });


    $(document).on('click', '.onglet4', function (e) {
        e.preventDefault();
        var date = dateRetourner($('#periode2').val());

        /*$('#cli_id').trigger('change');*/
        var cible = $(this).attr('data-cible');
        $(".onglet4").removeClass("active").removeClass("selected");
        /*$(".onglet2").removeClass("selected");*/
        $(this).addClass("active").addClass("selected");
        var link = $(this).find('a');
        var url = link.attr('href')+"/"+date.debut+"/"+date.fin;

        chargement({ selecteur: cible , modal: false });
        var texte = "";
        $.get(url,function(data){
            texte = data;
        }).done(function(){
            $(cible).html(texte);
        });
    });

    $(document).on('click', '.onglet3', function (e) {
        e.preventDefault();
        var date = dateRetourner($('#periode').val());

        /*$('#cli_id').trigger('change');*/
        var cible = $(this).attr('data-cible');
        $(".onglet3").removeClass("active").removeClass("selected");
        /*$(".onglet2").removeClass("selected");*/
        $(this).addClass("active").addClass("selected");
        var link = $(this).find('a');
        var url = link.attr('href')+"/"+$('#cli_id').val()+"/"+date.debut+"/"+date.fin;

        chargement({ selecteur: cible , modal: false });
        var texte = "";
        $.get(url,function(data){
            texte = data;
        }).done(function(){
            $(cible).html(texte);
        });
    });

    $(document).on('click', '.onglet8', function (e) {
        e.preventDefault();
        var date = dateRetourner($('#periode').val());
        console.log(date);
        /*$('#cli_id').trigger('change');*/
        var cible = $(this).attr('data-cible');
        $(".onglet8").removeClass("active").removeClass("selected");
        /*$(".onglet2").removeClass("selected");*/
        $(this).addClass("active").addClass("selected");
        var link = $(this).find('a');
        var url = link.attr('href')+"/"+$('#cli_id').val()+"/"+date.debut+"/"+date.fin+"/"+$('#mod_id').val();

        chargement({ selecteur: cible , modal: false });
        var texte = "";
        $.get(url,function(data){
            texte = data;
        }).done(function(){
            $(cible).html(texte);
        });
    });

    $(document).on('click','#fact_vent',function () {
        var ventes = [];
        $('.vente-case:checked').each(function (i) {
            ventes.push($(this).val());
        });
        if(ventes.length > 0){
            console.log(ventes);
            var donnee = { 'idVentes' : ventes};
            $("#facture_modal_save").show();
            chargement({ selecteur: "#facture_modal_body" , modal: false });
            $.post($(this).attr('data-lien'),donnee, function (data) {

                $("#facture_modal").modal("show");
                $("#facture_modal_body").html(data);
                $("#facture_modal_discard").show();
            });
        } else {
            alert("Veuillze cochez d'abord un vente à facturer");
        }

    });

    $(document).on('click','#group_valid',function () {
        var paiements = [];
        $('.paiement-case:checked').each(function (i) {
            paiements.push($(this).val());
        });
        if(paiements.length > 0){
            console.log(paiements);
            var donnee = { 'idPays' : paiements};
            $("#facture_modal_save").show();
            chargement({ selecteur: "#facture_modal_body" , modal: false });
            $.post($(this).attr('data-lien'),donnee, function (data) {

                $("#facture_modal").modal("show");
                $("#facture_modal_body").html(data);
                $("#facture_modal_discard").show();
            });
        } else {
            alert("Veuillze cochez d'abord un paiement à valider");
        }

    });

    $(document).on('click','#group_paiement',function () {
        let factures = [];
        $('.facture-case:checked').each(function (i) {
            factures.push($(this).val());
        });
        if(factures.length > 0){
            $("#pay_modal_discard").show();
            $("#pay_modal_save").show();
            $("#pay_modal").modal("show");
            chargement({ selecteur: "#pay_modal_body" , modal: false });
            const donnee = { 'idFactures' : factures};

            let dataReturn = null;

            $.post($(this).attr('data-lien'),donnee, function (data) {
                dataReturn = data;
            }).done(function() {
                $('#pay_modal_body').html(dataReturn);
            });
        } else {
            alert("Veuillze cochez d'abord une facture à payer");
        }

    });

    $(document).on('click',"#facture_modal_save", function () {
        var form = $("#facture_modal_body").find("form");
        var url = form.attr("action");

        if (valideForm( form )) {
            console.log("valide");
            var dataform = getdataForm(form);
            console.log(dataform);
            $("#facture_modal_save").hide();
            $("#facture_modal_discard").hide();
            chargement({ selecteur: "#facture_modal_body", modal: false});
            $.post(url,dataform,function(data){
                texte = data;
            }).done(function () {
                $("#facture_modal_save").show();
                $("#facture_modal_discard").show();
                if (typeof texte === "string") {
                    $("#facture_modal_body").html(texte);
                } else {
                    $("#facture_modal").modal("hide");
                    $('.modal-backdrop').remove();
                    /*alert(texte.cible);*/
                    $(texte.cible).trigger("click");
                }
            });
        }
    });

    $('.my-file').ace_file_input({
        no_file:'Pas de fichier choisi ...',
        btn_choose:'Choisir',
        btn_change:'Changer',
        droppable:false,
        onchange:null,
        thumbnail:false //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        //blacklist:'exe|php'
        //onchange:''
        //
    });

    $(document).on('change','#upload_modif', function (e) {
        // console.log($(this));
        var files = $(this)[0].files;

        if (files.length > 0) {
            // On part du principe qu'il n'y qu'un seul fichier
            // étant donné que l'on a pas renseigné l'attribut "multiple"
            var file = files[0],
                $image_preview = $('#preview-image');

            // Ici on injecte les informations recoltées sur le fichier pour l'utilisateur
            $image_preview.find('img').attr('src', window.URL.createObjectURL(file));

        }
    });

    $(document).on('click',"#logo_modal_save", function () {
        var form = $("#logo_modal_body").find("form");
        var url = form.attr("action");

        if(valideForm(form)){
            /*var dataform = new FormData();
             dataform.append($('#vente_scan').attr('name'),)*/
            var dataform = getFormData(form);
            /*dataform[$('#vente_scan').attr('name')] = $('#vente_scan')[0].files[0];*/
            dataform.append('laza','Houlder');
            dataform.append($('#upload_modif').attr('name'),$('#upload_modif')[0].files[0]);
            console.log(dataform);
            $.ajax({
                url : url,
                type : 'POST',
                data : dataform,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success : function(data) {
                    $("#logo_modal_save").show();
                    $("#logo_modal_discard").show();
                    if (typeof data === "string") {
                        $("#logo_modal_body").html(data);
                    } else {
                        $("#logo_modal").modal("hide");
                        $(data.cible).trigger("click");
                    }
                }
            });
        }
    });

    $(document).on('click',"#pj_modal_save", function () {
        var form = $("#pj_modal_body").find("form");
        var url = form.attr("action");

        if(valideForm(form)){
            /*var dataform = new FormData();
             dataform.append($('#vente_scan').attr('name'),)*/
            var dataform = getFormData(form);
            /*dataform[$('#vente_scan').attr('name')] = $('#vente_scan')[0].files[0];*/
            dataform.append('laza','Houlder');
            dataform.append($('#upload_piece').attr('name'),$('#upload_piece')[0].files[0]);
            console.log(dataform);
            $("#pj_modal_save").hide();
            $("#pj_modal_discard").hide();
            chargement({ selecteur: '#pj_modal_body' , modal: false });
            $.ajax({
                url : url,
                type : 'POST',
                data : dataform,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success : function(data) {
                    $("#pj_modal_save").show();
                    $("#pj_modal_discard").show();
                    if (typeof data === "string") {
                        $("#pj_modal_body").html(data);
                    } else {
                        $("#pj_modal").modal("hide");
                        $(data.cible).trigger("click");
                    }
                }
            });
        }
    });

    $(document).on('click',"#pay_modal_save", function () {
        var form = $("#pay_modal_body").find("form");
        var url = form.attr("action");

        if(valideForm(form)){
            /*var dataform = new FormData();
             dataform.append($('#vente_scan').attr('name'),)*/
            var dataform = getFormData(form);
            /*dataform[$('#vente_scan').attr('name')] = $('#vente_scan')[0].files[0];*/
            /*dataform.append('laza','Houlder');*/
            console.log($('#PaiementType_fileScan')[0].files[0]);
            dataform.append($('#PaiementType_fileScan').attr('name'),$('#PaiementType_fileScan')[0].files[0]);
            console.log(dataform);
            //$("#pay_modal_save").hide();
            //$("#pay_modal_discard").hide();
            chargement({ selecteur: '#pay_modal_body' , modal: false });
            $.ajax({
                url : url,
                type : 'POST',
                data : dataform,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success : function(data) {
                    $("#pay_modal_save").show();
                    $("#pay_modal_discard").show();
                    if (typeof data === "string") {
                        $("#pay_modal_body").html(data);
                    } else {
                        $("#pay_modal").modal("hide");
                        $(data.cible).trigger("click");
                    }
                }
            });
        }
    });

    $(document).on('click',"#bc_modal_save", function () {
        var form = $("#bc_modal_body").find("form");
        var cible = $(this).attr("data-cible");
        var url = form.attr("action");

        if(valideForm(form)){
            var formdata = getdataForm(form);
            console.log(formdata);
            var texte = '';
            $.post(url,formdata,function (data) {
                texte = data;
            }).done(function () {
                $("#bc_modal").modal("hide");
                $(cible).html(texte);
            });
        }
    });

    $(document).on('click',"#terminer_BC", function () {
        var form = $("#contenue").find("form");
        var url = form.attr("action");

        if(valideForm(form)){
            var formdata = getdataForm(form);
            console.log(formdata);
            var texte = '';
            $.post(url,formdata,function (data) {
                texte = data;
            }).done(function () {
                $("#bc").trigger("click");
            });
        }
    });

    $(document).on('click',"#autre_billet", function () {
        var form = $("#contenue").find("form");
        var url = form.attr("action");

        if(valideForm(form)){
            var formdata = getdataForm(form);
            console.log(formdata);
            var texte = '';
            $.post(url,formdata,function (data) {
                texte = data;
            }).done(function () {
                $("#contenue").html(texte);
            });
        }
    });
});

function startLoad() {
    $('body').removeClass('loaded');
}

function endLoad() {
    $('body').addClass('loaded');
}

function dateRetourner(valPeriode) {
    var start,end;

    if(valPeriode === "today"){
        start = moment();
        end = moment();
    } else if(valPeriode === "yesterday"){
        start = moment().subtract(1, 'days');
        end = moment().subtract(1, 'days');
    } else if(valPeriode === "week"){
        start = moment().startOf('week');
        end = moment().endOf('week');
    } else if(valPeriode === "last_week"){
        start = moment().subtract(1, 'week').startOf('week');
        end = moment().subtract(1, 'week').endOf('week');
    } else if(valPeriode === "month"){
        start = moment().startOf('month');
        end = moment().endOf('month');
    } else if(valPeriode === "last_month"){
        start = moment().subtract(1, 'month').startOf('month');
        end = moment().subtract(1, 'month').endOf('month');
    } else {
        start = moment($("#Debut").val());
        end = moment($("#Fin").val());
    }

    var dataForm = {};
    /*$('#reportrange span').html(start.format('YYYY-MM-DD') + ' | ' + end.format('YYYY-MM-DD'));*/
    dataForm["debut"] = start.format('YYYY-MM-DD');
    dataForm["fin"] = end.format('YYYY-MM-DD');
    return dataForm;
}

function dateRetourner2(valPeriode) {
    var returnDate = moment();

    if(valPeriode === "today"){
        returnDate = moment();
    } else if(valPeriode === "yesterday"){
        returnDate = moment().subtract(1, 'days');
    }

    var returnValue;
    /*$('#reportrange span').html(start.format('YYYY-MM-DD') + ' | ' + end.format('YYYY-MM-DD'));*/
    returnValue = returnDate.format('YYYY-MM-DD');
    return returnValue;
}