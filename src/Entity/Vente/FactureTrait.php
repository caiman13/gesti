<?php
/**
 * Created by PhpStorm.
 * User: houlder
 * Date: 3/14/19
 * Time: 4:34 PM
 */

namespace App\Entity\Vente;


use App\Entity\Admin\LigneCompta;
use App\Entity\Admin\Tiers;
use App\Library\AsLetter;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;

trait FactureTrait
{

    /**
     * @return mixed
     */
    public function getMontantNote()
    {
        return $this->getTotalPaiement() - $this->getTotalMontant();
    }

    /**
     * @return array
     */
    public function getVenteRemboursable()
    {
        $ventesReturn = array();
        /** @var Vente $item */
        foreach ($this->ventes as $item){
            if ($item->isRemboursable()){
                $ventesReturn[] = $item;
            }
        }
        return $ventesReturn;
    }

    /**
     * @return int
     */
    public function getRemboursable()
    {
        $montant = 0;

        /** @var ArrayCollection $lesVentes */
        $lesVentes = $this->getVentes();

        foreach ($lesVentes->getIterator() as $item){
            if ($item->isRemboursable()){
                $montant = $montant + $item->getTotalTarifBC();
            }
        }
        return $montant;
    }

    /**
     * @param bool $commission
     *
     * @return int
     */
    public function getTotalPaiement(bool $commission = true)
    {
        $montant = 0;
        $payements = $this->getPaiementFactures();
        /** @var PaiementFacture $pay */
        foreach ($payements as $pay){
            if ($commission) {
                $montant = $montant + (int)$pay->getTotalPaiement();
            } else {
                $montant = $montant + (int)$pay->getMontant();
            }
        }

        return (float) $montant;
    }

    public function getTotalMontant()
    {
        return (int)$this->getMontantTransport()
            +(int)$this->getMontantTaxe()
            +(int)$this->getMontantFees()
            +(int)$this->getMontantTva()
            + $this->getAutreProd();
    }

    public function getSolde($commission = true)
    {
        return $this->getTotalMontant() - $this->getTotalPaiement($commission);
    }

    /**
     *
     * @return string
     *
     */
    public function getLetter()
    {
        $text = AsLetter::asLetters($this->getTotalMontant());
        $text_explode = explode(' ', $text);
        if ($text_explode[0] == 'millions'){
            $text = 'un '.$text;
        }
        return $text;
    }

    /**
     * Check if facture totalement payé
     */
    public function checKSolde()
    {
        if ($this->getSolde() <= 0){
            $this->setPayed(true);
        }
    }
}
