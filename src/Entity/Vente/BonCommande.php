<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 12/09/2017
 * Time: 12:35
 */

namespace App\Entity\Vente;

use App\Entity\Admin\Currency;
use App\Entity\Admin\Tiers;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * Class BonCommande
 * @package App\Entity\Vente
 * @ORM\Table(name="bonCommande")
 * @ORM\Entity(repositoryClass="App\Repository\Vente\BonCommandeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class BonCommande
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", nullable=true)
     */
    private $reference;

    /**
     * @var string
     * @ORM\Column(name="pnr", type="string", nullable=true)
     */
    private $pnr;

    /**
     * @var string
     * @ORM\Column(name="pnr_propre", type="string", nullable=true)
     */
    private $pnrPropre;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Vente\Vente", mappedBy="bc", cascade={"persist","remove",})
     */
    private $ventes;

    /**
     * @var Tiers
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Tiers")
     * @ORM\JoinColumn(name="fournissuer_id", referencedColumnName="id")
     */
    private $tier;

    /**
     * @var string
     * @ORM\Column(name="montant_transport", type="string", nullable=true)
     */
    private $montantTransport;

    /**
     * @var string
     * @ORM\Column(name="montant_taxe", type="string", nullable=true)
     */
    private $montantTaxe;

    /**
     * @var Currency
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Currency")
     * @ORM\JoinColumn(name="cur_iso", referencedColumnName="code_iso")
     */
    private $currency;

    /**
     * @var string
     * @ORM\Column(name="fee_agv", type="string", nullable=true)
     */
    private $feeAGV;

    /**
     * @var string
     * @ORM\Column(name="etat", type="string")
     */
    private $etat = 'incomplet';

    /**
     * @var DateTime
     * @ORM\Column(name="date_bc", type="date")
     */
    private $dateBC;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ventes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return BonCommande
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Add venteuse Doctrine\Common\Collections\Collection;
     *
     * @param Vente $vente
     *
     * @return BonCommande
     */
    public function addVente(Vente $vente)
    {
        $this->ventes[] = $vente;

        return $this;
    }

    /**
     * Remove vente
     *
     * @param Vente $vente
     */
    public function removeVente(Vente $vente)
    {
        $this->ventes->removeElement($vente);
    }

    /**
     * Get ventes
     *
     * @return Vente[]|Collection
     */
    public function getVentes()
    {
        return $this->ventes;
    }

    /**
     * Set montantTransport
     *
     * @param string $montantTransport
     *
     * @return BonCommande
     */
    public function setMontantTransport($montantTransport)
    {
        $this->montantTransport = $montantTransport;

        return $this;
    }

    /**
     * Get montantTransport
     *
     * @return string
     */
    public function getMontantTransport()
    {
        return $this->montantTransport;
    }

    /**
     * Set montantTaxe
     *
     * @param string $montantTaxe
     *
     * @return BonCommande
     */
    public function setMontantTaxe($montantTaxe)
    {
        $this->montantTaxe = $montantTaxe;

        return $this;
    }

    /**
     * Get montantTaxe
     *
     * @return string
     */
    public function getMontantTaxe()
    {
        return $this->montantTaxe;
    }

    /**
     * Set feeAGV
     *
     * @param string $feeAGV
     *
     * @return BonCommande
     */
    public function setFeeAGV($feeAGV)
    {
        $this->feeAGV = $feeAGV;

        return $this;
    }

    /**
     * Get feeAGV
     *
     * @return string
     */
    public function getFeeAGV()
    {
        return $this->feeAGV;
    }

    /**
     * Set tier
     *
     * @param Tiers $tier
     *
     * @return BonCommande
     */
    public function setTier(Tiers $tier = null)
    {
        $this->tier = $tier;

        return $this;
    }

    /**
     * Get tier
     *
     * @return Tiers
     */
    public function getTier()
    {
        return $this->tier;
    }

    /**
     * Set currency
     *
     * @param Currency $currency
     *
     * @return BonCommande
     */
    public function setCurrency(Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }


    /**
     * @return DateTime
     */
    public function getDateBC()
    {
        return $this->dateBC;
    }

    /**
     * @param DateTime $dateBC
     * @return BonCommande
     */
    public function setDateBC($dateBC)
    {
        $this->dateBC = $dateBC;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @throws Exception
     */
    public function onSaveDateBC()
    {
        $this->dateBC = new DateTime();
    }

    /**
     * @return string
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param string $etat
     * @return BonCommande
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
        return $this;
    }


    /**
     * Set pnruse Doctrine\Common\Collections\Collection;
     *
     * @param string $pnr
     *
     * @return BonCommande
     */
    public function setPnr($pnr)
    {
        $this->pnr = $pnr;

        return $this;
    }

    /**
     * Get pnr
     *
     * @return string
     */
    public function getPnr()
    {
        return $this->pnr;
    }

    /**
     * Set pnrPropre
     *
     * @param string $pnrPropre
     *
     * @return BonCommande
     */
    public function setPnrPropre($pnrPropre)
    {
        $this->pnrPropre = $pnrPropre;

        return $this;
    }

    /**
     * Get pnrPropre
     *
     * @return string
     */
    public function getPnrPropre()
    {
        return $this->pnrPropre;
    }
}
