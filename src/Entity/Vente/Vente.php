<?php

namespace App\Entity\Vente;

use App\Entity\Admin\Classes;
use App\Entity\Admin\Currency;
use App\Entity\Admin\Fees;
use App\Entity\Admin\Tiers;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * Vente
 *
 * @ORM\Table(name="vente")
 * @ORM\Entity(repositoryClass="App\Repository\Vente\VenteRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Vente
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="num_billet", type="string", length=255 , nullable=true)
     */
    private $numBillet;

    /**
     * @var string
     *
     * @ORM\Column(name="book_ref", type="string", length=255, nullable=true)
     */
    private $bookRef;

    /**
     * @var string
     *
     * @ORM\Column(name="passager", type="string")
     */
    private $passager;

    /**
     * @var int
     *
     * @ORM\Column(name="montant_transport", type="integer", nullable=true)
     */
    private $montantTransport;

    /**
     * @var int
     *
     * @ORM\Column(name="taxe", type="integer" , nullable=true)
     */
    private $taxe;



    /**
     * @var Currency
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code_iso")
     */
    private $currency;

    /**
     * @var Fees
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Fees")
     * @ORM\JoinColumn(name="fees_id", referencedColumnName="id")
     */
    private $fees;

    /**
     * @var Classes
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Classes")
     * @ORM\JoinColumn(name="classes_id", referencedColumnName="id")
     */
    private $classes;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Vente\Parcour", cascade={"persist","remove"})
     * @ORM\JoinTable(name="vente_parcour",
     *      joinColumns={@ORM\JoinColumn(name="vente_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="parcour_id", referencedColumnName="id", unique=false, onDelete="CASCADE")}
     *      )
     */
    private $parcours;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Vente\PlusVente", cascade={"persist","remove"})
     * @ORM\JoinTable(name="vente_plus",
     *      joinColumns={@ORM\JoinColumn(name="vente_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="plus_vente_id", referencedColumnName="id", unique=false, onDelete="CASCADE")}
     *      )
     */
    private $lesPlus;


    /**
     * @var float
     * @ORM\Column(name="remboursement", type="float")
     */
    private $remboursement = 0;


    private $file_scan;

    /**
     * @var Facture
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Vente\Facture", inversedBy="ventes")
     * @ORM\JoinColumn(name="facture_id", referencedColumnName="id")
     */
    private $facture;

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string")
     */
    private $etat = "attente";

    /**
     * @var bool
     * @ORM\Column(name="reglee", type="boolean")
     */
    private $reglee = false;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=TRUE)
     */
    private $type;

    /**
     * @var bool
     * @ORM\Column(name="remboursable", type="boolean")
     */
    private $remboursable = false;

    /**
     * @var string
     * @ORM\Column(name="type_billet", type="string")
     */
    private $typeBillet = 'normal';

    /**
     * @var string
     *
     * @ORM\Column(name="type_vente", type="string")
     */
    private $typeVente = 'normal';

    /**
     * @var int
     *
     * @ORM\Column(name="autre_fees", type="float")
     */
    private $autreFees = 0;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_vente", type="date")
     */
    private $dateVente;

    /**
     * @var Tiers
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Tiers")
     * @ORM\JoinColumn(name="cie_id", referencedColumnName="id")
     */
    private $compagieAerienne;

    /**
     * @var float
     * @ORM\Column(name="fees_agv", type="float")
     */
    private $feesAGV = 0;

    /**
     * @var BonCommande
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Vente\BonCommande", inversedBy="ventes")
     * @ORM\JoinColumn(name="bc_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $bc;

    /**
     * @var bool
     * @ORM\Column(name="prevente", type="boolean")
     */
    private $prevente = false;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numBillet
     *
     * @param string $numBillet
     *
     * @return Vente
     */
    public function setNumBillet($numBillet)
    {
        $this->numBillet = $numBillet;

        return $this;
    }

    /**
     * Get numBillet
     *
     * @return string
     */
    public function getNumBillet()
    {
        return $this->numBillet;
    }

    /**
     * Set montantTransport
     *
     * @param int $montantTransport
     *
     * @return Vente
     */
    public function setMontantTransport($montantTransport)
    {
        $this->montantTransport = $montantTransport;

        return $this;
    }

    /**
     * Get montantTransport
     *
     * @return int
     */
    public function getMontantTransport()
    {
        /*$montant = $this->montantTransport + (int)$this->feesAGV;
        dump($this->montantTransport);
        dump($this->feesAGV);
        dump($montant);
        die;*/
        return $this->montantTransport;
    }

    public function getTotalMontantTransport()
    {
        return $this->montantTransport + (int)$this->feesAGV;
    }

    /**
     * Set taxe
     *
     * @param int $taxe
     *
     * @return Vente
     */
    public function setTaxe($taxe)
    {
        $this->taxe = $taxe;

        return $this;
    }

    /**
     * Get taxe
     *
     * @return int
     */
    public function getTaxe()
    {
        return $this->taxe;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->parcours = new ArrayCollection();
        $this->lesPlus = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getPassager()
    {
        return $this->passager;
    }

    /**
     * @param string $passager
     * @return Vente
     */
    public function setPassager($passager)
    {
        $this->passager = $passager;
        return $this;
    }


    /**
     * Add parcour
     *
     * @param Parcour $parcour
     *
     * @return Vente
     */
    public function addParcour(Parcour $parcour)
    {
        $this->parcours[] = $parcour;

        return $this;
    }

    /**
     * Remove parcour
     *
     * @param Parcour $parcour
     */
    public function removeParcour(Parcour $parcour)
    {
        $this->parcours->removeElement($parcour);
    }

    /**
     * Get parcours
     *
     * @return Collection
     */
    public function getParcours()
    {
        return $this->parcours;
    }

    /**
     * @return string
     */
    public function getBookRef()
    {
        return $this->bookRef;
    }

    /**
     * @param string $bookRef
     * @return Vente
     */
    public function setBookRef($bookRef)
    {
        $this->bookRef = $bookRef;
        return $this;
    }



    /**
     * Set fees
     *
     * @param Fees $fees
     *
     * @return Vente
     */
    public function setFees(Fees $fees = null)
    {
        $this->fees = $fees;

        return $this;
    }

    /**
     * Get fees
     *
     * @return Fees
     */
    public function getFees()
    {
        return $this->fees;
    }

    /**
     * Set classes
     *
     * @param Classes $classes
     *
     * @return Vente
     */
    public function setClasses(Classes $classes = null)
    {
        $this->classes = $classes;

        return $this;
    }

    /**
     * Get classes
     *
     * @return Classes
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Vente
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * @return mixed
     */
    public function getFileScan()
    {
        return $this->file_scan;
    }

    /**
     * @param mixed $file_scan
     */
    public function setFileScan($file_scan)
    {
        $this->file_scan = $file_scan;
    }

    /**
     * @return string
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param string $etat
     * @return Vente
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
        return $this;
    }


    /**
     * Set currency
     *
     * @param Currency $currency
     *
     * @return Vente
     */
    public function setCurrency(Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    public function getVoyageur(){
        return $this->getParcours()->get(0)->getVoyageurName();
    }

    /**
     * @return DateTime
     */
    public function getDateVente()
    {
        return $this->dateVente;
    }

    /**
     * @param DateTime $dateVente
     *
     * @return Vente
     */
    public function setDateVente($dateVente)
    {
        $this->dateVente = $dateVente;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @throws Exception
     */
    public function makeDateVente()
    {
        $this->setDateVente(new DateTime('now'));
    }

    /**
     * Set facture
     *
     * @param Facture $facture
     *
     * @return Vente
     */
    public function setFacture(Facture $facture = null)
    {
        $this->facture = $facture;

        return $this;
    }

    /**
     * Get facture
     *
     * @return Facture
     */
    public function getFacture()
    {
        return $this->facture;
    }

    public function getFeesTotal()
    {
        if ($this->getTypeBillet() == 'normal'){
            $montant_fee = ($this->getType() == 'simple')? $this->getClasses()->getSimple() : $this->getClasses()->getAllerRetour();
        } else {
            $montant_fee = $this->getAutreFees();
        }
        //dump(($this->getType() == 'simple')? $this->getClasses()->getSimple() : $this->getClasses()->getAllerRetour() + $this->getFeesAGV());die;
        return $montant_fee;
        //return ($this->getType() == 'simple')? $this->getClasses()->getSimple() : $this->getClasses()->getAllerRetour() + $this->getFeesAGV();
    }


    /**
     * Set feesAGV
     *
     * @param float $feesAGV
     *
     * @return Vente
     */
    public function setFeesAGV($feesAGV)
    {
        $this->feesAGV = $feesAGV;

        return $this;
    }

    /**
     * Get feesAGV
     *
     * @return float
     */
    public function getFeesAGV()
    {
        return $this->feesAGV;
    }

    /**
     * Set compagieAerienne
     *
     * @param Tiers $compagieAerienne
     *
     * @return Vente
     */
    public function setCompagieAerienne(Tiers $compagieAerienne = null)
    {
        $this->compagieAerienne = $compagieAerienne;

        return $this;
    }

    /**
     * Get compagieAerienne
     *
     * @return Tiers
     */
    public function getCompagieAerienne()
    {
        return $this->compagieAerienne;
    }

    /**
     * Set bc
     *
     * @param BonCommande $bc
     *
     * @return Vente
     */
    public function setBc(BonCommande $bc = null)
    {
        $this->bc = $bc;

        return $this;
    }

    /**
     * Get bc
     *
     * @return BonCommande
     */
    public function getBc()
    {
        return $this->bc;
    }

    /**
     * @return bool
     */
    public function isPrevente()
    {
        return $this->prevente;
    }

    /**
     * @param bool $prevente
     * @return Vente
     */
    public function setPrevente($prevente)
    {
        $this->prevente = $prevente;
        return $this;
    }


    /**
     * Get prevente
     *
     * @return boolean
     */
    public function getPrevente()
    {
        return $this->prevente;
    }

    /**
     * Add lesPlus
     *
     * @param PlusVente $lesPlus
     *
     * @return Vente
     */
    public function addLesPlus(PlusVente $lesPlus)
    {
        $this->lesPlus[] = $lesPlus;

        return $this;
    }

    /**
     * Remove lesPlus
     *
     * @param PlusVente $lesPlus
     */
    public function removeLesPlus(PlusVente $lesPlus)
    {
        $this->lesPlus->removeElement($lesPlus);
    }

    /**
     * Get lesPlus
     *
     * @return Collection
     */
    public function getLesPlus()
    {
        return $this->lesPlus;
    }

    public function getParcTown()
    {
        $elemnts = $this->parcours->toArray();
        $parc = '';
        $i = 0;
        $len = count($elemnts);


        /** @var Parcour $parcour */
        foreach ($elemnts as $parcour)
        {
            $parc = $parc.$parcour->getFromName().'/'.$parcour->getToName();
            if ($i != $len - 1) {
                $parc = $parc.'<hr class="separator">';
            }
            $i++;
        }
        return $parc;
    }

    public function getParcDate()
    {
        $elemnts = $this->parcours->toArray();
        $parc = '';
        $i = 0;
        $len = count($elemnts);

        /** @var Parcour $parcour */
        foreach ($elemnts as $parcour)
        {
            $parc = $parc.$parcour->getDateParcour()->format('D d M Y');
            if ($i != $len - 1) {
                $parc = $parc.'<hr class="separator">';
            }
            $i++;
        }
        return $parc;
    }

    /**
     * @return string
     */
    public function getParcDateMini()
    {
        $elemnts = $this->parcours->toArray();
        $parc = '';
        $i = 0;
        $len = count($elemnts);


        /** @var Parcour $parcour */
        foreach ($elemnts as $parcour)
        {
            $parc = $parc.$parcour->getDateParcour()->format('d/m/Y');
            if ($i != $len - 1) {
                $parc = $parc.'<hr class="separator">';
            }
            $i++;
        }
        return $parc;
    }

    public function getParcClass()
    {
        $elemnts = $this->parcours->toArray();
        $parc = '';
        $i = 0;
        $len = count($elemnts);

        /** @var Parcour $parcour */
        foreach ($elemnts as $parcour)
        {
            $parc = $parc.$parcour->getClasseBlt();
            if ($i != $len - 1) {
                $parc = $parc.'<hr class="separator">';
            }
            $i++;
        }
        return $parc;
    }

    public function getTotalTarifBC()
    {
        return (int)$this->montantTransport + (int)$this->taxe;
    }

    public function getTotalTarifBCFEES()
    {
        return $this->getTotalTarifBC() + $this->feesAGV;
    }


    /**
     * @return bool
     */
    public function isRemboursable()
    {
        return $this->remboursable;
    }

    /**
     * @param bool $remboursable
     * @return Vente
     */
    public function setRemboursable($remboursable)
    {
        $this->remboursable = $remboursable;
        return $this;
    }

    /**
     * @return string
     */
    public function getTypeBillet()
    {
        return $this->typeBillet;
    }

    /**
     * @param string $typeBillet
     * @return Vente
     */
    public function setTypeBillet($typeBillet)
    {
        $this->typeBillet = $typeBillet;
        return $this;
    }

    /**
     * @return string
     */
    public function getTypeVente()
    {
        return $this->typeVente;
    }

    /**
     * @param string $typeVente
     * @return Vente
     */
    public function setTypeVente($typeVente)
    {
        $this->typeVente = $typeVente;
        return $this;
    }

    /**
     * @return int
     */
    public function getAutreFees()
    {
        return $this->autreFees;
    }

    /**
     * @param int $autreFees
     * @return Vente
     */
    public function setAutreFees($autreFees)
    {
        $this->autreFees = $autreFees;
        return $this;
    }


    /**
     * Get remboursable
     *
     * @return boolean
     */
    public function getRemboursable()
    {
        return $this->remboursable;
    }

    /**
     * @return float
     */
    public function getRemboursement()
    {
        return $this->remboursement;
    }

    /**
     * @param float $remboursement
     * @return Vente
     */
    public function setRemboursement($remboursement)
    {
        $this->remboursement = $remboursement;
        return $this;
    }



    /**
     * @return bool
     */
    public function isReglee()
    {
        return $this->reglee;
    }

    /**
     * @param bool $reglee
     * @return Vente
     */
    public function setReglee($reglee)
    {
        $this->reglee = $reglee;
        return $this;
    }


    /**
     * Get reglee
     *
     * @return boolean
     */
    public function getReglee()
    {
        return $this->reglee;
    }
}
