<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 30/08/2017
 * Time: 09:36
 */

namespace App\Entity\Vente;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Scan
 * @package App\Entity\Vente
 *
 * @ORM\Table(name="piece_jointe")
 * @ORM\Entity(repositoryClass="ScanRepository")
 */
class Scan
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="file_name")
     */
    private $file;

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }


    /**
     * @param mixed $file
     * @return Scan
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


}
