<?php

namespace App\Entity\Vente;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Vente\PaiementFactureRepository")
 */
class PaiementFacture
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Paiement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Vente\Paiement", inversedBy="paiementFactures")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $paiement;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vente\Facture", inversedBy="paiementFactures")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $facture;

    /**
     * @ORM\Column(type="float")
     */
    private $montant;

    /**
     * @var string
     */
    private $factureref;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPaiement(): ?Paiement
    {
        return $this->paiement;
    }

    public function setPaiement(?Paiement $paiement): self
    {
        $this->paiement = $paiement;

        return $this;
    }

    public function getFacture(): ?Facture
    {
        return $this->facture;
    }

    public function setFacture(?Facture $facture): self
    {
        $this->facture = $facture;

        return $this;
    }

    public function getMontant(): ?float
    {
        return $this->montant;
    }

    /**
     * @param float $montant
     *
     * @return PaiementFacture
     */
    public function setMontant(float $montant): self
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * @return float|int
     */
    public function getCommission()
    {
        return ($this->getMontant()/$this->getPaiement()->getMontantRecu()) * $this->getPaiement()->getCommission();
    }

    /**
     * @return float|int
     */
    public function getTotalPaiement()
    {
        return $this->montant + $this->getCommission();
    }

    /**
     * @return mixed
     */
    public function getFactureref()
    {
        return $this->factureref;
    }

    /**
     * @param null|string $factureref
     *
     * @return PaiementFacture
     */
    public function setFactureref(?string $factureref = null): self
    {
        $this->factureref = $factureref;

        if (!$factureref) {
            $this->factureref = $this->getFacture()->getReference();
        }

        return $this;
    }

    /**
     * @return float|int
     */
    public function getMontantDevise()
    {
        $cours = $this->getPaiement()->getChange();

        return $this->getMontant() / $cours;
    }
}
