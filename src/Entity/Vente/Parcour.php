<?php

namespace App\Entity\Vente;

use Doctrine\ORM\Mapping as ORM;

/**
 * Parcour
 *
 * @ORM\Table(name="parcour")
 * @ORM\Entity(repositoryClass="App\Repository\Vente\ParcourRepository")
 */
class Parcour
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="from_name", type="string", length=255)
     */
    private $fromName;

    /**
     * @var string
     *
     * @ORM\Column(name="to_name", type="string", length=255)
     */
    private $toName;


    /**
     * @var string
     *
     * @ORM\Column(name="classe_blt", type="string", length=255)
     */
    private $classeBlt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_parcour", type="date")
     */
    private $dateParcour;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fromName
     *
     * @param string $fromName
     *
     * @return Parcour
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;

        return $this;
    }

    /**
     * Get fromName
     *
     * @return string
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * Set toName
     *
     * @param string $toName
     *
     * @return Parcour
     */
    public function setToName($toName)
    {
        $this->toName = $toName;

        return $this;
    }

    /**
     * Get toName
     *
     * @return string
     */
    public function getToName()
    {
        return $this->toName;
    }


    /**
     * Set classeBlt
     *
     * @param string $classeBlt
     *
     * @return Parcour
     */
    public function setClasseBlt($classeBlt)
    {
        $this->classeBlt = $classeBlt;

        return $this;
    }

    /**
     * Get classeBlt
     *
     * @return string
     */
    public function getClasseBlt()
    {
        return $this->classeBlt;
    }


    /**
     * Set dateParcour
     *
     * @param \DateTime $dateParcour
     *
     * @return Parcour
     */
    public function setDateParcour($dateParcour)
    {
        $this->dateParcour = $dateParcour;

        return $this;
    }

    /**
     * Get dateParcour
     *
     * @return \DateTime
     */
    public function getDateParcour()
    {
        return $this->dateParcour;
    }
}
