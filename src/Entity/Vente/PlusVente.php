<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 14/09/2017
 * Time: 11:24
 */

namespace App\Entity\Vente;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class PlusVente
 * @package App\Entity\Vente
 * @ORM\Table(name="plus_vente")
 * @ORM\Entity(repositoryClass="App\Repository\Vente\PlusVenteRepository")
 */
class PlusVente
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="libelle", type="string")
     */
    private $libelle;

    /**
     * @var string
     * @ORM\Column(name="montant", type="string")
     */
    private $montant;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Plus_vente
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     * @return Plus_vente
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
        return $this;
    }

    /**
     * @return string
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @param string $montant
     * @return Plus_vente
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
        return $this;
    }

}
