<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 04/09/2017
 * Time: 06:19
 */

namespace App\Entity\Vente;

use App\Entity\Admin\Tiers;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * Parcour
 *
 * @ORM\Table(name="facture")
 * @ORM\Entity(repositoryClass="App\Repository\Vente\FactureRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Facture
{
    use FactureTrait;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", nullable=true)
     */
    private $reference;

    /**
     * @var Tiers
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Tiers")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $tier;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Vente\Vente", mappedBy="facture", cascade={"remove"})
     */
    private $ventes;

    private $id_li;

    /**
     * @var string
     *
     * @ORM\Column(name="type_paiement", type="string")
     */
    private $typePaiement;

    /**
     * @var float
     * @ORM\Column(name="montat_rembourse", nullable=true, type="float")
     */
    private $montatRembourse = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="change_taux", type="string", nullable=true)
     */
    private $tauxChange;

    /**
     * @var string
     *
     * @ORM\Column(name="montant_tva", type="float")
     */
    private $montantTva;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_echeance", type="date", nullable=true)
     */
    private $dateEcheance;

    /**
     * @var string
     *
     * @ORM\Column(name="montant_transport", type="string")
     *
     */
    private $montantTransport;

    /**
     * @var string
     *
     * @ORM\Column(name="montant_taxe", type="string")
     */
    private $montantTaxe;

    /**
     * @var string
     *
     * @ORM\Column(name="montant_fees", type="string")
     */
    private $montantFees;

    /**
     * @var Scan
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Vente\Scan", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="file_scan_id", referencedColumnName="id")
     */
    private $scan;

    /**
     * @var boolean
     *
     * @ORM\Column(name="payed", type="boolean")
     */
    private $payed = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="annule", type="boolean")
     */
    private $annule = false;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_facture", type="date")
     */
    private $dateFacture;

    /**
     * @var float
     * @ORM\Column(name="autre_prod", type="float")
     */
    private $autreProd = 0;

    private $file_scan;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Vente\PaiementFacture", mappedBy="facture", orphanRemoval=true)
     */
    private $paiementFactures;

    /**
     * @return mixed
     */
    public function getFileScan()
    {
        return $this->file_scan;
    }

    /**
     * @param mixed $file_scan
     * @return Facture
     */
    public function setFileScan($file_scan)
    {
        $this->file_scan = $file_scan;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Facture
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set change
     *
     * @param string $change
     *
     * @return Facture
     */
    public function setTauxChange($change)
    {
        $this->tauxChange = $change;

        return $this;
    }

    /**
     * Get tauxChange
     *
     * @return string
     */
    public function getTauxChange()
    {
        return $this->tauxChange;
    }

    /**
     * @return string
     */
    public function getMontantTva()
    {
        return $this->montantTva;
    }

    /**
     * @param string $montantTva
     * @return Facture
     */
    public function setMontantTva($montantTva)
    {
        $this->montantTva = $montantTva;
        return $this;
    }

    /**
     * @param string $typePaiement
     * @return $this
     */
    public function setTypePaiement($typePaiement)
    {
        $this->typePaiement = $typePaiement;
        return $this;
    }

    /**
     * @return string
     */
    public function getTypePaiement()
    {
        return $this->typePaiement;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        //$this->paiements = new ArrayCollection();
        $this->ventes = new ArrayCollection();
        $this->paiementFactures = new ArrayCollection();
    }

    /**
     * @return DateTime
     */
    public function getDateEcheance()
    {
        return $this->dateEcheance;
    }

    /**
     * @param DateTime $dateEcheance
     * @return Facture
     */
    public function setDateEcheance($dateEcheance)
    {
        $this->dateEcheance = $dateEcheance;
        return $this;
    }

    /**
     * @return string
     */
    public function getMontantTransport()
    {
        return $this->montantTransport;
    }

    /**
     * @param string $montantTransport
     * @return Facture
     */
    public function setMontantTransport($montantTransport)
    {
        $this->montantTransport = $montantTransport;
        return $this;
    }

    /**
     * @return string
     */
    public function getMontantTaxe()
    {
        return $this->montantTaxe;
    }

    /**
     * @param string $montantTaxe
     * @return Facture
     */
    public function setMontantTaxe($montantTaxe)
    {
        $this->montantTaxe = $montantTaxe;
        return $this;
    }

    /**
     * @return string
     */
    public function getMontantFees()
    {
        return $this->montantFees;
    }

    /**
     * @param string $montantFees
     * @return Facture
     */
    public function setMontantFees($montantFees)
    {
        $this->montantFees = $montantFees;
        return $this;
    }


    /**
     * Set payed
     *
     * @param boolean $payed
     *
     * @return Facture
     */
    public function setPayed($payed)
    {
        $this->payed = $payed;

        return $this;
    }

    /**
     * Get payed
     *
     * @return boolean
     */
    public function isPayed()
    {
        return $this->payed;
    }

    /**
     * @return bool
     */
    public function isAnnule()
    {
        return $this->annule;
    }

    /**
     * @param bool $annule
     * @return Facture
     */
    public function setAnnule($annule)
    {
        $this->annule = $annule;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateFacture()
    {
        return $this->dateFacture;
    }

    /**
     * @param DateTime $dateFacture
     * @return Facture
     */
    public function setDateFacture($dateFacture)
    {
        $this->dateFacture = $dateFacture;
        return $this;
    }

    /**
     * @param ArrayCollection $paiements
     */
    public function setPaiements($paiements)
    {
        $this->paiements = $paiements;
    }

    /**
     * @ORM\PrePersist()
     * @throws Exception
     */
    public function makedateFacture()
    {
        $this->dateFacture = new DateTime('now');
    }

    /**
     * Set tier
     *
     * @param Tiers $tier
     *
     * @return Facture
     */
    public function setTier(Tiers $tier = null)
    {
        $this->tier = $tier;

        return $this;
    }

    /**
     * Get tier
     *
     * @return Tiers
     */
    public function getTier(): ?Tiers
    {
        return $this->tier;
    }

    /**
     * Add vente
     *
     * @param Vente $vente
     *
     * @return Facture
     */
    public function addVente(Vente $vente)
    {
        $this->ventes[] = $vente;

        $vente->setFacture($this);

        return $this;
    }

    /**
     * Remove vente
     *
     * @param Vente $vente
     */
    public function removeVente(Vente $vente)
    {
        $this->ventes->removeElement($vente);
    }

    /**
     * Get ventes
     *
     * @return Collection|Vente[]
     */
    public function getVentes()
    {
        return $this->ventes;
    }

    /**
     * Set scan
     *
     * @param Scan $scan
     *
     * @return Facture
     */
    public function setScan(Scan $scan = null)
    {
        $this->scan = $scan;

        return $this;
    }

    /**
     * Get scan
     *
     * @return Scan
     */
    public function getScan()
    {
        return $this->scan;
    }

    /**
     * @return float
     */
    public function getAutreProd()
    {
        return $this->autreProd;
    }

    /**
     * @param float $autreProd
     * @return Facture
     */
    public function setAutreProd($autreProd)
    {
        $this->autreProd = $autreProd;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getIdLi()
    {
        return $this->id_li;
    }

    /**
     * @param mixed $id_li
     * @return Facture
     */
    public function setIdLi($id_li)
    {
        $this->id_li = $id_li;
        return $this;
    }


    /**
     * @return float
     */
    public function getMontatRembourse()
    {
        return $this->montatRembourse;
    }

    /**
     * @param float $montatRembourse
     * @return Facture
     */
    public function setMontatRembourse($montatRembourse)
    {
        $this->montatRembourse = $montatRembourse;
        return $this;
    }

    /**
     * @return Collection|PaiementFacture[]
     */
    public function getPaiementFactures(): Collection
    {
        return $this->paiementFactures;
    }

    /**
     * @param PaiementFacture $paiementFacture
     *
     * @return Facture
     */
    public function addPaiementFacture(PaiementFacture $paiementFacture): self
    {
        if (!$this->paiementFactures->contains($paiementFacture)) {
            $this->paiementFactures[] = $paiementFacture;
            $paiementFacture->setFacture($this);
        }

        return $this;
    }

    /**
     * @param PaiementFacture $paiementFacture
     *
     * @return Facture
     */
    public function removePaiementFacture(PaiementFacture $paiementFacture): self
    {
        if ($this->paiementFactures->contains($paiementFacture)) {
            $this->paiementFactures->removeElement($paiementFacture);
            // set the owning side to null (unless already changed)
            if ($paiementFacture->getFacture() === $this) {
                $paiementFacture->setFacture(null);
            }
        }

        return $this;
    }
}
