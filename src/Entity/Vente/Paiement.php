<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 04/09/2017
 * Time: 08:42
 */

namespace App\Entity\Vente;

use App\Entity\Admin\Currency;
use App\Entity\Admin\Tiers;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Admin\PaiementMode;
use App\Entity\Reglement\Avoir;
use App\Entity\Reglement\NoteCredit;
use Symfony\Component\Validator\Constraints\DateTime;
use App\Library\AsLetter;

/**
 * Class Paiement
 * @package App\Entity\Vente
 *
 * @ORM\Table(name="paiement")
 * @ORM\Entity(repositoryClass="App\Repository\Vente\PaiementRepository")
 *
 */
class Paiement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="montant_recu", type="string", nullable=true)
     */
    private $montantRecu = null;

    /**
     * @var int
     * @ORM\Column(name="commission", type="integer")
     */
    private $commission = 0;

    /**
     * @var Currency
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Currency")
     * @ORM\JoinColumn(name="curency", referencedColumnName="code_iso", nullable=true)
     */
    private $currency;

    /**
     * @var int
     * @ORM\Column(name="montant_devise", type="integer")
     */
    private $montantDevise = 0;

    /**
     * @var int
     * @ORM\Column(name="taux_change", type="integer")
     */
    private $change = 1;

    /**
     * @var PaiementMode
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\PaiementMode")
     * @ORM\JoinColumn(name="mode_id", referencedColumnName="id")
     */
    private $moyenPay;

    /**
     * @var string
     *
     * @ORM\Column(name="num_cheque", type="string", nullable=true)
     */
    private $numCheque;

    /**
     * @var string
     *
     * @ORM\Column(name="ref_operation", type="string", nullable=true)
     */
    private $refOperation;

    /**
     * @var string
     *
     * @ORM\Column(name="autre_info", type="string", nullable=true)
     */
    private $nomBanque;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_banque", type="string", nullable=true)
     */
    private $autreInfo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="valide", type="boolean")
     */
    private $valide = false;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_paiement", type="date")
     */
    private $datePaiement;

    /**
     * @var Tiers
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Tiers")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Vente\PaiementFacture",
     *     mappedBy="paiement",
     *     orphanRemoval=true,
     *     cascade={"persist","remove"})
     */
    private $paiementFactures;

    /**
     * Paiement constructor.
     */
    public function __construct()
    {
        $this->paiementFactures = new ArrayCollection();
    }

    /**
     * @var NoteCredit
     * @ORM\ManyToOne(targetEntity="App\Entity\Reglement\NoteCredit")
     * @ORM\JoinColumn(name="note_credit_id", referencedColumnName="id")
     */
    private $noteCredit = null ;

    /**
     * @var Avoir
     * @ORM\ManyToOne(targetEntity="App\Entity\Reglement\Avoir")
     * @ORM\JoinColumn(name="avoir_id", referencedColumnName="id")
     */
    private $avoir;

    /**
     * @var Scan
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Vente\Scan", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="file_scan_id", referencedColumnName="id")
     */
    private $scan;

    /**
     * @var NoteCredit
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Reglement\NoteCredit")
     * @ORM\JoinColumn(name="related_note_id", referencedColumnName="id")
     */
    private $relatedNote;

    private $file_scan;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set montantRecu
     *
     * @param string $montantRecu
     *
     * @return Paiement
     */
    public function setMontantRecu($montantRecu)
    {
        $this->montantRecu = $montantRecu;

        return $this;
    }

    /**
     * Get montantRecu
     *
     * @return string
     */
    public function getMontantRecu()
    {
        return $this->montantRecu;
    }

    /**
     * Set numCheque
     *
     * @param string $numCheque
     *
     * @return Paiement
     */
    public function setNumCheque($numCheque)
    {
        $this->numCheque = $numCheque;

        return $this;
    }

    /**
     * Get numCheque
     *
     * @return string
     */
    public function getNumCheque()
    {
        return $this->numCheque;
    }

    /**
     * Set refOperation
     *
     * @param string $refOperation
     *
     * @return Paiement
     */
    public function setRefOperation($refOperation)
    {
        $this->refOperation = $refOperation;

        return $this;
    }

    /**
     * Get refOperation
     *
     * @return string
     */
    public function getRefOperation()
    {
        return $this->refOperation;
    }

    /**
     * Set nomBanque
     *
     * @param string $nomBanque
     *
     * @return Paiement
     */
    public function setNomBanque($nomBanque)
    {
        $this->nomBanque = $nomBanque;

        return $this;
    }

    /**
     * Get nomBanque
     *
     * @return string
     */
    public function getNomBanque()
    {
        return $this->nomBanque;
    }

    /**
     * Set datePaiement
     *
     * @param \DateTime $datePaiement
     *
     * @return Paiement
     */
    public function setDatePaiement($datePaiement)
    {
        $this->datePaiement = $datePaiement;

        return $this;
    }

    /**
     * Get datePaiement
     *
     * @return \DateTime|DateTime
     */
    public function getDatePaiement()
    {
        return $this->datePaiement;
    }

    /**
     * Set moyenPay
     *
     * @param PaiementMode $moyenPay
     *
     * @return Paiement
     */
    public function setMoyenPay(PaiementMode $moyenPay = null)
    {
        $this->moyenPay = $moyenPay;

        return $this;
    }

    /**
     * Get moyenPay
     *
     * @return PaiementMode
     */
    public function getMoyenPay()
    {
        return $this->moyenPay;
    }

    /**
     * Set valide
     *
     * @param boolean $valide
     *
     * @return Paiement
     */
    public function setValide($valide)
    {
        $this->valide = $valide;

        return $this;
    }

    /**
     * Get valide
     *
     * @return boolean
     */
    public function getValide()
    {
        return $this->valide;
    }

    /**
     * Set montantDevise
     *
     * @param integer $montantDevise
     *
     * @return Paiement
     */
    public function setMontantDevise($montantDevise)
    {
        $this->montantDevise = $montantDevise;

        return $this;
    }

    /**
     * Get montantDevise
     *
     * @return integer
     */
    public function getMontantDevise()
    {
        return $this->montantDevise;
    }

    /**
     * Set change
     *
     * @param integer $change
     *
     * @return Paiement
     */
    public function setChange($change)
    {
        $this->change = $change;

        return $this;
    }

    /**
     * Get change
     *
     * @return integer
     */
    public function getChange()
    {
        return $this->change;
    }

    /**
     * Set currency
     *
     * @param Currency $currency
     *
     * @return Paiement
     */
    public function setCurrency(Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    public function montantWithDevice()
    {
        if($this->getMontantDevise() != 0){
            $this->setMontantRecu(strval($this->getMontantDevise()*$this->getChange()));
        }
    }

    /**
     * Set noteCredit
     *
     * @param NoteCredit $noteCredit
     *
     * @return Paiement
     */
    public function setNoteCredit(NoteCredit $noteCredit = null)
    {
        $this->noteCredit = $noteCredit;

        return $this;
    }

    /**
     * Get noteCredit
     *
     * @return NoteCredit
     */
    public function getNoteCredit()
    {
        return $this->noteCredit;
    }


    /**
     * Set avoir
     *
     * @param Avoir $avoir
     *
     * @return Paiement
     */
    public function setAvoir(Avoir $avoir = null)
    {
        $this->avoir = $avoir;

        return $this;
    }

    /**
     * Get avoir
     *
     * @return Avoir
     */
    public function getAvoir()
    {
        return $this->avoir;
    }

    /**
     * @return int
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @param int $commission
     * @return Paiement
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;
        return $this;
    }


    /**
     * Set autreInfo
     *
     * @param string $autreInfo
     *
     * @return Paiement
     */
    public function setAutreInfo($autreInfo)
    {
        $this->autreInfo = $autreInfo;

        return $this;
    }

    /**
     * Get autreInfo
     *
     * @return string
     */
    public function getAutreInfo()
    {
        return $this->autreInfo;
    }

    /**
     * Set scan
     *
     * @param Scan $scan
     *
     * @return Paiement
     */
    public function setScan(Scan $scan = null)
    {
        $this->scan = $scan;

        return $this;
    }

    /**
     * Get scan
     *
     * @return Scan
     */
    public function getScan()
    {
        return $this->scan;
    }

    /**
     * @return mixed
     */
    public function getFileScan()
    {
        return $this->file_scan;
    }

    /**
     * @param mixed $file_scan
     * @return Paiement
     */
    public function setFileScan($file_scan)
    {
        $this->file_scan = $file_scan;
        return $this;
    }

    /**
     * @return Collection|PaiementFacture[]
     */
    public function getPaiementFactures(): Collection
    {
        return $this->paiementFactures;
    }

    /**
     * @param PaiementFacture $paiementFacture
     *
     * @return Paiement
     */
    public function addPaiementFacture(PaiementFacture $paiementFacture): self
    {
        if (!$this->paiementFactures->contains($paiementFacture)) {
            $this->paiementFactures[] = $paiementFacture;
            $paiementFacture->setPaiement($this);
        }

        return $this;
    }

    /**
     * @param PaiementFacture $paiementFacture
     *
     * @return Paiement
     */
    public function removePaiementFacture(PaiementFacture $paiementFacture): self
    {
        if ($this->paiementFactures->contains($paiementFacture)) {
            $this->paiementFactures->removeElement($paiementFacture);
            // set the owning side to null (unless already changed)
            if ($paiementFacture->getPaiement() === $this) {
                $paiementFacture->setPaiement(null);
            }
        }

        return $this;
    }

    /**
     * @return Tiers
     */
    public function getClient(): Tiers
    {
        return $this->client;
    }

    /**
     * @param Tiers $client
     *
     * @return Paiement
     */
    public function setClient(Tiers $client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return int
     */
    public function getMontantTotal()
    {
        return (int)$this->montantRecu + $this->commission;
    }

    /**
     * Get all facture associated to the paiements
     *
     * @return Facture[] $factures
     */
    public function getFactures()
    {
        $factures = [];

        foreach ($this->getPaiementFactures() as $paiementFacture){
            $factures[] = $paiementFacture->getFacture();
        }

        return $factures;
    }

    /**
     * @return NoteCredit
     */
    public function getRelatedNote(): NoteCredit
    {
        return $this->relatedNote;
    }

    /**
     * @param NoteCredit $relatedNote
     *
     * @return Paiement
     */
    public function setRelatedNote(NoteCredit $relatedNote): self
    {
        $this->relatedNote = $relatedNote;

        return $this;
    }

    public function letter()
    {
        return AsLetter::asLetters($this->getMontantTotal());
    }
}
