<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 30/09/2017
 * Time: 11:29
 */

namespace App\Entity\Vente;

use App\Entity\Admin\Tiers;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Facture_annule
 * @package App\Entity\Vente
 * @ORM\Table(name="facture_annule")
 * @ORM\Entity(repositoryClass="App\Repository\Vente\Facture_annuleRepository")
 */
class FactureAnnule
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", nullable=true)
     */
    private $reference;

    /**
     * @var Tiers
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Tiers")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $tier;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Facture_annule
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set tier
     *
     * @param \App\Entity\Admin\Tiers $tier
     *
     * @return Facture_annule
     */
    public function setTier(\App\Entity\Admin\Tiers $tier = null)
    {
        $this->tier = $tier;

        return $this;
    }

    /**
     * Get tier
     *
     * @return \App\Entity\Admin\Tiers
     */
    public function getTier()
    {
        return $this->tier;
    }
}
