<?php

namespace App\Entity\Admin;


use Doctrine\ORM\Mapping as ORM;

/**
 * TauxCom
 *
 * @ORM\Table(name="taux_com")
 * @ORM\Entity(repositoryClass="App\Repository\Admin\TauxComRepository")
 */
class TauxCom
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="taux", type="float")
     */
    private $taux;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set taux
     *
     * @param float $taux
     *
     * @return TauxCom
     */
    public function setTaux($taux)
    {
        $this->taux = $taux;

        return $this;
    }

    /**
     * Get taux
     *
     * @return float
     */
    public function getTaux()
    {
        return $this->taux;
    }
}
