<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 25/08/2017
 * Time: 12:30
 */

namespace App\Entity\Admin;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Currency
 * @package App\Entity\Admin
 *
 * @ORM\Table(name="currency")
 * @ORM\Entity(repositoryClass="App\Repository\Admin\CurrencyRepository")
 */
class Currency
{

    /*private $id;*/

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="code_iso", type="string")
     */
    private $code_iso;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string")
     */
    private $libelle;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active = false;

    /**
     * @return string
     */
    public function getCodeIso()
    {
        return $this->code_iso;
    }

    /**
     * @param string $code_iso
     * @return Currency
     */
    public function setCodeIso($code_iso)
    {
        $this->code_iso = $code_iso;
        return $this;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     * @return Currency
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return Currency
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }



    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
}
