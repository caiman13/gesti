<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 25/08/2017
 * Time: 14:20
 */

namespace App\Entity\Admin;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Paiement_mode
 * @package App\Entity\Admin
 *
 * @ORM\Table(name="paiement_mode")
 * @ORM\Entity(repositoryClass="App\Repository\Admin\PaiementModeRepository")
 */
class PaiementMode
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle" , type="string")
     */
    private $libelle;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return PaiementMode
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     *
     * @return PaiementMode
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
        return $this;
    }

    public function __toString()
    {
        return $this->getLibelle();
    }
}
