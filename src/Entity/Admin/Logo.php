<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 12/09/2017
 * Time: 01:22
 */

namespace App\Entity\Admin;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Logo
 * @package App\Entity\Admin
 *
 * @ORM\Table(name="logo")
 * @ORM\Entity(repositoryClass="App\Repository\Admin\LogoRepository")
 */
class Logo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="filename", type="string")
     */
    private $filename;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     * @return Logo
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
        return $this;
    }


}
