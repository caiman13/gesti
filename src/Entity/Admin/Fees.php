<?php

namespace App\Entity\Admin;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Fees
 *
 * @ORM\Table(name="fees")
 * @ORM\Entity(repositoryClass="App\Repository\Admin\FeesRepository")
 */
class Fees
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lib_court", type="string", length=255)
     */
    private $libCourt;

    /**
     * @var string
     *
     * @ORM\Column(name="lib_long", type="string", length=255)
     */
    private $libLong;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Classes", cascade={"persist","remove"})
     * @ORM\JoinTable(name="fees_classes",
     *      joinColumns={@ORM\JoinColumn(name="fees_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="classes_id", referencedColumnName="id", unique=false, onDelete="CASCADE")}
     *      )
     */
    private $classes;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libCourt
     *
     * @param string $libCourt
     *
     * @return Fees
     */
    public function setLibCourt($libCourt)
    {
        $this->libCourt = $libCourt;

        return $this;
    }

    /**
     * Get libCourt
     *
     * @return string
     */
    public function getLibCourt()
    {
        return $this->libCourt;
    }

    /**
     * Set libLong
     *
     * @param string $libLong
     *
     * @return Fees
     */
    public function setLibLong($libLong)
    {
        $this->libLong = $libLong;

        return $this;
    }

    /**
     * Get libLong
     *
     * @return string
     */
    public function getLibLong()
    {
        return $this->libLong;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->classes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add class
     *
     * @param \App\Entity\Admin\Classes $class
     *
     * @return Fees
     */
    public function addClass(\App\Entity\Admin\Classes $class)
    {
        $this->classes[] = $class;

        return $this;
    }

    /**
     * Remove class
     *
     * @param \App\Entity\Admin\Classes $class
     */
    public function removeClass(\App\Entity\Admin\Classes $class)
    {
        $this->classes->removeElement($class);
    }

    /**
     * Get classes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClasses()
    {
        return $this->classes;
    }
}
