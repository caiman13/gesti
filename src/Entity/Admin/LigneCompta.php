<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 21/09/2017
 * Time: 14:55
 */

namespace App\Entity\Admin;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class LigneCompta
 * @package App\Entity\Admin
 *
 * @ORM\Table(name="ligne_compte")
 * @ORM\Entity(repositoryClass="App\Repository\Admin\LigneComptaRepository")
 */
class LigneCompta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_ligne", type="date")
     */
    private $dateLigne;

    /**
     * @var string
     * @ORM\Column(name="code_journal", type="string")
     */
    private $codeJournal;

    /**
     * @var string
     *
     * @ORM\Column(name="ref_saisie", type="string")
     */
    private $refSaisie = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ref_fact")
     */
    private $refFact;

    /**
     * @var string
     * @ORM\Column(name="ref_avoir", type="string")
     */
    private $refAvoir = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ref_bc", type="string")
     */
    private $refBc = '';

    /**
     * @var string
     *
     * @ORM\Column(name="num_cmpt", type="string")
     */
    private $numCmpt;

    /**
     * @var string
     *
     * @ORM\Column(name="plan_compte", type="string", nullable=true)
     */
    private $planCompte = null;

    /**
     * @var int
     *
     * @ORM\Column(name="mnt_debit", type="integer")
     */
    private $mntDebit = 0;

    /**
     * @var int
     * @ORM\Column(name="mnt_credit", type="string")
     */
    private $mntCredit = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="detail", type="string")
     */
    private $detail = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $autrePlanCli;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getDateLigne()
    {
        return $this->dateLigne;
    }

    /**
     * @param DateTime $dateLigne
     * @return LigneCompta
     */
    public function setDateLigne($dateLigne)
    {
        $this->dateLigne = $dateLigne;
        return $this;
    }

    /**
     * @return string
     */
    public function getRefSaisie()
    {
        return $this->refSaisie;
    }

    /**
     * @param string $refSaisie
     * @return LigneCompta
     */
    public function setRefSaisie($refSaisie)
    {
        $this->refSaisie = $refSaisie;
        return $this;
    }

    /**
     * @return string
     */
    public function getRefFact()
    {
        return $this->refFact;
    }

    /**
     * @param string $refFact
     * @return LigneCompta
     */
    public function setRefFact($refFact)
    {
        $this->refFact = $refFact;
        return $this;
    }

    /**
     * @return string
     */
    public function getRefBc()
    {
        return $this->refBc;
    }

    /**
     * @param string $refBc
     * @return LigneCompta
     */
    public function setRefBc($refBc)
    {
        $this->refBc = $refBc;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumCmpt()
    {
        return $this->numCmpt;
    }

    /**
     * @param string $numCmpt
     * @return LigneCompta
     */
    public function setNumCmpt($numCmpt)
    {
        $this->numCmpt = $numCmpt;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlanCompte()
    {
        return $this->planCompte;
    }

    /**
     * @param string $planCompte
     * @return LigneCompta
     */
    public function setPlanCompte($planCompte)
    {
        $this->planCompte = $planCompte;
        return $this;
    }

    /**
     * @return int
     */
    public function getMntDebit()
    {
        return $this->mntDebit;
    }

    /**
     * @param int $mntDebit
     * @return LigneCompta
     */
    public function setMntDebit($mntDebit)
    {
        $this->mntDebit = $mntDebit;
        return $this;
    }

    /**
     * @return int
     */
    public function getMntCredit()
    {
        return $this->mntCredit;
    }

    /**
     * @param int $mntCredit
     * @return LigneCompta
     */
    public function setMntCredit($mntCredit)
    {
        $this->mntCredit = $mntCredit;
        return $this;
    }

    /**
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * @param string $detail
     * @return LigneCompta
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodeJournal()
    {
        return $this->codeJournal;
    }

    /**
     * @param string $codeJournal
     * @return LigneCompta
     */
    public function setCodeJournal($codeJournal)
    {
        $this->codeJournal = $codeJournal;
        return $this;
    }

    /**
     * @return string
     */
    public function getRefAvoir()
    {
        return $this->refAvoir;
    }

    /**
     * @param string $refAvoir
     *
     * @return LigneCompta
     */
    public function setRefAvoir($refAvoir)
    {
        $this->refAvoir = $refAvoir;

        return $this;
    }

    public function getAutrePlanCli(): ?string
    {
        return $this->autrePlanCli;
    }

    /**
     * @param string|null $autrePlanCli
     *
     * @return $this
     */
    public function setAutrePlanCli(?string $autrePlanCli): self
    {
        $this->autrePlanCli = $autrePlanCli;

        return $this;
    }


}
