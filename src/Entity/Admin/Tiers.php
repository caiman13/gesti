<?php

namespace App\Entity\Admin;


use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Tiers
 *
 * @ORM\Table(name="tiers")
 * @ORM\Entity(repositoryClass="App\Repository\Admin\TiersRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Tiers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @var Responsable
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Admin\Responsable", cascade={"all"})
     * @ORM\JoinColumn(name="responsable_id", referencedColumnName="id", nullable=true)
     */
    private $responsable;

    /**
     * @var string
     *
     * @ORM\Column(name="raisonSociale", type="string", length=255, nullable=true)
     */
    private $raisonSociale;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="mail", type="string", length=255, nullable=true)
     */
    private $mail;

    /**
     * @var string
     *
     * @ORM\Column(name="discriminator", type="string", length=255)
     */
    private $discriminator;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string" , length=255)
     */
    private $type = 'societe';

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Contact", cascade={"persist","remove"})
     * @ORM\JoinTable(name="tiers_contact",
     *      joinColumns={@ORM\JoinColumn(name="tiers_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="contact_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     */
    private $contacts;

    /**
     * @var string
     *
     * @ORM\Column(name="type_soc", type="string", nullable=true)
     */
    private $typeSoc;

    /**
     * @var string
     *
     * @ORM\Column(name="type_cli", type="string", nullable=true)
     */
    private $typeCli;

    /**
     * @var Tiers
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Tiers", inversedBy="compagnies")
     * @ORM\JoinColumn(name="societe_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $societe;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Admin\Tiers", mappedBy="societe")
     */
    private $compagnies;

    /**
     * @var string
     * @ORM\Column(name="num_stat", type="string", nullable=true)
     */
    private $numStat;

    /**
     * @var string
     * @ORM\Column(name="num_nif", type="string", nullable=true)
     */
    private $numNIF;

    /**
     * Only for general information of the company
     *
     * @var string
     *
     * @ORM\Column(name="num_rcs", type="string", nullable=true)
     */
    private $numRcs;

    /**
     * @var string
     *
     * @ORM\Column(name="num_cif", type="string", nullable=true)
     */
    private $numCIF;


    /**
     * @var DateTime
     * @ORM\Column(name="date_cif", type="date", nullable=true)
     */
    private $dateCIF;

    /**
     * @var Logo
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Admin\Logo", cascade={"persist", "remove"})
     */
    private $logo;

    /**
     * @var bool
     *
     * @ORM\Column(name="linked", type="boolean")
     */
    private $linked = false;

    /**
     * @var string
     *
     * @ORM\Column(name="num_compte", type="string", nullable=true)
     */
    private $numCompte;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $auteCompte;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Tiers
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Tiers
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set raisonSociale
     *
     * @param string $raisonSociale
     *
     * @return Tiers
     */
    public function setRaisonSociale($raisonSociale)
    {
        $this->raisonSociale = $raisonSociale;

        return $this;
    }

    /**
     * Get raisonSociale
     *
     * @return string
     */
    public function getRaisonSociale(): ?string
    {
        return $this->raisonSociale;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Tiers
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse() : ?string
    {
        return $this->adresse;
    }

    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return Tiers
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @return string
     */
    public function getDiscriminator()
    {
        return $this->discriminator;
    }

    /**
     * @param string $discriminator
     * @return Tiers
     */
    public function setDiscriminator($discriminator)
    {
        $this->discriminator = $discriminator;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Tiers
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contacts = new ArrayCollection();
        $this->compagnies = new ArrayCollection();
    }

    /**
     * Add contact
     *
     * @param Contact $contact
     *
     * @return Tiers
     */
    public function addContact(Contact $contact)
    {
        $this->contacts[] = $contact;

        return $this;
    }

    /**
     * Remove contact
     *
     * @param Contact $contact
     */
    public function removeContact(Contact $contact)
    {
        $this->contacts->removeElement($contact);
    }

    /**
     * Get contacts
     *
     * @return Collection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @return Responsable
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * @param Responsable $responsable
     * @return Tiers
     */
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;
        return $this;
    }

    function __toString()
    {
        if($this->getType() == "societe"){
            return $this->getRaisonSociale();
        } else {
            return $this->getNom()." ".$this->getPrenom();
        }
    }



    /**
     * Set typeSoc
     *
     * @param string $typeSoc
     *
     * @return Tiers
     */
    public function setTypeSoc($typeSoc)
    {
        $this->typeSoc = $typeSoc;

        return $this;
    }

    /**
     * Get typeSoc
     *
     * @return string
     */
    public function getTypeSoc()
    {
        return $this->typeSoc;
    }

    /**
     * Set societe
     *
     * @param Tiers $societe
     *
     * @return Tiers
     */
    public function setSociete(Tiers $societe = null)
    {
        $this->societe = $societe;

        return $this;
    }

    /**
     * Get societe
     *
     * @return Tiers
     */
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * Add compagny
     *
     * @param Tiers $compagny
     *
     * @return Tiers
     */
    public function addCompagny(Tiers $compagny)
    {
        $this->compagnies[] = $compagny;

        return $this;
    }

    /**
     * Remove compagny
     *
     * @param Tiers $compagny
     */
    public function removeCompagny(Tiers $compagny)
    {
        $this->compagnies->removeElement($compagny);
    }

    /**
     * Get compagnies
     *
     * @return Collection
     */
    public function getCompagnies()
    {
        return $this->compagnies;
    }

    /**
     * @return string
     */
    public function getNumStat()
    {
        return $this->numStat;
    }

    /**
     * @param string $numStat
     * @return Tiers
     */
    public function setNumStat($numStat)
    {
        $this->numStat = $numStat;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumRcs()
    {
        return $this->numRcs;
    }

    /**
     * @param string $numRcs
     * @return Tiers
     */
    public function setNumRcs($numRcs)
    {
        $this->numRcs = $numRcs;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumCIF()
    {
        return $this->numCIF;
    }

    /**
     * @param string $numCIF
     * @return Tiers
     */
    public function setNumCIF($numCIF)
    {
        $this->numCIF = $numCIF;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumNIF()
    {
        return $this->numNIF;
    }

    /**
     * @param string $numNIF
     * @return Tiers
     */
    public function setNumNIF($numNIF)
    {
        $this->numNIF = $numNIF;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateCIF()
    {
        return $this->dateCIF;
    }

    /**
     * @param DateTime $dateCIF
     * @return Tiers
     */
    public function setDateCIF($dateCIF)
    {
        $this->dateCIF = $dateCIF;
        return $this;
    }
    


    /**
     * Set logo
     *
     * @param Logo $logo
     *
     * @return Tiers
     */
    public function setLogo(Logo $logo = null)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return Logo
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @return bool
     */
    public function isLinked()
    {
        return $this->linked;
    }

    /**
     * @param bool $linked
     * @return Tiers
     */
    public function setLinked($linked)
    {
        $this->linked = $linked;
        return $this;
    }

    /**
     * @return string
     */
    public function getTypeCli()
    {
        return $this->typeCli;
    }

    /**
     * @param string $typeCli
     * @return Tiers
     */
    public function setTypeCli($typeCli)
    {
        $this->typeCli = $typeCli;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumCompte()
    {
        return $this->numCompte;
    }

    /**
     * @param string $numCompte
     * @return Tiers
     */
    public function setNumCompte($numCompte)
    {
        $this->numCompte = $numCompte;
        return $this;
    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function myCompte()
    {
        if($this->discriminator == 'client') {
            $this->setTypeSoc(null);
            if ($this->typeCli == 'client-grp') {
                $this->numCompte = '41150000';
            } else {
                $this->numCompte = '41160000';
            }
        }
    }

    /**
     * Get linked
     *
     * @return boolean
     */
    public function getLinked()
    {
        return $this->linked;
    }

    public function getAuteCompte(): ?string
    {
        return $this->auteCompte;
    }

    public function setAuteCompte(?string $auteCompte): self
    {
        $this->auteCompte = $auteCompte;

        return $this;
    }
}
