<?php

namespace App\Entity\Admin;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classes
 *
 * @ORM\Table(name="classes")
 * @ORM\Entity(repositoryClass="App\Repository\Admin\ClassesRepository")
 */
class Classes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="classes", type="string", length=255)
     */
    private $classes;

    /**
     * @var float
     *
     * @ORM\Column(name="simple", type="float")
     */
    private $simple;

    /**
     * @var float
     *
     * @ORM\Column(name="aller_retour", type="float")
     */
    private $allerRetour;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set classes
     *
     * @param string $classes
     *
     * @return Classes
     */
    public function setClasses($classes)
    {
        $this->classes = $classes;

        return $this;
    }

    /**
     * Get classes
     *
     * @return string
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * Set simple
     *
     * @param float $simple
     *
     * @return Classes
     */
    public function setSimple($simple)
    {
        $this->simple = $simple;

        return $this;
    }

    /**
     * Get simple
     *
     * @return float
     */
    public function getSimple()
    {
        return $this->simple;
    }

    /**
     * Set allerRetour
     *
     * @param float $allerRetour
     *
     * @return Classes
     */
    public function setAllerRetour($allerRetour)
    {
        $this->allerRetour = $allerRetour;

        return $this;
    }

    /**
     * Get allerRetour
     *
     * @return float
     */
    public function getAllerRetour()
    {
        return $this->allerRetour;
    }
}
