<?php

namespace App\Entity\Reglement;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Vente\Vente;

/**
 * InfoVente
 *
 * @ORM\Table(name="info_vente")
 * @ORM\Entity(repositoryClass="App\Repository\Reglement\InfoVenteRepository")
 */
class InfoVente
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    private $nameCie;

    /**
     * @var string
     * @ORM\Column(name="passager", type="string")
     */
    private $passager;

    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float")
     */
    private $montant;

    /**
     * @var string
     *
     * @ORM\Column(name="compteCie", type="string", length=255)
     */
    private $compteCie;

    /**
     * @var string
     * @ORM\Column(name="cie_name", type="string")
     */
    private $cieName;

    /**
     * @var string
     * @ORM\Column(name="ticket_number", type="string")
     */
    private $ticketNumber;

    public function __construct(Vente $vente = null)
    {
        if($vente != null)
        {
            $this->montant = $vente->getTotalTarifBC();
            $this->cieName = $vente->getCompagieAerienne()->getRaisonSociale();
            $this->nameCie = $this->cieName;
            $this->compteCie = $vente->getCompagieAerienne()->getNumCompte();
            $this->ticketNumber = $vente->getNumBillet();
            $this->passager = $vente->getPassager();
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set montant
     *
     * @param float $montant
     *
     * @return InfoVente
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set compteCie
     *
     * @param string $compteCie
     *
     * @return InfoVente
     */
    public function setCompteCie($compteCie)
    {
        $this->compteCie = $compteCie;

        return $this;
    }

    /**
     * Get compteCie
     *
     * @return string
     */
    public function getCompteCie()
    {
        return $this->compteCie;
    }

    /**
     * @return string
     */
    public function getCieName()
    {
        return $this->cieName;
    }

    /**
     * @param string $cieName
     * @return InfoVente
     */
    public function setCieName($cieName)
    {
        $this->cieName = $cieName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNameCie()
    {
        return $this->nameCie;
    }

    /**
     * @param mixed $nameCie
     * @return InfoVente
     */
    public function setNameCie($nameCie)
    {
        $this->nameCie = $nameCie;
        return $this;
    }

    /**
     * @return string
     */
    public function getTicketNumber()
    {
        return $this->ticketNumber;
    }

    /**
     * @param string $ticketNumber
     * @return InfoVente
     */
    public function setTicketNumber($ticketNumber)
    {
        $this->ticketNumber = $ticketNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassager()
    {
        return $this->passager;
    }

    /**
     * @param string $passager
     * @return InfoVente
     */
    public function setPassager($passager)
    {
        $this->passager = $passager;
        return $this;
    }

}
