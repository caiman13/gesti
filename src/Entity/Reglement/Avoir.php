<?php

namespace App\Entity\Reglement;

use App\Entity\Admin\LigneCompta;
use App\Entity\Admin\Tiers;
use App\Library\AsLetter;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Vente\Facture;
use Exception;

/**
 * Avoir
 *
 * @ORM\Table(name="avoir")
 * @ORM\Entity(repositoryClass="App\Repository\Reglement\AvoirRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Avoir
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="reference", type="string", nullable=true)
     */
    private $reference;

    /**
     * @var Tiers
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Tiers")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float")
     */
    private $montant;

    /**
     * @var Facture
     * @ORM\ManyToOne(targetEntity="App\Entity\Vente\Facture")
     * @ORM\JoinColumn(name="facture_id", referencedColumnName="id")
     */
    private $facture;

    /**
     * @var bool
     *
     * @ORM\Column(name="payed", type="boolean")
     */
    private $payed = false;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\Reglement\InfoVente", cascade={"persist","remove"})
     * @ORM\JoinTable(name="avoir_info_vente",
     *      joinColumns={@ORM\JoinColumn(name="avoir_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="info_vente_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     */
    private $ventes;

    /**
     * @var DateTime
     * @ORM\Column(name="date_avoir", type="date")
     */
    private $dateAvoir;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set montant
     *
     * @param float $montant
     *
     * @return Avoir
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set payed
     *
     * @param boolean $payed
     *
     * @return Avoir
     */
    public function setPayed($payed)
    {
        $this->payed = $payed;

        return $this;
    }

    /**
     * Get payed
     *
     * @return bool
     */
    public function getPayed()
    {
        return $this->payed;
    }

    /**
     * Set client
     *
     * @param Tiers $client
     *
     * @return Avoir
     */
    public function setClient(Tiers $client = null)
    {
        $this->client = $client;

        return $this;
    }


    /**
     * @param string $refFacture
     * @return Avoir
     */
    public function setRefFacture($refFacture)
    {
        $this->refFacture = $refFacture;
        return $this;
    }

    /**
     * @param Facture $facture
     * @return array
     * @throws Exception
     */
    public function getLignes(Facture $facture)
    {
        $lignes = array();
        $detail = 'REMBOURSEMENT ';
        if($this->getClient()->getType() == 'personne'){
            $detail = $detail.'PART '.$facture->getReference().' '.$this->getClient();
        } else {
            $detail = $this->getClient().' '.$facture->getReference();
        }

        $code_journal = 'VTE AV';

        $ligne1 = new LigneCompta();
        $ligne1->setCodeJournal($code_journal);
        $ligne1->setRefFact($this->getReference());
        $ligne1->setDateLigne(new DateTime());
        $ligne1->setNumCmpt($facture->getTier()->getNumCompte());
        $ligne1->setMntCredit((int)$this->getMontant());
        $ligne1->setDetail($detail);
        $ligne1->setRefAvoir($this->getReference());
        $lignes[] = $ligne1;


        /** @var InfoVente $item */
        foreach ($this->getVentes()->getIterator() as $item){
            $ligne = new LigneCompta();
            $ligne->setCodeJournal($code_journal);
            $ligne->setRefFact($this->getReference());
            $ligne->setDateLigne(new DateTime());
            $ligne->setNumCmpt($item->getCompteCie());
            $ligne->setMntDebit((int)$item->getMontant());
            $ligne->setDetail($detail);
            $ligne->setRefAvoir($this->getReference());
            $lignes[] = $ligne;
        }

        return $lignes;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ventes = new ArrayCollection();
    }

    /**
     * Get client
     *
     * @return Tiers
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Add vente
     *
     * @param InfoVente $vente
     *
     * @return Avoir
     */
    public function addVente(InfoVente $vente)
    {
        $this->ventes[] = $vente;

        return $this;
    }

    /**
     * Remove vente
     *
     * @param InfoVente $vente
     */
    public function removeVente(InfoVente $vente)
    {
        $this->ventes->removeElement($vente);
    }

    /**
     * Get ventes
     *
     * @return Collection
     */
    public function getVentes()
    {
        return $this->ventes;
    }

    /**
     * @return $this
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function faireMontant()
    {
        $total = 0;
        foreach ($this->ventes->getIterator() as $item){
            $total = $total + $item->getMontant();
        }
        $this->setMontant($total);
        return $this;
    }

    public function getCieVente()
    {
        $elements = $this->ventes->toArray();
        $total = '';
        $i = 0;
        $len = count($elements);
        foreach ($elements as $item){
            $total = $total.$item->getCieName();
            if ($i != $len - 1) {
                $total = $total.'/';
            }
            $i++;
        }
        return $total;
    }

    /**
     * @return DateTime
     */
    public function getDateAvoir()
    {
        return $this->dateAvoir;
    }

    /**
     * @param DateTime $dateAvoir
     * @return Avoir
     */
    public function setDateAvoir($dateAvoir)
    {
        $this->dateAvoir = $dateAvoir;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     *
     * @throws Exception
     */
    public function faireDateAvoir()
    {
        $this->setDateAvoir(new DateTime());
    }

    public function __toString()
    {
        return strval($this->getMontant()).' MGA du '.$this->getDateAvoir()->format('d/m/Y');
    }

    /**
     * Set facture
     *
     * @param Facture $facture
     *
     * @return Avoir
     */
    public function setFacture(Facture $facture = null)
    {
        $this->facture = $facture;

        return $this;
    }

    /**
     * Get facture
     *
     * @return Facture
     */
    public function getFacture()
    {
        return $this->facture;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return Avoir
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    public function setCieVente()
    {
        foreach ($this->ventes->getIterator() as $item){
            $item->setNameCie($item->getCieName());
        }
    }

    public function getLetter()
    {
        $text = AsLetter::asLetters($this->getMontant());
        $text_explode = explode(' ', $text);
        if ($text_explode[0] == 'millions'){
            $text = 'un '.$text;
        }
        return $text;
    }
}
