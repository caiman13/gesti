<?php

namespace App\Entity\Reglement;

use App\Entity\Admin\LigneCompta;
use App\Entity\Admin\Tiers;
use Doctrine\ORM\Mapping as ORM;

/**
 * Reglement
 *
 * @ORM\Table(name="reglement")
 * @ORM\Entity(repositoryClass="App\Repository\Reglement\ReglementRepository")
 */
class Reglement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float")
     */
    private $montant;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var Tiers
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Tiers")
     * @ORM\JoinColumn(name="cie_id", referencedColumnName="id")
     */
    private $cie;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set montant
     *
     * @param float $montant
     *
     * @return Reglement
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Reglement
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }


    /**
     * Set cie
     *
     * @param \App\Entity\Admin\Tiers $cie
     *
     * @return Reglement
     */
    public function setCie(\App\Entity\Admin\Tiers $cie = null)
    {
        $this->cie = $cie;

        return $this;
    }

    /**
     * Get cie
     *
     * @return \App\Entity\Admin\Tiers
     */
    public function getCie()
    {
        return $this->cie;
    }

    public function getLignes()
    {
        $lignes = array();
        $detail = 'REGLT VENTE '.$this->getCie();

        $ligne2 = new LigneCompta();
        $ligne2->setCodeJournal('B34');
        $ligne2->setRefFact('RGT-'.$this->id.'/'.$this->date->format('y'));
        $ligne2->setDateLigne($this->getDate());
        $ligne2->setNumCmpt($this->getCie()->getNumCompte());
        $ligne2->setMntDebit((int)$this->getMontant());
        $ligne2->setDetail($detail);
        $lignes[] = $ligne2;

        $ligne1 = new LigneCompta();
        $ligne1->setCodeJournal('B34');
        $ligne1->setRefFact('RGT-'.$this->id.'/'.$this->date->format('y'));
        $ligne1->setDateLigne($this->getDate());
        $ligne1->setNumCmpt('51216000');
        $ligne1->setMntCredit((int)$this->getMontant());
        $ligne1->setDetail($detail);
        $lignes[] = $ligne1;


        return $lignes;
    }
}
