<?php

namespace App\Entity\Reglement;

use App\Entity\Admin\Tiers;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Vente\Facture;

/**
 * NoteCredit
 *
 * @ORM\Table(name="note_credit")
 * @ORM\Entity(repositoryClass="App\Repository\Reglement\NoteCreditRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class NoteCredit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="reference", type="string")
     */
    private $reference ='';

    /**
     * @var Facture
     * @ORM\ManyToOne(targetEntity="App\Entity\Vente\Facture")
     * @ORM\JoinColumn(name="facture_id", referencedColumnName="id")
     */
    private $facture;

    /**
     * @var Tiers
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Tiers")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float")
     */
    private $montant;

    /**
     * @var bool
     *
     * @ORM\Column(name="payed", type="boolean")
     */
    private $payed = false;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_note", type="date")
     */
    private $dateNote;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set montant
     *
     * @param float $montant
     *
     * @return NoteCredit
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set payed
     *
     * @param boolean $payed
     *
     * @return NoteCredit
     */
    public function setPayed($payed)
    {
        $this->payed = $payed;

        return $this;
    }

    /**
     * Get payed
     *
     * @return bool
     */
    public function getPayed()
    {
        return $this->payed;
    }

    /**
     * Set client
     *
     * @param \App\Entity\Admin\Tiers $client
     *
     * @return NoteCredit
     */
    public function setClient(\App\Entity\Admin\Tiers $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \App\Entity\Admin\Tiers
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return \DateTime
     */
    public function getDateNote()
    {
        return $this->dateNote;
    }

    /**
     * @param \DateTime $dateNote
     * @return NoteCredit
     */
    public function setDateNote($dateNote)
    {
        $this->dateNote = $dateNote;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function faireDateNote()
    {
        $this->setDateNote(new \DateTime());
    }

    public function __toString()
    {
        return strval($this->getMontant()).' MGA du '.$this->getDateNote()->format('d/m/Y');
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return NoteCredit
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * Set facture
     *
     * @param \App\Entity\Vente\Facture $facture
     *
     * @return NoteCredit
     */
    public function setFacture(\App\Entity\Vente\Facture $facture = null)
    {
        $this->facture = $facture;

        return $this;
    }

    /**
     * Get facture
     *
     * @return \App\Entity\Vente\Facture
     */
    public function getFacture()
    {
        return $this->facture;
    }
}
