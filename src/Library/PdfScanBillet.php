<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 01/09/2017
 * Time: 06:42
 */

namespace App\Library;


use Exception;

class PdfScanBillet
{
    /**
     * @var Pdf_decode
     */
    private $reader;

    /**
     * PdfScan constructor
     */
    public function __construct()
    {
        $this->reader = new Pdf_decode();
    }


    /**
     * @return Pdf_decode
     */
    public function getReader()
    {
        return $this->reader;
    }

    private function buildItinerary(array $result, int $index)
    {
        $valueReturned['end'] = false;
        $itinnerary1['date'] = trim($result[$index++]);
        $res = trim($result[$index++]);
        if (count(explode(" ", $res)) == 2) {
            $itinnerary1['from'] = $res;
        } else {
            $itinnerary1['from'] = $res." ".$result[$index++];
        }
        $res = trim($result[$index++]);
        if (count(explode(" ", $res)) == 2) {
            $itinnerary1['to'] = $res;
        } else {
            $itinnerary1['to'] = $res." ".$result[$index++];
        }

        $itinnerary1['flight'] = trim($result[$index++]);
        $itinnerary1['class'] = trim($result[$index++]);

        $valueReturned ['itinerary'] = $itinnerary1;


        /*dump($itinnerary1);*/
        while (trim($result[$index]) !== '-') {
            $indice = $index + 3;
            if (($result[$indice] === 'Receipt') || ($result[$indice] === '(1) Ok = ')) {
                $valueReturned['end'] = true;

                $valueReturned['index'] = $index;

                return $valueReturned;
            }
            $index++;
        }
        $index++;

        $valueReturned['index'] = $index;

        return $valueReturned;
    }

    private function getItinerary(array $result)
    {
        $from_key = array_search("From", $result);
        $form_key = $from_key + 2;
        $itinneraries = [];

        $end = false;


        while ($end == false){
            $itinReturn = $this->buildItinerary($result, $form_key);

            $end = $itinReturn['end'];

            $itinneraries[] = $itinReturn['itinerary'];


            $form_key = $itinReturn['index'];
        }

        $returnData["itinerary"] = $itinneraries;

        return $returnData;
    }

    private function getAirlineSurcharge(array $result)
    {
        $key_airline = array_search('Airline Surcharges', $result);

        $values = explode(" ", trim($result[$key_airline + 2]));
        $nkeyAir = $key_airline + 2;
        /*dump($values); die;*/
        $surg = [];
        $surg["devise"] = $values[0];
        $surg["montant"] = 0;

        while (trim($result[$nkeyAir]) !== 'Total Amount') {
            $values = explode(" ", trim($result[$nkeyAir]));
            /*dump(count($values)); die;*/
            if (count($values) == 2) {
                $surg["montant"] = $surg["montant"] + (int)substr($values[1], 0, -2);
            } else {
                $surg["montant"] = $surg["montant"] + (int)substr($values[2], 0, -2);
            }

            $nkeyAir++;
        }


        $returnData["airline_surcharge"] = $surg;

        return $returnData;
    }

    private function getTaxesAndMontant(array $result)
    {
        $returnData = [];
        $fare_key = (array_search("Equiv Fare Paid", $result)) ? array_search(
            "Equiv Fare Paid",
            $result
        ) : array_search("Air Fare", $result);


        $res = preg_replace("/: /", "", trim($result[$fare_key + 1]));

        $values = explode(" ", $res);

        $montant = [];

        if (count($values) == 2) {
            $montant["devise"] = $values[0];
            $montant["montant"] = $values[1];
        } else {
            $montant["devise"] = 'MGA';
            $montant["montant"] = $values[0];
        }


        $returnData["montant"] = $montant;
        $tax_ind = $fare_key + 2;
        $taxes = [];
        while (trim($result[$tax_ind]) !== 'Airline Surcharges') {
            if (trim($result[$tax_ind]) == 'Tax') {
                $tax_ind = $tax_ind + 2;
            } else {
                $values = explode(" ", trim($result[$tax_ind]));
                /*dump(count($values)); die;*/
                $tax = [];
                $tax["devise"] = $values[0];
                /*dump($values); die;*/
                if (count($values) == 2) {
                    $tax["montant"] = substr($values[1], 0, -2);
                } else {
                    $tax["montant"] = substr($values[2], 0, -2);
                }


                $taxes[] = $tax;
                $tax_ind++;
            }
        }
        $returnData["tax"] = $taxes;

        return $returnData;
    }

    /**
     * @param $filename
     *
     * @return array|null
     * @throws Exception
     */
    public function scan($filename)
    {
        $result = explode("\n", $this->reader->decode($filename));
        /*die;*/
//         dump($result); die;
        try {
            $donnees = [];
            $donnees["cie_ae"] = $result[11];
            $donnees["ticket_number"] = $result[12];
            $donnees["traveler"] = $result[17];
            $donnees["book_ref"] = $result[2];


            $donnees = array_merge($donnees,$this->getTaxesAndMontant($result));

            //dump($donnees); die;

            $donnees = array_merge($donnees,$this->getAirlineSurcharge($result));


            $donnees = array_merge($donnees,$this->getItinerary($result));

            return $donnees;
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }

    }
}