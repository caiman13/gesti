<?php


namespace App\Library;


class CommFacture
{
    /**
     * @var string
     */
    private $factyreRef;

    /**
     * @var int
     */
    private $commissionPaiement;

    public function __construct(string $factyreRef, int $commissionPaiement)
    {
        $this->factyreRef = $factyreRef;
        $this->commissionPaiement = $commissionPaiement;
    }

    /**
     * @return string
     */
    public function getFactyreRef(): string
    {
        return $this->factyreRef;
    }

    /**
     * @param string $factyreRef
     *
     * @return CommFacture
     */
    public function setFactyreRef(string $factyreRef): self
    {
        $this->factyreRef = $factyreRef;

        return $this;
    }

    /**
     * @return int
     */
    public function getCommissionPaiement(): int
    {
        return $this->commissionPaiement;
    }

    /**
     * @param int $commissionPaiement
     *
     * @return CommFacture
     */
    public function setCommissionPaiement(int $commissionPaiement): self
    {
        $this->commissionPaiement = $commissionPaiement;

        return $this;
    }
}