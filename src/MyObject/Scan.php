<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 01/09/2017
 * Time: 06:54
 */

namespace App\MyObject;


class Scan
{
    private $file;



    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     * @return Scan
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }


}