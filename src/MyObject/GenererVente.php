<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 18/09/2017
 * Time: 14:09
 */

namespace App\MyObject;


use Doctrine\Common\Collections\ArrayCollection;

class GenererVente
{
    /**
     * @var array
     */
    private $ventes;

    public function __construct()
    {
        $this->ventes = new ArrayCollection();
    }

    /**
     * @return array
     */
    public function getVentes()
    {
        return $this->ventes;
    }

    /**
     * @param array $ventes
     * @return $this
     */
    public function setVentes($ventes)
    {
        $this->ventes = $ventes;
        return $this;
    }
}