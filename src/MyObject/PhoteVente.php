<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 18/09/2017
 * Time: 14:13
 */

namespace App\MyObject;


use App\Entity\Admin\Classes;
use App\Entity\Admin\Fees;
use App\Entity\Vente\Vente;

class PhoteVente
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Vente
     */
    private $vente;

    /**
     * @var string
     */
    private $passager;

    /**
     * @var Fees
     */
    private $fees;

    /**
     * @var string
     */
    private $numBillet;

    /**
     * @var string
     */
    private $bookRef;

    /**
     * @var Classes
     */
    private $classes;

    public function __construct(Vente $vente)
    {
        $this->setId($vente->getId())
            ->setPassager($vente->getPassager())
        ;
        $this->vente = $vente;
    }

    /*public function __construct1(Vente $vente)
    {
        $this->setId($vente->getId())
            ->setPassager($vente->getPassager())
            ;
    }*/

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return PhoteVente
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassager()
    {
        return $this->passager;
    }

    /**
     * @param string $passager
     * @return PhoteVente
     */
    public function setPassager($passager)
    {
        $this->passager = $passager;
        return $this;
    }

    /**
     * @return Fees
     */
    public function getFees()
    {
        return $this->fees;
    }

    /**
     * @param Fees $fees
     * @return PhoteVente
     */
    public function setFees($fees)
    {
        $this->fees = $fees;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumBillet()
    {
        return $this->numBillet;
    }

    /**
     * @param string $numBillet
     * @return PhoteVente
     */
    public function setNumBillet($numBillet)
    {
        $this->numBillet = $numBillet;
        return $this;
    }

    /**
     * @return string
     */
    public function getBookRef()
    {
        return $this->bookRef;
    }

    /**
     * @param string $bookRef
     * @return PhoteVente
     */
    public function setBookRef($bookRef)
    {
        $this->bookRef = $bookRef;
        return $this;
    }

    /**
     * @return Classes
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * @param Classes $classes
     * @return PhoteVente
     */
    public function setClasses($classes)
    {
        $this->classes = $classes;
        return $this;
    }

    /**
     * @return Vente
     */
    public function getVente()
    {
        return $this->vente;
    }

    /**
     * @param Vente $vente
     * @return PhoteVente
     */
    public function setVente($vente)
    {
        $this->vente = $vente;
        return $this;
    }


}