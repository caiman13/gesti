<?php

namespace App\Controller\Etat;

use App\Controller\BaseController;
use App\Repository\Admin\TiersRepository;
use App\Repository\Vente\FactureRepository;
use App\Services\FactureService;
use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FactureController
 * @package App\Controller\Etat
 *
 * @Route("/facture")
 */
class FactureController extends BaseController
{
    /**
     * Index
     *
     * @Route("/", name="etat_facture_index")
     *
     * @param TiersRepository $tiersRepository
     *
     * @return Response
     * @throws Exception
     */
    public function index(TiersRepository $tiersRepository)
    {
        $clients = $tiersRepository->findBy(['discriminator' => 'client'], ['raisonSociale' => 'ASC', 'nom' => 'ASC']);

        return $this->render(
            'etat/facture/index.html.twig',
            [
                'clients' => $clients,
                'today' => new DateTime(),
            ]
        );
    }

    /**
     * List facture for etat
     *
     * @Route("/list/{etat}/{id_cli}/{debut}/{fin}", 
     *     name="etat_facture_list", 
     *     defaults={
     *     "id_cli" = 0,
     *     "debut" = 0,
     *     "fin" = 0
     *     })
     *
     * @param $etat
     * @param $id_cli
     * @param $debut
     * @param $fin
     * @param TiersRepository $tiersRepository
     * @param FactureRepository $factureRepository
     *
     * @return Response
     */
    public function listAction(
        $etat,
        $id_cli,
        $debut,
        $fin,
        TiersRepository $tiersRepository,
        FactureRepository $factureRepository
    ) {
        $payed = ($etat == 'payee') ? true : false;
        if ($id_cli != 0) {
            $client = $tiersRepository->find($id_cli);
            $factures = $factureRepository->getByDateCliAndEtat($client, $payed, $debut, $fin);
        } else {
            $factures = $factureRepository->getByDateEtat($payed, $debut, $fin);
        }
        

        return $this->render(
            'etat/facture/list.html.twig',
            [
                'factures' => $factures,
                'etat' => $etat,
                'debut' => $debut,
                'fin' => $fin,
                'idCli' => $id_cli,
            ]
        );
    }

    /**
     * List annule for etat
     * @Route("/list-annule/{id_cli}/{debut}/{fin}",
     *     name="etat_facture_annule",
     *     defaults={
     *     "id_cli" = 0,
     *     "debut" = 0,
     *     "fin" = 0
     *     })
     *
     * @param $id_cli
     * @param $debut
     * @param $fin
     * @param TiersRepository $tiersRepository
     * @param FactureRepository $factureRepository
     *
     * @return Response
     */
    public function listannuleAction(
        $id_cli,
        $debut,
        $fin,
        TiersRepository $tiersRepository,
        FactureRepository $factureRepository
    ) {
        if ($id_cli != 0) {
            $client = $tiersRepository->find($id_cli);
            $factures = $factureRepository->getAnnuleByCliByDate($client, $debut, $fin);
        } else {
            $factures = $factureRepository->getAnnuleByDate($debut, $fin);
        }
        

        return $this->render(
            'etat/facture/list.html.twig',
            [
                'factures' => $factures,
                'etat' => 'annule',
            ]
        );
    }

    /**
     * Print pdf of etat
     *
     * @Route("/rapport-pdf/{etat}/{id_cli}/{debut}/{fin}",
     *     name="etat_facture_raport_pdf",
     *     defaults={
     *     "id_cli" = 0,
     *     "debut" = 0,
     *     "fin" = 0
     *     })
     *
     * @param $etat
     * @param $id_cli
     * @param $debut
     * @param $fin
     * @param TiersRepository $tiersRepository
     * @param FactureRepository $factureRepository
     *
     * @param FactureService $factureService
     *
     * @return Response
     * @throws Exception
     */
    public function etatPdf(
        $etat,
        $id_cli,
        $debut,
        $fin,
        TiersRepository $tiersRepository,
        FactureRepository $factureRepository,
        FactureService $factureService
    ) {
        $payed = ($etat == 'payee') ? true : false;
        $client = $tiersRepository->find($id_cli);
        $factures = $factureRepository->getByDateCliAndEtat($client, $payed, $debut, $fin);
        $ets = $tiersRepository->find(1);
        $filename = sprintf('fature-etat-%s.pdf', $client);

        $html = $this->renderView(
            'etat/facture/pdf.html.twig',
            [
                'factures' => $factures,
                'ets' => $ets,
                'etat' => $etat,
                'client' => $client,
                'today' => new DateTime(),
                'totalMontant' => $factureService->getTotalMontant($factures),
                'totalSolde' => $factureService->getTotalMontant($factures, true, false),
                'totalPayed' => FactureService::getTotalPaye($factures, false),
            ]
        );

        $footer = $this->renderView(
            'footer.html.twig',
            [
                'ets' => $ets,
            ]
        );

        return new Response(
            $this->pdf->getOutputFromHtml($html, ['footer-html' => $footer]),
            200,
            [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            ]
        );
    }
}
