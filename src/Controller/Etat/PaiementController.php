<?php

namespace App\Controller\Etat;

use App\Controller\BaseController;
use App\Repository\Admin\PaiementModeRepository;
use App\Repository\Admin\TiersRepository;
use App\Repository\Vente\PaiementRepository;
use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PaiementController
 * @package App\Controller\Etat
 *
 * @Route("/paiement")
 */
class PaiementController extends BaseController
{
    /**
     * Index
     *
     * @Route("/", name="etat_paiement_index")
     *
     * @param TiersRepository $tiersRepository
     * @param PaiementModeRepository $paiementModeRepository
     *
     * @return Response
     * @throws Exception
     */
    public function index(TiersRepository $tiersRepository, PaiementModeRepository $paiementModeRepository)
    {
        $clients = $tiersRepository->findBy(['discriminator' => 'client'], ['raisonSociale' => 'ASC', 'nom' => 'ASC']);
        $paiement_modes = $paiementModeRepository->findAll();

        return $this->render('etet/paiement/index.html.twig', [
            'clients' => $clients,
                'today' => new DateTime(),
                'payMods' => $paiement_modes,
            ]
        );
    }

    /**
     * Liste des paiements
     *
     * @Route("/list/{etat}/{id_cli}/{debut}/{fin}/{modPay}",
     *     name="etat_paiement_list",
     *     defaults={
     *     "id_cli" = 0,
     *     "debut" = 0,
     *     "fin" = 0,
     *     "modPay" = 0,
     *     })
     *
     * @param $etat
     * @param $id_cli
     * @param $debut
     * @param $fin
     * @param $modPay
     * @param PaiementRepository $repository
     *
     * @return Response
     */
    public function listByModPayAction($etat, $id_cli, $debut, $fin, $modPay, PaiementRepository $repository)
    {
        $paiements = $repository->getByTypeByEtatByCli($etat, $id_cli, $debut, $fin, $modPay);

        return $this->render(
            'etet/paiement/list.html.twig',
            [
                'paiements' => $paiements,
            ]
        );
    }

}
