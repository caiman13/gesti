<?php

namespace App\Controller\Etat;

use App\Controller\BaseController;
use App\Repository\Admin\TiersRepository;
use App\Repository\Vente\VenteRepository;
use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class VenteController
 * @package App\Controller\Etat
 *
 * @Route("/vente")
 */
class VenteController extends BaseController
{
    /**
     * Index
     *
     * @Route("/", name="etat_vente_index")
     *
     * @param TiersRepository $tiersRepository
     *
     * @return Response
     * @throws Exception
     */
    public function index(TiersRepository $tiersRepository)
    {
        $clients = $tiersRepository->findBy(
            [
                'discriminator' => 'fournisseurs',
                'typeSoc' => 'Compagnie aériènne',
            ],
            [
                'raisonSociale' => 'ASC',
                'nom' => 'ASC',
            ]
        );

        return $this->render(
            'etat/vente/index.html.twig',
            [
                'clients' => $clients,
                'today' => new DateTime(),
            ]
        );
    }

    /**
     * List
     *
     * @Route("/list/{etat}/{id_cie}/{debut}/{fin}",
     *     name="etat_vente_list",
     *     defaults={
     *     "id_cie" = 0,
     *     "debut" = 0,
     *     "fin" = 0
     *     })
     *
     * @param $etat
     * @param $id_cie
     * @param $debut
     * @param $fin
     * @param VenteRepository $repository
     *
     * @return Response
     */
    public function list($etat, $id_cie, $debut, $fin, VenteRepository $repository)
    {
        $ventes = $repository->findByClientAndActive($etat, $debut, $fin, (int)$id_cie);

        return $this->render(
            'etat/vente/list.html.twig',
            [
                'ventes' => $ventes,
                'active' => $etat,
                'id_cie' => (int)$id_cie,
            ]
        );
    }
}
