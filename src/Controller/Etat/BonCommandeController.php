<?php

namespace App\Controller\Etat;

use App\Controller\BaseController;
use App\Repository\Admin\TiersRepository;
use App\Repository\Vente\BonCommandeRepository;
use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BonCommandeController
 * @package App\Controller\Etat
 *
 * @Route("/bon-commande")
 */
class BonCommandeController extends BaseController
{
    /**
     * Lists all BC entities.
     *
     * @Route("/", name="bc_etat_index")
     *
     * @param TiersRepository $tiersRepository
     *
     * @return Response
     * @throws Exception
     */
    public function index(TiersRepository $tiersRepository)
    {
        return $this->render(
            'etat/bc/index.html.twig',
            [
                'today' => new DateTime(),
                'clients' => $tiersRepository->findBy(
                    ['discriminator' => 'fournisseurs', 'linked' => false],
                    ['raisonSociale' => 'ASC', 'nom' => 'ASC']
                ),
            ]
        );
    }

    /**
     * List of bc
     *
     * @Route("/list/{etat}/{id_cli}/{debut}/{fin}",
     *     name="bc_etat_list",
     *     defaults={
     *     "id_cli" = 0,
     *     "debut" = 0,
     *     "fin" = 0
     *     })
     *
     * @param $etat
     * @param $id_cli
     * @param $debut
     * @param $fin
     * @param BonCommandeRepository $bonCommandeRepository
     *
     * @return Response
     */
    public function list($etat , $id_cli, $debut, $fin, BonCommandeRepository $bonCommandeRepository)
    {
        $bcs = $bonCommandeRepository->bcByFRNSByDateByEtat($etat , $id_cli, $debut, $fin);
        return $this->render('etat/bc/list.html.twig', [
            'bcs' => $bcs
        ]);
    }
}
