<?php

namespace App\Controller\Etat;

use App\Controller\BaseController;
use App\Repository\Admin\TiersRepository;
use App\Repository\Reglement\AvoirRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AvoirController
 * @package App\Controller\Etat
 *
 * @Route("/avoir")
 */
class AvoirController extends BaseController
{
    /**
     * Lists all Avoir entities.
     *
     * @Route("/", name="etat_avoir_index")
     *
     * @param TiersRepository $tiersRepository
     *
     * @return Response
     */
    public function index(TiersRepository $tiersRepository)
    {
        $clients = $tiersRepository->findBy(
            ['discriminator' => 'client'],
            ['raisonSociale' => 'ASC', 'nom' => 'ASC']
        );

        return $this->render('etat/avoir/index.html.twig', array(
            'clients' => $clients,
        ));
    }

    /**
     * Liste avoir
     *
     * @Route("/list/{etat}/{id_cli}/{debut}/{fin}",
     *     name="etat_avoir_list",
     *     defaults={
     *     "id_cli" = 0,
     *     "debut" = 0,
     *     "fin" = 0
     *     })
     *
     * @param $etat
     * @param $id_cli
     * @param $debut
     * @param $fin
     * @param AvoirRepository $avoirRepository
     *
     * @return Response
     */
    public function list($etat , $id_cli, $debut, $fin, AvoirRepository $avoirRepository)
    {
        $state = ($etat == 'payee')?true:false;
        $avoirs = $avoirRepository->getByEtaTCliPeriode($state, $id_cli, $debut, $fin);
        return $this->render('etat/avoir/list.html.twig', [
            'avoirs' => $avoirs,
            'id_cli' => (int)$id_cli,
            'etat'   => $etat
        ]);
    }
}
