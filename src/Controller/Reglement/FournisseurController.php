<?php

namespace App\Controller\Reglement;

use App\Controller\BaseController;
use App\Entity\Logs;
use App\Entity\Reglement\Reglement;
use App\Entity\Vente\Vente;
use App\Form\Reglement\ReglementType;
use App\Repository\Admin\TiersRepository;
use App\Repository\Vente\VenteRepository;
use DateTime;
use Exception;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FournisseurController
 * @package App\Controller\Reglement
 *
 * @Route("/fournisseurs")
 */
class FournisseurController extends BaseController
{
    /**
     * @Route("/", name="fournisseur_reglement_index")
     *
     * @param TiersRepository $repository
     *
     * @return Response
     * @throws Exception
     */
    public function indexAction(TiersRepository $repository)
    {
        return $this->render(
            'reglement/fournisseur/index.html.twig',
            [
                'today' => new DateTime(),
                'clients' => $repository->findBy(
                    ['discriminator' => 'fournisseurs', 'typeSoc' => 'Compagnie aériènne']
                ),
            ]
        );
    }

    /**
     * List fournisseurs reglement
     *
     * @Route("/list/{etat}/{id_cie}/{debut}/{fin}",
     *     name="fournisseur_reglement_list",
     *     defaults={
     *     "id_cie" = 0,
     *     "debut" = 0,
     *     "fin" = 0
     *     })
     *
     * @param $etat
     * @param $id_cie
     * @param $debut
     * @param $fin
     *
     * @param VenteRepository $repository
     *
     * @return Response
     */
    public function listAction($etat, $id_cie, $debut, $fin, VenteRepository $repository)
    {
        $state = ($etat == 'reglee') ? true : false;
        /*dump($state);die;*/
        $ventes = $repository->tousVenteParCieParPeriode($state, $id_cie, $debut, $fin);

        return $this->render(
            'reglement/fournisseur/list.html.twig',
            [
                'ventes' => $ventes,
                'etat' => $etat,
                'id_cie' => $id_cie,
                'debut' => $debut,
                'fin' => $fin,
            ]
        );
    }

    /**
     *
     * @Route("/regler/{id_cie}/{debut}/{fin}", name="fournisseur_reglement_regler")
     *
     * @param Request $request
     * @param $debut
     * @param $fin
     * @param $id_cie
     *
     * @param TiersRepository $tiersRepository
     * @param VenteRepository $venteRepository
     *
     * @return Response
     */
    public function reglerAction(
        Request $request,
        $debut,
        $fin,
        $id_cie,
        TiersRepository $tiersRepository,
        VenteRepository $venteRepository
    ) {
        $reglement = new Reglement();
        $em = $this->getDoctrine()->getManager();
        $total = 0;
        $ventes = $venteRepository->tousVenteParCieParPeriode(false, $id_cie, $debut, $fin);
        /** @var Vente $vente */
        foreach ($ventes as $vente) {
            $total = $total + $vente->getMontantTransport() + $vente->getTaxe();
        }
        $fournisseur = $tiersRepository->find($id_cie);
        $reglement->setMontant($total);
        $reglement->setCie($fournisseur);
        $form = $this->createForm(
            ReglementType::class,
            $reglement,
            [
                'action' => $this->generateUrl(
                    'fournisseur_reglement_regler',
                    [
                        'id_cie' => $id_cie,
                        'debut' => $debut,
                        'fin' => $fin,
                    ]
                ),
            ]
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = [];

            foreach ($ventes as $vente) {
                $vente->setReglee(true);
                $em->flush();
            }
            $em->persist($reglement);

            $em->flush();
            $lignes = $reglement->getLignes();
            foreach ($lignes as $ligne) {
                $em->persist($ligne);
                $em->flush();
            }

            $logs = new Logs();
            $logs->setDescription(
                sprintf(
                    "Reglement du fournisseur %s",
                    $fournisseur
                )
            );
            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);

            $data['cible'] = "#paye";

            return new Response(json_encode($data), 201, ['Content-Type' => 'application/json']);
        }

        return $this->render(
            'confirm.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}
