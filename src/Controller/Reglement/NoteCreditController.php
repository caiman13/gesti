<?php

namespace App\Controller\Reglement;


use App\Controller\BaseController;
use App\Manager\LogsManager;
use App\Repository\Admin\TiersRepository;
use App\Repository\Reglement\NoteCreditRepository;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Snappy\Pdf;
use App\Entity\Reglement\NoteCredit;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * NoteCredit controller.
 * @Route("/note-credit")
 */
class NoteCreditController extends BaseController
{
    /**
     * @var TiersRepository
     */
    private $tiersRepository;
    /**
     * @var NoteCreditRepository
     */
    private $creditRepository;

    /**
     * NoteCreditController constructor.
     *
     * @param PaginatorInterface $paginator
     * @param Pdf $pdf
     * @param TiersRepository $tiersRepository
     * @param NoteCreditRepository $creditRepository
     * @param LogsManager $logsManager
     */
    public function __construct(
        PaginatorInterface $paginator,
        Pdf $pdf,
        TiersRepository $tiersRepository,
        NoteCreditRepository $creditRepository,
        LogsManager $logsManager
    ) {
        parent::__construct($paginator, $pdf, $logsManager);
        $this->tiersRepository = $tiersRepository;
        $this->creditRepository = $creditRepository;
    }

    /**
     * Index NoteCredit entities.
     *
     * @Route("/", name="notecredit_index")
     *
     * @return Response
     */
    public function indexAction()
    {
        $clients = $this->tiersRepository->findBy(
            ['discriminator' => 'client'],
            ['raisonSociale' => 'ASC', 'nom' => 'ASC']
        );

        return $this->render(
            'reglement/notecredit/index.html.twig',
            [
                'clients' => $clients,
            ]
        );
    }

    /**
     * List Note de credit
     *
     * @Route("/list/{etat}/{id_cli}/{debut}/{fin}",
     *     name="notecredit_list_etat",
     *     defaults={
     *     "id_cli" = 0,
     *     "debut" = 0,
     *     "fin" = 0
     *     })
     *
     * @param $etat
     * @param $id_cli
     * @param $debut
     * @param $fin
     *
     * @return Response
     */
    public function listAction($etat, $id_cli, $debut, $fin)
    {
        $state = ($etat == 'payee') ? true : false;
        $noteCredits = $this->creditRepository->getByEtaTCliPeriode(
            $state,
            $id_cli,
            $debut,
            $fin
        );

        return $this->render(
            'reglement/notecredit/list.html.twig',
            [
                'noteCredits' => $noteCredits,
                'id_cli' => (int)$id_cli,
                'etat' => $etat,
            ]
        );
    }

    /**
     * Finds and displays a NoteCredit entity.
     *
     * @Route("/{id}/show", name="notecredit_show")
     *
     * @param Request $request
     * @param NoteCredit $noteCredit
     *
     * @return Response
     */
    public function showAction(Request $request, NoteCredit $noteCredit)
    {
        $form = $this->createAvoirForm($noteCredit);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $noteCredit->setPayed(true);
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $data = [];
            $data['cible'] = "#paye";

            return new Response(json_encode($data), 201, ['Content-Type' => 'application/json']);
        }

        return $this->render(
            'confirm.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param NoteCredit $avoir
     *
     * @return FormInterface The form
     */
    public function createAvoirForm(NoteCredit $avoir)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('notecredit_show', ['id' => $avoir->getId()]))
            ->setMethod('POST')
            ->getForm();
    }
}
