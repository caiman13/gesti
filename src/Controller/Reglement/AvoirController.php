<?php

namespace App\Controller\Reglement;


use App\Controller\BaseController;
use App\Entity\Logs;
use App\Form\Reglement\AvoirType;
use App\Entity\Reglement\Avoir;
use App\Manager\LogsManager;
use App\Repository\Admin\LigneComptaRepository;
use App\Repository\Admin\TiersRepository;
use App\Repository\Reglement\AvoirRepository;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Snappy\Pdf;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AvoirController
 * @package App\Controller\Reglement
 *
 * @Route("/avoir")
 */
class AvoirController extends BaseController
{
    /**
     * @var AvoirRepository
     */
    private $repository;

    /**
     * AvoirController constructor.
     *
     * @param PaginatorInterface $paginator
     * @param Pdf $pdf
     * @param AvoirRepository $repository
     * @param LogsManager $logsManager
     */
    public function __construct(
        PaginatorInterface $paginator,
        Pdf $pdf,
        AvoirRepository $repository,
        LogsManager $logsManager
    ) {
        parent::__construct($paginator, $pdf, $logsManager);
        $this->repository = $repository;
    }

    /**
     * Index of avoir
     *
     * @Route("/", name="avoir_index")
     *
     * @param TiersRepository $tiersRepository
     *
     * @return Response
     */
    public function index(TiersRepository $tiersRepository)
    {
        $clients = $tiersRepository->findBy(['discriminator' => 'client'], ['raisonSociale' => 'ASC', 'nom' => 'ASC']);

        return $this->render(
            'reglement/avoir/index.html.twig',
            [
                'clients' => $clients,
            ]
        );
    }

    /**
     * List avoir
     *
     * @Route("/list/{etat}/{id_cli}/{debut}/{fin}",
     *     name="avoir_list_etat",
     *     defaults={
     *     "id_cli" = "0",
     *     "debut" = "0",
     *     "fin" = "0"
     * })
     *
     * @param $etat
     * @param $id_cli
     * @param $debut
     * @param $fin
     *
     * @return Response
     */
    public function listAction($etat, $id_cli, $debut, $fin)
    {
        $em = $this->getDoctrine()->getManager();
        $state = ($etat == 'payee') ? true : false;
        $avoirs = $this->repository->getByEtaTCliPeriode($state, $id_cli, $debut, $fin);

        return $this->render(
            'reglement/avoir/list.html.twig',
            [
                'avoirs' => $avoirs,
                'id_cli' => (int)$id_cli,
                'etat' => $etat,
            ]
        );
    }

    /**
     * Finds and displays a Avoir entity.
     *
     * @Route("/{id}/show", name="avoir_show")
     *
     * @param Request $request
     * @param Avoir $avoir
     *
     * @return Response
     */
    public function showAction(Request $request, Avoir $avoir)
    {
        $form = $this->createAvoirForm($avoir);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $avoir->setPayed(true);
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $data = [];
            $data['cible'] = "#paye";

            return new Response(json_encode($data), 201, ['Content-Type' => 'application/json']);
        }

        return $this->render(
            'confirm.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Edit Avoir
     *
     * @Route("/{id}/edit", name="avoir_modif")
     *
     * @param Request $request
     * @param Avoir $avoir
     * @param LigneComptaRepository $comptaRepository
     *
     * @return Response
     * @throws Exception
     */
    public function modifAction(Request $request, Avoir $avoir, LigneComptaRepository $comptaRepository)
    {
        $avoir->setCieVente();
        $form = $this->createForm(
            AvoirType::class,
            $avoir,
            [
                'action' => $this->generateUrl('avoir_modif', ['id' => $avoir->getId()]),
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $avoir->faireMontant();
            $em->persist($avoir);
            $em->flush();
            $lignes = $comptaRepository->findBy(['refAvoir' => $avoir->getReference()]);

            foreach ($lignes as $ligne) {
                /*$ligne->set*/
                if ($ligne->getMntCredit() == 0) {
                    $ligne->setMntDebit($avoir->getMontant());
                } else {
                    $ligne->setMntCredit($avoir->getMontant());
                }
                $em->flush();
            }

            $facture = $avoir->getFacture();
            $facture->setMontatRembourse($avoir->getMontant());
            $em->persist($facture);
            $em->flush();
            $logs = new Logs();
            $logs->setDescription(
                sprintf(
                    "Modification de l'avoir ref : %s",
                    $avoir->getReference()
                )
            );

            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);

            $data = [];
            $data['cible'] = ($avoir->getPayed()) ? "#paye" : "#non-paye";

            return new Response(json_encode($data), 201, ['Content-Type' => 'application/json']);
        }

        return $this->render(
            'facture/avoir.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Print pdf avoir
     *
     * @Route("/{id}/print-pdf", name="avoir_pdf_print")
     *
     * @param Avoir $avoir
     *
     * @param TiersRepository $tiersRepository
     *
     * @return Response
     */
    public function pdfAction(Avoir $avoir, TiersRepository $tiersRepository)
    {
        $ets = $tiersRepository->find(1);
        $filename = sprintf('avoir-%s.pdf', $avoir->getClient());
        $html = $this->renderView(
            'reglement/avoir/avoir.html.twig',
            [
                'avoir' => $avoir,
                'ets' => $ets,
            ]
        );

        $footer = $this->renderView(
            'footer.html.twig',
            [
                'ets' => $ets,
            ]
        );

        return new Response(
            $this->pdf->getOutputFromHtml($html, ['footer-html' => $footer]),
            200,
            [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            ]
        );
    }

    /**
     * @param Avoir $avoir
     *
     * @return FormInterface The form
     */
    public function createAvoirForm(Avoir $avoir)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('avoir_show', ['id' => $avoir->getId()]))
            ->setMethod('POST')
            ->getForm();
    }
}
