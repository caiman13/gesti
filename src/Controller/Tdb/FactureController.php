<?php

namespace App\Controller\Tdb;

use App\Controller\BaseController;
use App\Repository\Vente\FactureRepository;
use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FactureController
 * @package App\Controller\Tdb
 *
 * @Route("/facture")
 */
class FactureController extends BaseController
{
    /**
     * index of facture tdb
     *
     * @Route("/", name="facture_tdb")
     *
     * @return Response
     * @throws Exception
     */
    public function indexAction()
    {
        return $this->render('tdb/facture.html.twig', array('today' => new DateTime()));
    }

    /**
     * List action on facture tdb
     *
     * @Route("/list/{debut}/{fin}", name="facture_tdb_list",
     *     defaults={"debut" = "0", "fin" = "0"})
     *
     *
     * @param $debut
     * @param $fin
     * @param FactureRepository $factureRepository
     *
     * @return Response
     */
    public function listAction($debut, $fin, FactureRepository $factureRepository)
    {
        $totalannule = count($factureRepository->getAnnuleByDate($debut, $fin));
        $total = $factureRepository->getTotallfactreByperiode($debut, $fin);
        $totalPayed = count($factureRepository->getByDateEtat(true, $debut, $fin));
        $totalnonPayed = count($factureRepository->getByDateEtat(false, $debut, $fin));
        $factureEchu = $factureRepository->getEchuFacture($debut, $fin);
        return $this->render('tdb/facture_list.html.twig', [
            'total' => current($total),
            'totalPayed' => $totalPayed,
            'totalnonPayed' => $totalnonPayed,
            'totalannule'  => $totalannule,
            'factureEchu'  => $factureEchu
        ]);
    }
}
