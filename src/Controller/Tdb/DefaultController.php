<?php

namespace App\Controller\Tdb;

use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 * @package App\Controller\Tdb
 *
 *
 */
class DefaultController extends BaseController
{
    /**
     * index page of tdb
     * @Route("/index", name="tdb_homepage")
     *
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('tdb/default.html.twig', [
            'icon' => 'fa fa-tachometer',
            'title' => 'Tableau de bord'
        ]);
    }

}
