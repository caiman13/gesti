<?php

namespace App\Controller\Tdb;

use App\Controller\BaseController;
use App\Repository\Vente\BonCommandeRepository;
use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BonCommandeController
 * @package App\Controller\Tdb
 *
 *
 * @Route("/bon-commande")
 */
class BonCommandeController extends BaseController
{
    /**
     * Index of bc tdb
     *
     * @Route("/", name="bc_tdb_index")
     *
     * @return Response
     * @throws Exception
     */
    public function index()
    {
        return $this->render(
            'tdb/bc.html.twig',
            [
                'today' => new DateTime(),
            ]
        );
    }

    /**
     * List action on tdb BC
     *
     * @Route("/list/{debut}/{fin}", name="bc_tdb_list",
     *     defaults={"debut" = "0", "fin" = "0"})
     *
     *
     * @param $debut
     * @param $fin
     * @param BonCommandeRepository $bonCommandeRepository
     *
     * @return Response
     */
    public function list($debut, $fin, BonCommandeRepository $bonCommandeRepository)
    {
        $totalBC = $bonCommandeRepository->totalBcPeriode($debut, $fin);
        $etat = ['valide', 'non-valide', 'vendu'];
        $nbrBcByEtat = [];
        $totalBcByEtat = $bonCommandeRepository->totalBcPeriodeEtat($debut, $fin);

        foreach ($etat as $item) {
            $key = array_search($item, array_column($totalBcByEtat,'etat'));

            if ($key !== false) {
                $nbrBcByEtat[$item] = $totalBcByEtat[$key]['totalBc'];
            } else {
                $nbrBcByEtat[$item] = 0;
            }
        }

        $bcsEmisParFrns = $bonCommandeRepository->bcEmmisJoinFournisseurs($debut, $fin);
        $raisonSoc = [];
        $montants = [];
        foreach ($bcsEmisParFrns as $bcsEmisParFrn) {
            $raisonSoc[] = $bcsEmisParFrn['raisonSociale'];
            $montants[] = $bcsEmisParFrn['montant_total'];
        }

        return $this->render(
            'tdb/bc_list.html.twig',
            [
                'totalBc' => $totalBC,
                'totalBcNonValide' => $nbrBcByEtat['non-valide'],
                'totalBcEmis' => $nbrBcByEtat['valide'],
                'totalBctermine' => $nbrBcByEtat['vendu'],
                'bcs' => $bcsEmisParFrns,
                'raisonSoc' => json_encode($raisonSoc),
                'montant' => json_encode($montants),
            ]
        );
    }
}
