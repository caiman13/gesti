<?php

namespace App\Controller\Tdb;

use App\Controller\BaseController;
use App\Repository\Admin\PaiementModeRepository;
use App\Repository\Vente\PaiementRepository;
use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PaiementController
 * @package App\Controller\Tdb
 *
 * @Route("/paiement")
 */
class PaiementController extends BaseController
{
    /**
     * index of paiement tdb
     *
     * @Route("/", name="paiement_tdb")
     *
     * @return Response
     * @throws Exception
     */
    public function index()
    {
        return $this->render('tdb/paiement.html.twig', [
            'today' => new DateTime()
        ]);
    }

    /**
     * List action on paiement tdb
     *
     * @Route("/list/{debut}/{fin}",
     *     name="paiement_tdb_list",
     *     defaults={
     *     "debut" = "0",
     *     "fin" = "0"}
     *     )
     *
     *
     * @param $debut
     * @param $fin
     * @param PaiementRepository $paiementRepository
     *
     * @return Response
     */
    public function listAction($debut, $fin, PaiementRepository $paiementRepository)
    {
        $paiements = $paiementRepository->getByDateEtat(false, $debut, $fin);
        $nbpaiemet = $paiementRepository->getTotalPaementperiode($debut, $fin);
        $paiementByEtat = $paiementRepository->getTotalPaementperiodeEtat($debut, $fin);
        $payNbr = [];
        foreach ($paiementByEtat as $value) {
            dump($value);
            $etat = $value['etat'];
            $payNbr["$etat"] = $value['total'];
        }

        return $this->render('tdb/paiemen_list.html.twig', [
            'paiements' => $paiements,
            'totalPaiement' => current($nbpaiemet),
            'nbpaiementValide' => isset($payNbr['valide'])?$payNbr['valide']: '0',
            'nbpaiemenNonValide' => isset($payNbr['non_valide'])?$payNbr['non_valide']: '0',
        ]);
    }
}
