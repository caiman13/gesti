<?php

namespace App\Controller\Tdb;

use App\Controller\BaseController;
use App\Repository\Vente\VenteRepository;
use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class VenteController
 * @package App\Controller\Tdb
 *
 * @Route("/vente")
 */
class VenteController extends BaseController
{
    /**
     * index of paiement tdb
     *
     * @Route("/", name="vente_tdb")
     *
     * @return Response
     * @throws Exception
     */
    public function indexAction()
    {
        return $this->render('tdb/index_vente.html.twig', [
            'today' => new DateTime(),
        ]);
    }

    /**
     * *List action on vente tdb
     *
     * @Route("/list/{debut}/{fin}", name="vente_tdb_list",
     *     defaults={"debut" = "0", "fin" = "0"})
     *
     *
     * @param $debut
     * @param $fin
     * @param VenteRepository $venteRepository
     *
     * @return Response
     */
    public function listAction($debut, $fin, VenteRepository $venteRepository)
    {
        $ventes = $venteRepository->venteParCieParPeriode($debut,$fin);
        $raisonSoc = array();
        $montants = array();
        foreach ($ventes as $vente){
            $raisonSoc[] = $vente['raisonSociale'];
            $montants[] = $vente['montant_total'];
        }
        /*$raisonSoc = json_encode($raisonSoc);*/
        $ventesTotal = $venteRepository->venteTotalparPeriode($debut,$fin);
        $ventesFacture = $venteRepository->venteTotalparPeriodeParEtat($debut,$fin, 'facture');
        $ventesNonFacture = $venteRepository->venteTotalparPeriodeParEtat($debut,$fin, 'non-facture');
        /*dump($ventes); die;*/
        return $this->render('tdb/vente.html.twig', [
            'ventes' => $ventes,
            'ventesTotal' => current($ventesTotal),
            'ventesFacture' => current($ventesFacture),
            'ventesNonFacture' => current($ventesNonFacture),
            'raisonSoc' => json_encode($raisonSoc),
            'montant'  => json_encode($montants)
        ]);
    }
}
