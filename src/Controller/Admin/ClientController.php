<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Repository\Admin\TiersRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ClientController
 * @package App\Controller\Admin
 *
 *
 */
class ClientController extends BaseController
{
    /**
     * @Route("/client", name="admin_client")
     * @param TiersRepository $tiersRepository
     * @return Response
     */
    public function index(TiersRepository $tiersRepository)
    {
        $clients = $tiersRepository->findBy(
            ['discriminator' => 'client'],
            ['raisonSociale' => 'ASC', 'nom' => 'ASC']
        );

        return $this->render('admin/client/index.html.twig', array(
            'clients' => $clients,
            'title'   => 'Les clients',
            'icon'    => 'fa fa-id-badge'
        ));
    }
}
