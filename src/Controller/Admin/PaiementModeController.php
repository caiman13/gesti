<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Repository\Admin\PaiementModeRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Admin\PaiementMode;
use App\Form\Admin\PaiementModeType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Paiement_mode controller.
 *
 */
class PaiementModeController extends BaseController
{
    /**
     * Lists all Paiement_mode entities.
     *
     * @Route("/paiement-mode", name="paiement_mode_index")
     *
     *
     * @param PaiementModeRepository $paiement_modeRepository
     *
     * @return Response
     */
    public function indexAction(PaiementModeRepository $paiement_modeRepository)
    {

        $paiement_modes = $paiement_modeRepository->findAll();

        return $this->render('admin/paiement_mode/index.html.twig', array(
            'paiement_modes' => $paiement_modes,
        ));
    }

    /**
     * Creates a new Paiement_mode entity.
     *
     * @Route("/paiement-mode/new", name="paiement_mode_new", methods={"GET", "POST"})
     *
     * @param Request $request
     * @return Response|JsonResponse
     */
    public function newAction(Request $request)
    {
        $paiement_mode = new PaiementMode();
        $form = $this->createForm(PaiementModeType::class, $paiement_mode, [
            'action' => $this->generateUrl('paiement_mode_new')
        ]);
        $form->handleRequest($request);
        $data = array();
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($paiement_mode);
            $em->flush();

            $data[self::CILBLE_INDEX ] = "#paiement";
            $this->addFlash(
                self::INFO_FLASH_TYPE,
                self::AJOUT_SUCCESS_MESSAGE
            );

            return $this->json($data);
        }

        return $this->render('new.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing Paiement_mode entity.
     *
     * @Route("/paiement-mode/{id}", name="paiement_mode_edit", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param PaiementMode $paiement_mode
     *
     * @return Response
     */
    public function editAction(Request $request, PaiementMode $paiement_mode)
    {
        $editForm = $this->createForm(PaiementModeType::class, $paiement_mode, [
            'action' =>$this->generateUrl('paiement_mode_edit',['id' => $paiement_mode->getId()])
        ]);
        $editForm->handleRequest($request);
        $data = array();
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($paiement_mode);
            $em->flush();

            $data[self::CILBLE_INDEX ] = "#paiement";
            $this->addFlash(
                self::INFO_FLASH_TYPE,
                self::MODIF_SUCCESS_MESSAGE
            );

            return $this->json($data);
        }

        return $this->render('new.html.twig', array(
            'paiement_mode' => $paiement_mode,
            'form' => $editForm->createView(),
        ));
    }

}
