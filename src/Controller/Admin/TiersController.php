<?php
/**
 * Created by PhpStorm.
 * User: houlder
 * Date: 3/2/19
 * Time: 8:34 PM
 */

namespace App\Controller\Admin;


use App\Controller\BaseController;
use App\Form\Admin\TiersType;
use App\Repository\Admin\TiersRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Admin\Tiers;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TiersController
 * @package App\Controller\Admin
 *
 * @Route("/tiers")
 */
class TiersController extends BaseController
{

    const NEW_CLIENT_TEMPLATE = 'admin/client/new.html.twig';

    /**
     * @param FormInterface $form
     * @param string $discriminator
     *
     * @return FormInterface
     */
    private function setFormBySiscriminator(FormInterface $form, string $discriminator)
    {
        if ("client" === $discriminator) {
            $form->add(
                'auteCompte',
                TextType::class,
                [
                    'attr' => [
                        'class' => '',
                    ],
                    'label' => 'Plan de compte CLIENT',
                    'required' => false,
                ]
            );
        } elseif ("fournisseurs" === $discriminator) {
            $form->add(
                'numCompte',
                TextType::class,
                [
                    'attr' => [
                        'class' => '',
                    ],
                    'label' => 'Plan de compte',
                    'required' => false,
                ]
            );
        }

        return $form;
    }

    /**
     * @Route("/index/{discriminator}", name="tiers_index")
     *
     * @param $discriminator
     *
     * @return Response
     */
    public function index($discriminator)
    {
        $title = ($discriminator == "client") ? "Les ".$discriminator."s" : "Les ".$discriminator;
        $icon = ($discriminator == "client")?"fa fa-id-badge":"fa fa-group";
        return $this->render('admin/client/index.html.twig', array(
            'dicriminator' => $discriminator,
            'title' => $title,
            'icon'    => $icon
        ));
    }

    /**
     * @Route("/lier-cie/{id}/compagnie-aerienne", name="tiers_link", methods={"GET","POST"})
     *
     * @param Request $request
     * @param Tiers $tiers
     * @return Response
     */
    public function lierAction(Request $request, Tiers $tiers)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createFormBuilder()
            ->add('agv', EntityType::class, [
                'class' => 'App\Entity\Admin\Tiers',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->where('t.typeSoc = ?1')
                        ->setParameter(1,'Agence de Voyage');
                },
                'attr' => [
                    'class' => 'field'
                ],
                'label' => 'Agence de voyage'
            ])
            ->setAction($this->generateUrl('tiers_link', ['id' => $tiers->getId()]))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            // data is an array with "name", "email", and "message" keys
            $data = $form->getData();
            $tiers->setSociete($data['agv']);
            $tiers->setLinked(true);
            $em->persist($tiers);
            $em->flush();
            $data[self::CILBLE_INDEX ] = "#".$tiers->getType();
            $this->addFlash(
                self::INFO_FLASH_TYPE,
                self::AJOUT_SUCCESS_MESSAGE
            );
            return $this->json($data);
        }

        return $this->render('admin/client/lier_form.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{discriminator}/{type}", name="tiers_list",
     *     requirements={
     *         "discriminator": "client|fournisseurs",
     *         "type": "personne|societe|cie_ae|agc_voy"
     *     },
     *     methods={"GET"}
     *     )
     *
     * @param TiersRepository $tiersRepository
     * @param $discriminator
     * @param $type
     * @return Response
     */
    public function listAction(TiersRepository $tiersRepository,$discriminator, $type)
    {
        $data = array();
        $data["discriminator"] = $discriminator;
        $data_cond = array();
        $data_cond["discriminator"] = $discriminator;
        if($type == 'cie_ae'){
            $data_cond['typeSoc'] = "Compagnie aériènne";
            $data["type_soc"] = $type;
            $type_retour = 'societe';
        } else if ($type == 'agc_voy'){
            $data_cond['typeSoc'] = "Agence de Voyage";
            $data["type_soc"] = $type;
            $type_retour = 'societe';
        } else {
            $data_cond['type'] = $type;
            $type_retour = $type;
        }
        $tiers = $tiersRepository->findBy(
            $data_cond,
            ['raisonSociale' => 'ASC', 'nom' => 'ASC']
        );

        $data["type"] = $type_retour;
        $data["lesTiers"] = $tiers;

        return $this->render('admin/client/list.html.twig', $data );

    }

    /**
     *
     * @Route("/new/{discriminator}", name="tiers_new",
     *     requirements={"discriminator": "client|fournisseurs"}
     *     )
     *
     * Creates a new Tiers entity.
     * @param Request $request
     * @param $discriminator
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request , $discriminator)
    {
        $tiers = new Tiers();
        $form = $this->createForm(TiersType::class, $tiers,[
            'action' => $this->generateUrl('tiers_new', [
                'discriminator' => $discriminator
            ])
        ]);

        $form = $this->setFormBySiscriminator($form, $discriminator);

        $form->handleRequest($request);
        $data = array();
        if ($form->isSubmitted()) {
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $tiers->setDiscriminator($discriminator);
                $tiers->setRaisonSociale(strtoupper($tiers->getRaisonSociale()));
                /*if ($discriminator == 'client'){
                    if ($tiers->getTypeCli() === 'autre'){
                        $tiers->setNumCompte('41160000');
                    } else {
                        $tiers->setNumCompte('41160000');
                    }
                }*/
                /*dump($tiers); die;*/
                $em->persist($tiers);
                $em->flush();
                /*dump($tiers); die;*/
                $data[self::CILBLE_INDEX ] = "#".$tiers->getType();
                $this->addFlash(
                    self::INFO_FLASH_TYPE,
                    self::AJOUT_SUCCESS_MESSAGE
                );

                return $this->json($data);
            } else {
                $this->addFlash(
                    self::DANGER_FLASH_TYPE,
                    self::ERROR_MESSAGE
                );
            }
        }

        return $this->render(self::NEW_CLIENT_TEMPLATE, array(
            'edit' => "false",
            'form' => $form->createView(),
            'discriminator' => $discriminator
        ));
    }

    /**
     *
     * @Route("/new-ajax/{discriminator}", name="tiers_ajax_add",
     *     requirements={"discriminator": "client|fournisseurs"},
     *     defaults={"discriminator": "client"}
     *     )
     *
     * @param Request $request
     * @param $discriminator
     * @return Response
     *
     */
    public function newAjax(Request $request, $discriminator, TiersRepository $repository)
    {
        $tiers = new Tiers();
        $form = $this->createForm(
            TiersType::class,
            $tiers,
            [
                'action' => $this->generateUrl('tiers_ajax_add'),
            ]
        );

        $form = $this->setFormBySiscriminator($form, $discriminator);

        $form->handleRequest($request);
        $data = [];
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $tiers->setDiscriminator($discriminator);
                $tiers->setRaisonSociale(strtoupper($tiers->getRaisonSociale()));
                /*dump($tiers); die;*/
                $em->persist($tiers);
                $em->flush();
                $data[self::CILBLE_INDEX ] = "#".$tiers->getType();
                $this->addFlash(
                    self::INFO_FLASH_TYPE,
                    self::AJOUT_SUCCESS_MESSAGE
                );
                return $this->render('vente/cli_ajax.html.twig', [
                    'id_cli' => $tiers->getId(),
                    'clients' => $repository->findBy(['discriminator' => $discriminator])
                ]);
            } else {
                $this->addFlash(
                    self::DANGER_FLASH_TYPE,
                    self::ERROR_MESSAGE
                );
            }
        }

        return $this->render(
            'admin/client/new.html.twig',
            array(
                'edit' => "false",
                'form' => $form->createView(),
                'discriminator' => $discriminator,
            ));
    }


    /**
     *
     * @Route("/edit/{discriminator}/{id}", name="tiers_edit",
     *     requirements={"discriminator": "client|fournisseurs"}
     *     )
     *
     * Displays a form to edit an existing Tiers entity.
     * @param Request $request
     * @param Tiers $tier
     * @param $discriminator
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Tiers $tier, $discriminator)
    {
        $editForm = $this->createForm(
            TiersType::class,
            $tier,
            [
                'action' => $this->generateUrl(
                    'tiers_edit',
                    [
                        'id' => $tier->getId(),
                        'discriminator' => $discriminator,
                    ]
                ),
            ]
        );

        if ("client" === $discriminator) {
            $editForm->add(
                'auteCompte',
                TextType::class,
                [
                    'attr' => [
                        'class' => '',
                    ],
                    'label' => 'Plan de compte CLIENT',
                    'required' => false,
                ]
            );
        } elseif ("fournisseurs" === $discriminator) {
            $editForm->add(
                'numCompte',
                TextType::class,
                [
                    'attr' => [
                        'class' => '',
                    ],
                    'label' => 'Plan de compte',
                    'required' => false,
                ]
            );
        }


        $editForm->handleRequest($request);

        $data = [];
        if ($editForm->isSubmitted()) {
            if ($editForm->isValid()) {
                $em = $this->getDoctrine()->getManager();
                /*            dump($tier); die;*/
                $tier->setRaisonSociale(strtoupper($tier->getRaisonSociale()));
                $em->persist($tier);
                $em->flush();
                $data[self::CILBLE_INDEX ] = "#".$tier->getType();
                $this->addFlash(
                    self::INFO_FLASH_TYPE,
                    self::MODIF_SUCCESS_MESSAGE
                );

                return $this->json($data);
            } else {
                $this->addFlash(
                    self::DANGER_FLASH_TYPE,
                    self::ERROR_MESSAGE
                );
            }
        }

        return $this->render(self::NEW_CLIENT_TEMPLATE, array(
            'edit' => "true",
            'form' => $editForm->createView(),
            'discriminator' => $discriminator
        ));
    }

    /**
     * @Route("/delete/{id}", name="tiers_delete")
     *
     * Deletes a Tiers entity.
     * @param Request $request
     * @param Tiers $tier
     * @return Response
     */
    public function deleteAction(Request $request, Tiers $tier)
    {
        $form = $this->createDeleteForm($tier);
        $disc = $tier->getDiscriminator();
        $type = $tier->getType();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tier);
            $em->flush();
            $this->addFlash(
                self::INFO_FLASH_TYPE,
                'Votre '.$disc.' a été supprimé avec succée'
            );
        } else {
            $this->addFlash(
                self::DANGER_FLASH_TYPE,
                'Une erreur s\'est produit lors de la suppression'
            );
        }

        $data = array();
        $data[self::CILBLE_INDEX ] = "#".$type;

        return $this->json($data);

        /*        if ($form->isSubmitted() && $form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($tier);
                    $em->flush();
                }

                return $this->redirectToRoute('tiers_index');*/
    }

    /**
     * Creates a form to delete a Tiers entity.
     *
     * @param Tiers $tier The Tiers entity
     *
     * @return FormInterface The form
     */
    private function createDeleteForm(Tiers $tier)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tiers_delete', array('id' => $tier->getId())))
            ->setMethod('POST')
            ->getForm()
            ;
    }

    /**
     *
     * @Route("/confirm/{id}", name="tiers_confirm")
     *
     * @param Tiers $tiers
     * @return Response
     */
    public function confirmAction(Tiers $tiers)
    {
        $form = $this->createDeleteForm($tiers);
        return $this->render('confirm.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     *
     * @Route("/add-cie/{id}/agence-de-voyage", name="tiers_ajouter_societe")
     *
     * @param Request $request
     * @param Tiers $tiers
     * @param TiersRepository $tiersRepository
     *
     * @return Response
     */
    public function addSocAction(Request $request, Tiers $tiers, TiersRepository $tiersRepository)
    {
        $em = $this->getDoctrine()->getManager();

        $agv = $tiersRepository->findBy(
            ['typeSoc' => "Compagnie aériènne", 'linked' => false],
            ['raisonSociale' => 'ASC', 'nom' => 'ASC']
        );
        $agv_choice = array();
        foreach ($agv as $value){
            $agv_choice[$value->getRaisonSociale()] = $value->getId();
        }
        $form = $this->createFormBuilder()
            ->add('cie_ae', ChoiceType::class,[
                'choices'   => $agv_choice,
                'attr' => [
                    'class' => 'field chosen-select tag-input-style',
                    'data-placeholder' => 'Choisissez la compagnie aériènne'
                ],
                'multiple' => true,
                'label' => 'Agence de voyage'
            ])
            ->setAction($this->generateUrl('tiers_ajouter_societe', ['id' => $tiers->getId()]))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            // data is an array with "name", "email", and "message" keys
            $data = $form->getData();

            foreach ($data['cie_ae'] as $value){
                $cie = $tiersRepository->find((int)$value);
                $cie->setSociete($tiers);
                $cie->setLinked(true);
                $em->persist($cie);
            }
            $em->flush();
            $data[self::CILBLE_INDEX ] = "#".$tiers->getType();
            $this->addFlash(
                self::INFO_FLASH_TYPE,
                self::AJOUT_SUCCESS_MESSAGE
            );

            return $this->json($data);
        }
        return $this->render('admin/client/lier_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}