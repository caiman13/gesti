<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Repository\Admin\CurrencyRepository;
use App\Repository\Admin\FeesRepository;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Admin\Fees;
use App\Form\Admin\FeesType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Fees controller.
 *
 */
class FeesController extends BaseController
{
    /**
     * Lists all Fees entities.
     *
     * @Route("/fees-index", name="fees_index")
     *
     * @param CurrencyRepository $currencyRepository
     * @param FeesRepository $feesRepository
     * @return Response
     */
    public function indexAction(CurrencyRepository $currencyRepository, FeesRepository $feesRepository)
    {
        $currency = $currencyRepository->findOneBy(['active' => true]);

        $fees = $feesRepository->findAll();

        return $this->render('admin/fees/index.html.twig', array(
            'fees' => $fees,
            'currency' => $currency->getCodeIso()
        ));
    }

    /**
     * Creates a new Fees entity.
     *
     * @Route("/fees-new", name="fees_new", methods={"GET","POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $fee = new Fees();
        $form = $this->createForm(FeesType::class, $fee, [
            'action' => $this->generateUrl('fees_new')
        ]);
        $form->handleRequest($request);
        $data = array();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($fee);
            $em->flush();
            $data[self::CILBLE_INDEX ] = "#fees";
            $this->addFlash(
                self::INFO_FLASH_TYPE,
                self::AJOUT_SUCCESS_MESSAGE
            );

            return $this->json($data);
        }

        return $this->render('admin/fees/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Fees entity.
     *
     * @Route("/fees-show/{id}", name="fees_show", defaults={"id" = 0})
     *
     * @param Fees $fee
     * @return Response
     */
    public function showAction(Fees $fee)
    {
        return $this->render('admin/fees/show.html.twig', array(
            'fee' => $fee,
        ));
    }

    /**
     * Displays a form to edit an existing Fees entity.
     *
     * @Route("/fees-edit/{id}", name="fees_edit", methods={"GET","POST"})
     *
     * @param Request $request
     * @param Fees $fee
     * @return Response
     */
    public function editAction(Request $request, Fees $fee)
    {
        $editForm = $this->createForm(FeesType::class, $fee, [
            'action' => $this->generateUrl('fees_edit',['id' => $fee->getId()])
        ]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fee);
            $em->flush();
            $data[self::CILBLE_INDEX ] = "#fees";
            $this->addFlash(
                self::INFO_FLASH_TYPE,
                self::MODIF_SUCCESS_MESSAGE
            );

            return $this->json($data);
        }

        return $this->render('admin/fees/new.html.twig', array(
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Fees entity.
     *
     * @Route("/fees-delete/{id}", name="fees_delete")
     *
     * @param Request $request
     * @param Fees $fee
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Fees $fee)
    {
        $form = $this->createDeleteForm($fee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fee);
            $em->flush();
        }

        return $this->redirectToRoute('fees_index');
    }

    /**
     * Creates a form to delete a Fees entity.
     *
     * @param Fees $fee The Fees entity
     *
     * @return FormInterface
     */
    private function createDeleteForm(Fees $fee)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('fees_delete', array('id' => $fee->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
