<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Form\Admin\TVAType;
use App\Entity\Admin\TVA;
use App\Repository\Admin\TVARepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * TVA controller.
 *
 */
class TVAController extends BaseController
{
    /**
     * Lists all TVA entities.
     *
     * @Route("/tva", name="tva_index")
     *
     * @param TVARepository $repository
     * @return Response
     */
    public function indexAction(TVARepository $repository)
    {
        $tVAs = $repository->findAll();

        return $this->render('admin/tva/index.html.twig', array(
            'tVAs' => $tVAs,
        ));
    }

    /**
     * Edit tva value
     *
     * @Route("/tva/{id}", name="tva_edit")
     *
     * @param Request $request
     * @param TVA $tier
     * @return Response|JsonResponse
     */
    public function editAction(Request $request, TVA $tier)
    {
        $editForm = $this->createForm(TVAType::class, $tier,[
            'action' => $this->generateUrl('tva_edit', [
                'id' => $tier->getId(),
            ])
        ]);

        $editForm->handleRequest($request);
        $data = array();
        if ($editForm->isSubmitted()) {
            if($editForm->isValid()){
                $em = $this->getDoctrine()->getManager();

                $em->persist($tier);
                $em->flush();
                $data[self::CILBLE_INDEX ] = "#tva";
                $this->addFlash(
                    self::INFO_FLASH_TYPE,
                    self::MODIF_SUCCESS_MESSAGE
                );

                return $this->json($data);
            } else {
                $this->addFlash(
                    self::DANGER_FLASH_TYPE,
                    self::ERROR_MESSAGE
                );
            }
        }

        return $this->render('new.html.twig', array(
            'form' => $editForm->createView(),
        ));
    }
}
