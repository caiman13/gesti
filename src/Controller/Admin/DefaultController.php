<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends BaseController
{
    /**
     * Default page for params
     *
     * @Route("/param", name="admin_param")
     *
     * @return Response
     */
    public function paramAction()
    {
        return $this->render('admin/Default/param.html.twig', array(
            'title' => 'Paramètre',
            'icon' => 'fa fa-wrench'
        ));
    }
}
