<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Repository\Admin\CurrencyRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Admin\Currency;
use App\Form\Admin\CurrencyType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Currency controller.
 *
 * @Route("/currency")
 *
 */
class CurrencyController extends BaseController
{
    protected $monaie = "#monaie";


    /**
     * Lists all Currency entities.
     *
     * @Route("/", name="currency_index")
     *
     * @param CurrencyRepository $repository
     * @return Response
     */
    public function indexAction(CurrencyRepository $repository)
    {
        $currencies = $repository->findAll();
        return $this->render('admin/currency/index.html.twig', array(
            'currencies' => $currencies,
        ));
    }

    /**
     * Creates a new Currency entity.
     *
     * @Route("/new", name="currency_new")
     *
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $currency = new Currency();
        $form = $this->createForm(CurrencyType::class, $currency,[
            'action' => $this->generateUrl('currency_new')
        ]);
        $form->handleRequest($request);
        $data = array();
        if ($form->isSubmitted()) {
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($currency);
                $em->flush();
                $data[self::CILBLE_INDEX] = $this->monaie;
                $this->addFlash(
                    self::INFO_FLASH_TYPE,
                    self::AJOUT_SUCCESS_MESSAGE
                );

                return $this->json($data);
            } else {
                $this->addFlash(
                    self::DANGER_FLASH_TYPE,
                    self::ERROR_MESSAGE
                );
            }
        }

        return $this->render('new.html.twig', array(
            'form' => $form->createView(),
        ));
    }



    /**
     * Displays a form to edit an existing Currency entity.
     *
     * @Route("/{code_iso}/edit", name="currency_edit", methods={"GET", "POST"})
     *
     *
     * @param Request $request
     * @param Currency $currency
     * @return Response
     */
    public function editAction(Request $request, Currency $currency)
    {
        $editForm = $this->createForm(CurrencyType::class, $currency,[
            'action' => $this->generateUrl('currency_edit', [
                'code_iso' => $currency->getCodeIso(),
            ])
        ]);
        $editForm->handleRequest($request);
        $data = array();
        if ($editForm->isSubmitted()) {
            if($editForm->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($currency);
                $em->flush();
                $data[self::CILBLE_INDEX ] = $this->monaie;
                $this->addFlash(
                    self::INFO_FLASH_TYPE,
                    self::MODIF_SUCCESS_MESSAGE
                );

                return $this->json($data);
            } else {
                $this->addFlash(
                    self::DANGER_FLASH_TYPE,
                    self::ERROR_MESSAGE
                );
            }
        }


        return $this->render('new.html.twig', array(
            'edit' => "true",
            'form' => $editForm->createView(),
        ));
    }


    /**
     * @param Currency $currency
     * @return mixed
     */
    private function createActiveForm(Currency $currency)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('currency_activate', array('code_iso' => $currency->getCodeIso())))
            ->setMethod('POST')
            ->getForm()
            ;
    }

    /**
     *
     * @Route("/{code_iso}/confirm", name="currency_confirm")
     *
     * @param Currency $currency
     *
     * @return Response
     */
    public function confirmAction(Currency $currency)
    {
        $form = $this->createActiveForm($currency);
        return $this->render('admin/currency/confirm.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * Activate currency
     *
     * @Route("/{code_iso}/activate", name="currency_activate")
     *
     * @param Request $request
     * @param Currency $currency
     * @param CurrencyRepository $repository
     *
     * @return Response
     */
    public function activeAction(Request $request,Currency $currency, CurrencyRepository $repository)
    {
        $form = $this->createActiveForm($currency);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $currencies = $repository->findAll();
            foreach ($currencies as $value){
                $value->setActive(false);
            }
            $currency->setActive(true);
            $em->flush();
            $this->addFlash(
                self::INFO_FLASH_TYPE,
                ''.$currency->getCodeIso().' a été choisi comme votre unité monétaire'
            );
        } else {
            $this->addFlash(
                self::DANGER_FLASH_TYPE,
                self::ERROR_MESSAGE
            );
        }

        $data = array();
        $data[self::CILBLE_INDEX ] = $this->monaie;

        return $this->json($data);
    }

}
