<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Admin\Logo;
use App\Entity\Admin\Tiers;
use App\Form\Admin\TiersType;
use App\Repository\Admin\TiersRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GeneralInfoController extends BaseController
{
    /**
     *
     * @Route("/general-index", name="general_index")
     *
     * @param TiersRepository $tiersRepository
     * @return Response
     */
    public function indexAction(TiersRepository $tiersRepository)
    {
        return $this->render(
            'admin/general/index.html.twig',
            [
                'client' => $tiersRepository->findOneBy(
                    ['discriminator' => 'generale'],
                    ['raisonSociale' => 'ASC', 'nom' => 'ASC']
                ),
            ]
        );
    }

    /**
     *
     * @Route("/general-modif/{id}",
     *     name="general_modif",
     *     methods={"GET","POST"},
     *     defaults={"id" = "1"}
     *     )
     *
     * @param Request $request
     * @param Tiers $tiers
     * @return Response
     */
    public function modif(Request $request, Tiers $tiers)
    {
        $form = $this->createForm(TiersType::class, $tiers,[
            'action' => $this->generateUrl('general_modif')
        ]);
        $form->handleRequest($request);
        $data = array();
        if ($form->isSubmitted()) {
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                /*dump($tiers); die;*/
                $em->persist($tiers);
                $em->flush();
                $data[self::CILBLE_INDEX ] = "#generale_info";
                $this->addFlash(
                    self::INFO_FLASH_TYPE,
                    'Mofification effectué avec succée!'
                );

                return $this->json($data);
            } else {
                $this->addFlash(
                    self::DANGER_FLASH_TYPE,
                    self::ERROR_MESSAGE
                );
            }
        }

        return $this->render('admin/client/new.html.twig', array(
            'edit' => "false",
            'form' => $form->createView(),
            'discriminator' => $tiers->getDiscriminator()
        ));
    }

    /**
     *
     * @Route("/general-modif-logo/{id}",
     *     name="general_modif_logo",
     *     methods={"GET","POST"},
     *     defaults={"id" = "1"}
     *     )
     *
     * @param Request $request
     * @param Tiers $tiers
     * @return Response
     */
    public function modifLogo(Request $request, Tiers $tiers)
    {
        if($request->isMethod('POST')){
            /*dump($request->files); die;*/
            $file = $request->files->get('file');
            $fileName = $file->getClientOriginalName().$file->guessExtension();
            /*dump($this->getParameter('logo_directory')); die;*/
            $file->move(
                $this->getParameter('logo_directory'),
                $fileName
            );
            $logo = new Logo();
            $logo->setFilename($fileName);
            $tiers->setLogo($logo);
            $em = $this->getDoctrine()->getManager();
            /*dump($tiers); die;*/
            $em->persist($tiers);
            $em->flush();
            $data[self::CILBLE_INDEX ] = "#generale_info";
            $this->addFlash(
                self::INFO_FLASH_TYPE,
                'Mofification effectué avec succée!'
            );

            return $this->json($data);
        }
        return $this->render('admin/general/logo.html.twig',[
            'client' => $tiers,
            'action' => $this->generateUrl('general_modif_logo')
        ]);
    }
}
