<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Admin\TauxCom;
use App\Form\Admin\TauxComType;
use App\Repository\Admin\TauxComRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TauxComController extends BaseController
{
    /**
     * Index of taux comm
     *
     * @Route("/taux-com", name="taux_index")
     *
     * @param TauxComRepository $repository
     * @return Response
     */
    public function indexAction(TauxComRepository $repository)
    {
        $tVAs = $repository->findAll();

        return $this->render('admin/taux/index.html.twig', array(
            'tVAs' => $tVAs,
        ));
    }

    /**
     * Edit taux com
     *
     * @Route("/taux-com/{id}", name="taux_edit", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param TauxCom $tier
     * @return Response|JsonResponse
     */
    public function editAction(Request $request, TauxCom $tier)
    {
        $editForm = $this->createForm(TauxComType::class, $tier,[
            'action' => $this->generateUrl('taux_edit', [
                'id' => $tier->getId(),
            ])
        ]);

        $editForm->handleRequest($request);
        $data = array();
        if ($editForm->isSubmitted()) {
            if($editForm->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($tier);
                $em->flush();
                $data[self::CILBLE_INDEX ] = "#taux_com";
                $this->addFlash(
                    self::INFO_FLASH_TYPE,
                    self::MODIF_SUCCESS_MESSAGE
                );

                return $this->json($data);
            } else {
                $this->addFlash(
                    self::DANGER_FLASH_TYPE,
                    self::ERROR_MESSAGE
                );
            }
        }

        return $this->render('new.html.twig', array(
            'form' => $editForm->createView(),
        ));
    }
}
