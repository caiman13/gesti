<?php
/**
 * Created by PhpStorm.
 * User: houlder
 * Date: 10/7/18
 * Time: 8:45 AM
 */

namespace App\Controller;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends BaseController
{
    /**
     * @Route("/", name="homepage")
     * @return Response
     */
    public function index()
    {
        return $this->render('default/index.html.twig', array(
            'class_based' => 'index-page'
        ));
    }
}