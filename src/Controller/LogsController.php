<?php

namespace App\Controller;

use App\Entity\User;
use App\Manager\LogsManager;
use App\Repository\Admin\TiersRepository;
use App\Repository\UserRepository;
use DateTime;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Snappy\Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/logs")
 *
 * Class LogsController
 * @package App\Controller
 */
class LogsController extends BaseController
{
    /**
     * @var LogsManager
     */
    private $manager;

    public function __construct(PaginatorInterface $paginator, Pdf $pdf, LogsManager $manager)
    {
        parent::__construct($paginator, $pdf, $manager);
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="logs_index")
     * @param TiersRepository $tiersRepository
     *
     * @return Response
     * @throws Exception
     */
    public function index(UserRepository $userRepository)
    {
        $users = $userRepository->findAll();

        return $this->render(
            'logs/index.html.twig',
            [
                'users' => $users,
                'today' => new DateTime(),
            ]
        );
    }

    /**
     * @Route("/list/{userId}/{debut}/{fin}",
     *     name="logs_list",
     *     defaults={
     *     "userId" = 0,
     *     "debut" = 0,
     *     "fin" = 0
     *     })
     */
    public function list($userId, $debut, $fin, UserRepository $userRepository)
    {
        $user = ($userId == 0) ? null : $userRepository->find($userId);
        $logs = $this->manager->getUserAndDate($debut, $fin, $user);

        return $this->render(
            'logs/list.html.twig',
            [
                'logs' => $logs,
            ]
        );
    }
}
