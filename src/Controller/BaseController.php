<?php
/**
 * Description :
 * Date: 21/09/2018
 * Time: 14:44
 */

namespace App\Controller;


use App\Manager\LogsManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use  Knp\Snappy\Pdf;

class BaseController extends AbstractController
{

    const CILBLE_INDEX = 'cible';

    const ERROR_MESSAGE = 'Une erreur s\'est produit';

    const DANGER_FLASH_TYPE = 'danger';

    const INFO_FLASH_TYPE = 'info';

    const AJOUT_SUCCESS_MESSAGE = 'Ajout effectué avec succée!';

    const MODIF_SUCCESS_MESSAGE = 'Modification effectué avec succée!';

    /**
     * @var array
     */
    protected $data = array();


    /**
     * @var Pagin$pdfatorInterface|null
     */
    protected $paginator = null;

    /**
     * @var Pdf
     */
    protected $pdf;

    /**
     * @var LogsManager
     */
    protected $logsManager;

    /**
     * BaseController constructor.
     *
     * @param PaginatorInterface $paginator
     * @param Pdf $pdf
     * @param LogsManager $logsManager
     */
    public function __construct(PaginatorInterface $paginator, Pdf $pdf, LogsManager $logsManager)
    {
        $this->paginator = $paginator;
        $this->pdf = $pdf;
        $this->logsManager = $logsManager;
    }

    /**
     * @param string $view
     * @param array $parameters
     * @param Response|null $response
     * @return Response
     */
    public function render(
        string $view,
        array $parameters = array(),
        Response $response = null
    ): Response {

        $dataReturn = array_merge($this->data, $parameters);

        return parent::render($view, $dataReturn);
    }
}