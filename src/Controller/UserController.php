<?php

namespace App\Controller;

use App\Form\UserEditPasswordType;
use App\Manager\LogsManager;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Snappy\Pdf;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;

/**
 * User controller.
 *
 * @Route("/user")
 *
 */
class UserController extends BaseController
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * UserController constructor.
     *
     * @param PaginatorInterface $paginator
     * @param Pdf $pdf
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param LogsManager $logsManager
     */
    public function __construct(
        PaginatorInterface $paginator,
        Pdf $pdf,
        UserPasswordEncoderInterface $passwordEncoder,
        LogsManager $logsManager
    ) {
        parent::__construct($paginator, $pdf, $logsManager);
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Lists all User entities.
     *
     * @Route("/", name="user_index")
     *
     * @param UserRepository $repository
     *
     * @return Response
     */
    public function index(UserRepository $repository)
    {
        $users = $repository->findAll();
/*        dump($users); die;*/

        return $this->render('user/index.html.twig', array(
            'users' => $users,
            'title' => 'Les utilisateurs',
            'icon' => 'fa fa-user-plus'
        ));
    }

    /**
     *
     * Re-init User
     *
     * @Route("/{id}/init", name="user_reinitialize")
     *
     * @param Request $request
     * @param User $user
     *
     * @return Response
     */
    public function initAction(Request $request, User $user)
    {
        $form = $this->initUserForm($user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $user->setPlainPassword('123456789');

            $password = $this->passwordEncoder->encodePassword($user, $user->getPlainPassword());

            $user->setPassword($password);

            $em->persist($user);
            $em->flush();
            $data = array('cible' => '#users_list');
            $this->addFlash(
                'info',
                'Le nouveau mot de passe est : "'.$user->getPlainPassword().'"'
            );

            return new Response(json_encode($data),201,array('Content-Type'=>'application/json'));
        }
        return $this->render('confirm.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Creates a form to delete a User entity.
     *
     * @param User $user The User entity
     *
     * @return FormInterface The form
     */
    private function initUserForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_reinitialize', array('id' => $user->getId())))
            ->setMethod('POST')
            ->getForm()
            ;
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/new", name="user_new")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user, [
            'action' => $this->generateUrl('user_new'),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /*dump($user); die;*/

            $password = $this->passwordEncoder->encodePassword($user, $user->getPlainPassword());

            $user->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $data = array('cible' => '#users_list');
            $this->addFlash(
                'info',
                'Ajout effectué avec succée!'
            );

            return new Response(json_encode($data),201,array('Content-Type'=>'application/json'));

            /*return $this->redirectToRoute('user_show', array('id' => $user->getId()));*/
        }

        return $this->render('user/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     *
     * @param Request $request
     * @param User $user
     *
     * @return Response
     */
    public function editAction(Request $request, User $user)
    {
        $editForm = $this->createForm(UserType::class, $user, [
            'action' => $this->generateUrl('user_edit', array('id' => $user->getId()))
        ]);
        $editForm->remove('plainPassword');
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /*dump($user->getRoles()); die;*/
            $em->persist($user);
            $em->flush();

            $data = array('cible' => '#users_list');
            $this->addFlash(
                'info',
                'Modification effectué avec succée!'
            );

            return new Response(json_encode($data),201,array('Content-Type'=>'application/json'));
        }

        return $this->render('user/new.html.twig', array(
            'user' => $user,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}/delete", name="user_delete")
     *
     * @param Request $request
     * @param User $user
     *
     * @return RedirectResponse|Response
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
            $data['cible'] = "#users_list";
            $this->addFlash(
                'info',
                'Suppression effectué avec succée!'
            );
            return new Response(json_encode($data),201,array('Content-Type'=>'application/json'));
        }

        return $this->render('confirm.html.twig', [
            'form' =>$form->createView()
        ]);
    }

    /**
     *
     * Edit rhe user password
     *
     * @Route("/{id}/edit-password", name="user_change_password")
     *
     * @param User $user The User entity
     * @param Request $request
     *
     * @return Response
     */
    public function editPassword(User $user, Request $request)
    {
        $editForm = $this->createForm(UserEditPasswordType::class, $user, [
            'action' => $this->generateUrl('user_change_password', ['id' => $user->getId()])
        ]);
        $editForm->remove('oldPassword');
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPlainPassword()));
            $em->persist($user);
            $em->flush();
            $data = ['cible' => $this->generateUrl('homepage')];
            $this->addFlash(
                'info',
                'Mot de passe modifié avec succée!'
            );

            return new Response(json_encode($data),201,array('Content-Type'=>'application/json'));
        }

        return $this->render('new.html.twig', array(
            'user' => $user,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Creates a form to delete a User entity.
     *
     * @param User $user
     *
     * @return FormInterface The form
     */
    private function createDeleteForm(User $user)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('POST')
            ->getForm()
        ;
    }
}
