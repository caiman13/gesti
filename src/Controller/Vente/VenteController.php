<?php

namespace App\Controller\Vente;

use App\Controller\BaseController;
use App\Entity\Logs;
use App\Library\PdfScanBillet;
use App\Manager\FactureManager;
use App\Manager\LogsManager;
use App\Repository\Admin\CurrencyRepository;
use App\Repository\Admin\TiersRepository;
use App\Repository\Admin\TVARepository;
use App\Repository\Vente\VenteRepository;
use DateTime;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Snappy\Pdf;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Vente\Facture;
use App\Entity\Vente\Parcour;
use App\Form\Vente\FactureType;
use App\MyObject\Scan as FileScan;
use App\Entity\Vente\Vente;
use App\Form\Vente\ScanType;
use App\Form\Vente\VenteType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 * @package App\Controller\Vente
 *
 * @Route("/vente")
 */
class VenteController extends BaseController
{
    /**
     * @var VenteRepository
     */
    private $repository;

    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;

    /**
     * @var FactureManager
     */
    private $factureManager;

    /**
     * @var TiersRepository
     */
    private $tiersRepository;

    /**
     * DefaultController constructor.
     *
     * @param PaginatorInterface $paginator
     * @param Pdf $pdf
     * @param VenteRepository $repository
     * @param CurrencyRepository $currencyRepository
     * @param FactureManager $factureManager
     * @param TiersRepository $tiersRepository
     * @param LogsManager $logsManager
     */
    public function __construct(
        PaginatorInterface $paginator,
        Pdf $pdf,
        VenteRepository $repository,
        CurrencyRepository $currencyRepository,
        FactureManager $factureManager,
        TiersRepository $tiersRepository,
        LogsManager $logsManager
    ) {
        parent::__construct($paginator, $pdf, $logsManager);
        $this->repository = $repository;
        $this->currencyRepository = $currencyRepository;
        $this->factureManager = $factureManager;
        $this->tiersRepository = $tiersRepository;
    }

    /**
     * @return CurrencyRepository
     */
    public function getCurrencyRepository(): CurrencyRepository
    {
        return $this->currencyRepository;
    }

    /**
     * Index of list vente
     *
     * @Route("/", name="vente_index")
     *
     * @return Response
     * @throws Exception
     */
    public function listIndex()
    {
        return $this->render(
            'vente/list_index.html.twig',
            [
                'today' => new DateTime(),
            ]
        );
    }

    /**
     * Liste all vente
     *
     * @Route("/list/{active}/{debut}/{fin}", name="vente_list",
     *     defaults={"debut" = "0", "fin" = "0"}
     *     )
     *
     * @param $active
     * @param $debut
     * @param $fin
     *
     * @return Response
     */
    public function listvente($active, $debut, $fin)
    {
        $ventes = $this->getRepository()->findByClientAndActive($active, $debut, $fin);

        return $this->render(
            'vente/list.html.twig',
            [
                'ventes' => $ventes,
                'active' => $active,
            ]
        );
    }

    /**
     * @return VenteRepository
     */
    public function getRepository(): VenteRepository
    {
        return $this->repository;
    }

    /**
     * Create new Vente
     *
     * @Route("/new", name="vente_nouveau", methods={"GET", "POST"})
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function newVente(Request $request)
    {
        /*die('ok');*/
        $vente = new Vente();
        $em = $this->getDoctrine()->getManager();
        $currency = $this->currencyRepository->findOneBy(['active' => true]);
        $vente->setCurrency($currency);
        $form = $this->createForm(
            VenteType::class,
            $vente,
            [
                'action' => $this->generateUrl('vente_nouveau'),
            ]
        );
        $scan = new FileScan();
        $scan_form = $this->createForm(
            ScanType::class,
            $scan,
            [
                'action' => $this->generateUrl('vente_scan_pdf'),
            ]
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {


            $vente->setEtat('non-facture');

            $em->persist($vente);
            $em->flush();

            $logs = new Logs();
            $logs->setDescription(
                sprintf(
                    "Creation du billet n° : %s",
                    $vente->getNumBillet()
                )
            );
            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);

            return $this->redirect($this->generateUrl("vente_nouveau"));
            // ...
        }

        return $this->render(
            'vente/form_vente.html.twig',
            [
                'form_titre' => '<i class="ace-icon fa fa-money"></i>&nbsp;Nouvelle vente',
                'form' => $form->createView(),
                'devise' => $currency->getCodeIso(),
                'scan_form' => $scan_form->createView(),
            ]
        );
    }

    /**
     * Edit vente
     *
     * @Route("/{id}/edit", name="vente_edit", methods={"GET","POST"})
     *
     * @param Request $request
     * @param Vente $vente
     *
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function editVente(Request $request, Vente $vente)
    {
        $form = $this->createForm(
            VenteType::class,
            $vente,
            [
                'action' => $this->generateUrl('vente_edit', ['id' => $vente->getId()]),
            ]
        );

        $form->add(
            'feesAGV',
            TextType::class,
            [
                'attr' => [
                    'class' => 'field',
                    'readonly' => 'true',
                ],
                'label' => 'Fees Agence de voyage',
            ]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            /*$vente->setDevise($vente->getDevise()->__tostring());*/

            $vente->setEtat('non-facture');

            $em->persist($vente);
            $em->flush();
            $this->addFlash(
                'danger',
                'Votre modification a été effectué avec succés. Merci'
            );


            $logs = new Logs();
            $logs->setDescription(
                sprintf(
                    "Modification du billet n° : %s",
                    $vente->getNumBillet()
                )
            );
            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);

            return $this->redirect($this->generateUrl("vente_nouveau"));
            // ...
        }

        return $this->render(
            'vente/edit.html.twig',
            [
                'form_titre' => '<i class="ace-icon fa fa-money"></i>&nbsp;Modifier la vente',
                'form' => $form->createView(),
                /*'devise' => $currency->getCodeIso(),
                'scan_form' => $scan_form->createView(),*/
            ]
        );
    }


    /**
     * Scan billet pdf
     *
     * @Route("/vente/action/scan/pdf-scan", name="vente_scan_pdf", methods="POST")
     *
     * @param Request $request
     * @param PdfScanBillet $pdfScan
     *
     * @param CurrencyRepository $currencyRepository
     *
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function scanPdf(
        Request $request,
        PdfScanBillet $pdfScan,
        CurrencyRepository $currencyRepository
    ) {
        $em = $this->getDoctrine()->getManager();
        $scan = new FileScan();
        $scan_form = $this->createForm(
            ScanType::class,
            $scan,
            [
                'action' => $this->generateUrl('vente_scan_pdf'),
            ]
        );
        $scan_form->handleRequest($request);
        /** @var UploadedFile $file */
        $file = $scan->getFile();
        $fileName = $file->getClientOriginalName().$file->guessExtension();

        $file->move(
            $this->getParameter('upload_directory'),
            $fileName
        );

        $vente = new Vente();

        $scan->setFile($fileName);

        $data = $pdfScan->scan($this->getParameter('upload_directory').'/'.$fileName);

        if ($data == null) {
            $form = $this->createForm(
                VenteType::class,
                $vente,
                [
                    'action' => $this->generateUrl('vente_nouveau'),
                ]
            );

            $scan = new FileScan();
            $scan_form = $this->createForm(
                ScanType::class,
                $scan,
                [
                    'action' => $this->generateUrl('vente_scan_pdf'),
                ]
            );

            return $this->render(
                "vente/form_vente.html.twig",
                [
                    'form' => $form->createView(),
                    'form_titre' => '<i class="ace-icon fa fa-money"></i>&nbsp;Nouvelle vente',
                    'vente' => $vente,
                    'scan_form' => $scan_form->createView(),
                ]
            );
        }
        /*        dump($result);*/
        unlink($this->getParameter('upload_directory').'/'.$fileName);

        $vente->setBookRef($data['book_ref']);
        $vente->setNumBillet($data['ticket_number']);
        $vente->setMontantTransport($data['montant']['montant']);
        $vente->setCurrency($currencyRepository->findOneBy(['code_iso' => $data['montant']['devise']]));
        $tax_total = 0;
        foreach ($data['tax'] as $value) {
            $tax_total = $tax_total + (int)$value['montant'];
        }

        $tax_total = $tax_total + (int)$data['airline_surcharge']['montant'];

        $vente->setTaxe(strval($tax_total));
        /*dump($data['itinerary']); die;*/
        foreach ($data['itinerary'] as $value) {
            $explode = explode(' ', $value['date']);
            if (count($explode) == 4) {
                $parcour = new Parcour();
                $parcour->setFromName($value['from']);
                $parcour->setToName($value['to']);
                $parcour->setClasseBlt($value['class']);

                $parcour->setDateParcour(new DateTime($value['date']));
                /*$parcour->setVoyageurName($data['traveler']);*/
                $vente->addParcour($parcour);
            }
        }
        $vente->setPassager($data['traveler']);
        $vente->setCompagieAerienne(
            $this->tiersRepository->findOneBy(['raisonSociale' => $data["cie_ae"]])
        );

        /*$em->persist($vente);
        $em->flush();*/
        /*dump($vente); die;*/

        if ($this->repository->findOneBy(['numBillet' => $vente->getNumBillet()])) {
            $this->addFlash(
                'danger',
                'Ce billet a déja été scanné! Veuiller en choisir un autre. Merci'
            );

            return $this->redirect($this->generateUrl('vente_nouveau'));
        }

        $form = $this->createForm(
            VenteType::class,
            $vente,
            [
                'action' => $this->generateUrl('vente_nouveau'),
            ]
        );

        $scan = new FileScan();
        $scan_form = $this->createForm(
            ScanType::class,
            $scan,
            [
                'action' => $this->generateUrl('vente_scan_pdf'),
            ]
        );

        return $this->render(
            "vente/form_vente.html.twig",
            [
                'form' => $form->createView(),
                'form_titre' => '<i class="ace-icon fa fa-money"></i>&nbsp;Nouvelle vente',
                'vente' => $vente,
                'scan_form' => $scan_form->createView(),
            ]
        );
    }

    /**
     * show vente détail
     *
     * @Route("/{id}/show-vente", name="vente_show")
     *
     * @param Vente $vente
     *
     * @param TVARepository $TVARepository
     *
     * @return Response
     */
    public function show(Vente $vente, TVARepository $TVARepository)
    {
        $em = $this->getDoctrine()->getManager();
        $tva = $TVARepository->find(1);
        $currency = $this->currencyRepository->findOneBy(['active' => true]);

        /*if($vente->isValide()){
            $cible =
        }*/

        return $this->render(
            'vente/show.html.twig',
            [
                'vente' => $vente,
                'tva' => $tva,
                'devise' => $currency,
                'cible' => '#'.$vente->getEtat(),
            ]
        );
    }

    /**
     * Delete vente
     *
     * @Route("/{id}/delete", name="vente_delete", methods={"POST"})
     *
     * @param Request $request
     * @param Vente $vente
     *
     * @return Response
     */
    public function delete(Request $request, Vente $vente)
    {
        $form = $this->createDeleteForm($vente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($vente);
            $em->flush();


            $logs = new Logs();
            $logs->setDescription(
                sprintf(
                    "Suppression du billet n° : %s",
                    $vente->getNumBillet()
                )
            );
            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);
            $this->addFlash(
                'info',
                'Votre vente a été supprimé avec succée'
            );
        } else {
            $this->addFlash(
                'danger',
                'Une erreur s\'est produit lors de la suppression'
            );
        }

        $data = [];
        $data['cible'] = "#non-facture";

        return new Response(json_encode($data), 201, ['Content-Type' => 'application/json']);
    }

    /**
     * @param Vente $vente
     *
     * @return FormInterface The form
     */
    private function createDeleteForm(Vente $vente)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('vente_delete', ['id' => $vente->getId()]))
            ->setMethod('POST')
            ->getForm();
    }

    /**
     * Facturer Vente
     *
     * @Route("/facturer", name="vente_facturer")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function facturer(Request $request)
    {
        $facture = new Facture();
        $clients = $this->tiersRepository->findBy(
            ['discriminator' => 'client'],
            ['nom' => 'ASC', 'raisonSociale' => 'ASC']
        );
        $cli_choice = [];
        foreach ($clients as $client) {
            $cli_choice[$client->__toString()] = $client->getId();
        }
        $cli_choice["Nouveau client"] = "999999999";
        $idventes = $request->request->get('idVentes');

        $form = $this->createForm(
            FactureType::class,
            $facture,
            [
                'action' => $this->generateUrl('vente_do_facturer'),
                'cli_choice' => $cli_choice,
            ]
        );
        $currency = $this->currencyRepository->findOneBy(['active' => true]);

        $form->remove("tauxChange");

        return $this->render(
            'vente/facturer.html.twig',
            [
                'idVentes' => implode(",", $idventes),
                'currency' => $currency,
                'form' => $form->createView(),
                'data_url' => $this->generateUrl('tiers_ajax_add'),
            ]
        );
    }

    /**
     * Faire facturation de la vente
     *
     * @Route("/do-facturer", name="vente_do_facturer")
     *
     * @param Request $request
     * @param TVARepository $TVARepository
     * @param FactureManager $factureManager
     * @param TiersRepository $tiersRepository
     *
     * @return Response
     * @throws Exception
     */
    public function doFacturer(
        Request $request,
        TVARepository $TVARepository,
        FactureManager $factureManager
    ) {
        //$em = $this->getDoctrine()->getManager();
        $facture = new Facture();
        $idventes = explode(",", $request->request->get('idVentes'));
        foreach ($idventes as $idvente) {
            /* dump($idvente);*/
            $vente = $this->repository->find($idvente);
            $facture->addVente($vente);
        }
        $clients = $this->tiersRepository->findBy(['discriminator' => 'client']);
        $cli_choice = [];
        foreach ($clients as $client) {
            $cli_choice[$client->__toString()] = $client->getId();
        }
        $cli_choice["Nouveau client"] = "";
        $form = $this->createForm(
            FactureType::class,
            $facture,
            [
                'action' => $this->generateUrl('vente_do_facturer'),
                'cli_choice' => $cli_choice,
            ]
        );

        //dump($facture); die;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $factureManager->saveFacture($facture, $TVARepository, $this->tiersRepository);

            $billets = [];

            foreach ($facture->getVentes() as $vente) {
                $billets[] = $vente->getNumBillet();
            }

            $sBillet = implode(', ', $billets);

            $logs = new Logs();
            $logs->setDescription(
                sprintf(
                    "Facturation des billet n° : %s et création de la facture ref: %s",
                    $sBillet,
                    $facture->getReference()
                )
            );
            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);

            $data['cible'] = "#list_fac";
            $this->addFlash(
                'info',
                'facturation effectué avec succée!'
            );

            return new Response(json_encode($data), 201, ['Content-Type' => 'application/json']);
        }
    }

    /**
     * Confirm delete vente
     *
     * @Route("/{id}/confirm-delete", name="vente_confirm", methods={"GET"})
     *
     * @param Vente $vente
     *
     * @return Response
     */
    public function confirm(Vente $vente)
    {
        $form = $this->createDeleteForm($vente);

        return $this->render(
            'confirm.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}
