<?php

namespace App\Controller\Vente;

use App\Controller\BaseController;
use App\Entity\Logs;
use App\Repository\Admin\CurrencyRepository;
use App\Repository\Admin\TiersRepository;
use App\Repository\Vente\BonCommandeRepository;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Vente\BonCommande;
use App\Entity\Vente\Vente;
use App\Form\Vente\BCType;
use App\Form\Vente\GenFactType;
use App\Form\Vente\VenteBcType;
use App\MyObject\GenererVente;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BonCommandeController
 * @package App\Controller\Vente
 *
 * @Route("/bon-commande")
 */
class BonCommandeController extends BaseController
{
    /**
     * Index of BC
     *
     * @Route("/", name="bc_index")
     *
     * @param TiersRepository $tiersRepository
     *
     * @return Response
     */
    public function index(TiersRepository $tiersRepository)
    {
        $fournisseurs = $tiersRepository->findBy(
            ['discriminator' => 'fournisseurs', 'linked' => false],
            ['raisonSociale' => 'ASC', 'nom' => 'ASC']
        );
        return $this->render('bc/index.html.twig', [
            'fournisseurs' => $fournisseurs
        ]);
    }


    /**
     * Show BC
     *
     * @Route("/{id}", name="bc_show")
     *
     * @param BonCommande $bc
     * @return Response
     */
    public function showAction(BonCommande $bc): Response
    {
        return $this->render('bc/show.html.twig', array('bc' => $bc));
    }

    /**
     * Init BC create
     *
     * @Route("/init/create-form", name="bc_init_create")
     *
     * @param Request $request
     *
     * @param BonCommandeRepository $bonCommandeRepository
     *
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function initCreateAction(Request $request, BonCommandeRepository $bonCommandeRepository)
    {
        $em = $this->getDoctrine()->getManager();
        $bc = new BonCommande();
        $form = $this->createForm(BCType::class, $bc, [
            'action' => $this->generateUrl('bc_init_create')
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $ref = $bonCommandeRepository->generateRef();
            $bc->setReference($ref);
            $em->persist($bc);
            $em->flush();

            $logs = new Logs();
            $logs->setDescription(
                sprintf(
                    "Initialisation du BC ref: %s",
                    $bc->getReference()
                )
            );
            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);

            return $this->redirect($this->generateUrl('bc_do_create', ['id' => $bc->getId()]));
        }

        return $this->render(
            'new.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Init BC create
     *
     * @Route("/init/modif-form/{id}", name="bc_init_modif")
     *
     * @param Request $request
     *
     * @param BonCommandeRepository $bonCommandeRepository
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function initModif(Request $request, BonCommandeRepository $bonCommandeRepository, BonCommande $bc)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(
            BCType::class,
            $bc,
            [
                'action' => $this->generateUrl('bc_init_modif', ['id' => $bc->getId()]),
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $bc->setEtat('non-valide');
            $em->persist($bc);
            $em->flush();

            $logs = new Logs();
            $logs->setDescription(
                sprintf(
                    "Modification du BC ref: %s",
                    $bc->getReference()
                )
            );
            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);

            $vente = $bc->getVentes()->first();

            return $this->redirect(
                $this->generateUrl(
                    'bc_do_modif',
                    [
                        'id' => $bc->getId(),
                        'id_vente' => $vente->getId(),
                    ]));
        }

        return $this->render('new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Modif detail BC
     *
     * @Route("/modif/{id}/{id_vente}/operation",
     *     name="bc_do_modif",
     *     methods={"GET", "POST"}
     *     )
     * @ParamConverter("bonCommande", options={"mapping"={"id"="id"}})
     *
     *
     * @param Request $request
     * @param BonCommande $bonCommande
     * @param $id_vente
     * @param CurrencyRepository $currencyRepository
     *
     * @return Response
     * @throws Exception
     */
    public function modifDetailBC(
        Request $request,
        BonCommande $bonCommande,
        $id_vente,
        CurrencyRepository $currencyRepository
    ): Response {
        $venteCurent = null;
        $ventes = $bonCommande->getVentes();

        foreach ($ventes as $vente) {
            if ($vente->getId() === (int)$id_vente) {
                $venteCurent = $vente;
                break;
            }
        }

        $minusTransport = (int)$bonCommande->getMontantTransport() - (int)$venteCurent->getMontantTransport();
        $minusTaxe = (int)$bonCommande->getMontantTaxe() - (int)$venteCurent->getTaxe();
        $minusFeedsAvg = (int)$bonCommande->getFeeAGV() - (int)$venteCurent->getFeesAGV();

        $form = $this->createForm(
            VenteBcType::class,
            $venteCurent,
            [
                'action' => $this->generateUrl(
                    'bc_do_modif',
                    [
                        'id' => $bonCommande->getId(),
                        'id_vente' => $venteCurent->getId(),
                    ]
                ),
            ]
        );

        $form->handleRequest($request);

        $venteNext = $ventes->next();
        //$isNullNext = is_null($venteNext);
        $modifNext = ($venteNext && !($venteCurent->getId() == $venteNext->getId())) ?? false;

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $venteCurent->setBc($bonCommande);
            $venteCurent->setTypeBillet('normal');
            $montat_transp = $minusTransport + (int)$venteCurent->getMontantTransport();
            $montat_taxe = $minusTaxe + (int)$venteCurent->getTaxe();
            $fees_AGV = $minusFeedsAvg + (int)$venteCurent->getFeesAGV();
            $bonCommande->setMontantTransport(strval($montat_transp));
            $bonCommande->setEtat('non-valide');
            $bonCommande->setMontantTaxe(strval($montat_taxe));
            $bonCommande->setFeeAGV(strval($fees_AGV));
            $em->persist($venteCurent);
            $em->persist($bonCommande);
            $em->flush();
            if ($modifNext) {
                $form = $this->createForm(
                    VenteBcType::class,
                    $venteNext,
                    [
                        'action' => $this->generateUrl(
                            'bc_do_modif',
                            [
                                'id' => $bonCommande->getId(),
                                'id_vente' => $venteNext->getId(),
                            ]
                        ),
                    ]
                );

                $logs = new Logs();
                $logs->setDescription(
                    sprintf(
                        "Modofication de billet sur BC ref: %s",
                        $bonCommande->getReference()
                    )
                );
                $logs->setUser($this->getUser());
                $this->logsManager->save($logs);


                return $this->render(
                    'bc/new.html.twig',
                    [
                        'form' => $form->createView(),
                        'form_titre' => '<i class="ace-icon fa fa-money"></i>&nbsp;Modifier BC',
                        'bc' => $bonCommande,
                        'retour' => 'retour',
                        'next' => $modifNext,
                    ]
                );
            }
        }

        return $this->render(
            'bc/new.html.twig',
            array(
                'form' => $form->createView(),
                'form_titre' => '<i class="ace-icon fa fa-money"></i>&nbsp;Modifier BC',
                'bc' => $bonCommande,
                'next' => $modifNext,
            )
        );
    }

    /**
     * List of BC by etat and client
     *
     * @Route("/{etat}/list/{id_cli}",
     *     name="bc_list",
     *     defaults={"id_cli" = 0})
     *
     * @param BonCommandeRepository $bonCommandeRepository
     * @param TiersRepository $tiersRepository
     * @param $etat
     * @param $id_cli
     * @return Response
     */
    public function listAction(
        BonCommandeRepository $bonCommandeRepository,
        TiersRepository $tiersRepository,
        $etat,
        $id_cli
    ): Response
    {
        if ($id_cli == 0) {
            $bcs = $bonCommandeRepository->findBy(['etat' => $etat]);
        } else {
            $tier = $tiersRepository->find($id_cli);
            $bcs = $bonCommandeRepository->findBy(['tier' => $tier, 'etat' => $etat]);
        }

        return $this->render('bc/list.html.twig', [
            'etat' => $etat,
            'bcs' => $bcs
        ]);
    }

    /**
     * Generate pdf of BC
     *
     * @Route("/pdf-bc/{id}", name="bc_pdf")
     *
     * @param TiersRepository $tiersRepository
     * @param BonCommande $bc
     * @return Response
     */
    public function pdfAction(TiersRepository $tiersRepository, BonCommande $bc): Response
    {
        $ets = $tiersRepository->find(1);
        $filename = sprintf('bc-%s.pdf', $bc->getReference());
        $html = $this->renderView('bc/bc.html.twig', [
            'bc' => $bc,
            'ets' => $ets,
        ]);

        return new Response(
            $this->pdf->getOutputFromHtml($html),
            200,
            [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            ]
        );
    }

    /**
     * Action to valide a BC
     *
     * @Route("/{id}/valider", name="bc_valider", methods={"POST"})
     *
     * @param Request $request
     * @param BonCommande $bc
     *
     * @return Response
     * @throws Exception
     */
    public function validerAction(Request $request, BonCommande $bc)
    {
        $form = $this->createValiderForm($bc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $bc->setEtat("valide");
            $em->persist($bc);
            $em->flush();
            $logs = new Logs();
            $logs->setDescription(
                sprintf(
                    "Validation du BC ref: %s",
                    $bc->getReference()
                )
            );
            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);

            $this->addFlash(
                'info',
                'Votre validation a été effectué avec succée'
            );
        } else {
            $this->addFlash(
                'danger',
                'Une erreur s\'est produit lors de la validation'
            );
        }

        $data = array();
        $data['cible'] = "#valide";

        return new Response(json_encode($data), 201, array('Content-Type' => 'application/json'));
    }

    /**
     * Do create BC
     *
     * @Route("/create/{id}/operation",
     *     name="bc_do_create",
     *     methods={"GET", "POST"}
     *     )
     *
     * @param Request $request
     * @param BonCommande $bc
     * @param CurrencyRepository $currencyRepository
     *
     * @return Response
     * @throws Exception
     */
    public function doCreate(
        Request $request,
        BonCommande $bc,
        CurrencyRepository $currencyRepository
    ): Response {
        $modifNext = true;
        $em = $this->getDoctrine()->getManager();
        $vente = new Vente();
        $currency = $currencyRepository->findOneBy(['active' => true]);
        $vente->setCurrency($currency);
        $form = $this->createForm(VenteBcType::class, $vente, [
            'action' => $this->generateUrl('bc_do_create', ['id' => $bc->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $vente->setBc($bc);
            $vente->setTypeBillet('normal');
            $montat_transp = (int)$bc->getMontantTransport() + (int)$vente->getMontantTransport();
            $montat_taxe = (int)$bc->getMontantTaxe() + (int)$vente->getTaxe();
            $fees_AGV = (int)$bc->getFeeAGV() + (int)$vente->getFeesAGV();
            $vente->setPrevente(true);
            $bc->setMontantTransport(strval($montat_transp));
            $bc->setEtat('non-valide');
            $bc->setMontantTaxe(strval($montat_taxe));
            $bc->setFeeAGV(strval($fees_AGV));
            $em->persist($vente);
            $em->persist($bc);
            $em->flush();
            $vente = new Vente();
            $form = $this->createForm(VenteBcType::class, $vente, [
                'action' => $this->generateUrl('bc_do_create', ['id' => $bc->getId()])
            ]
            );

            $logs = new Logs();
            $logs->setDescription(
                sprintf(
                    "Ajout de billet sur BC ref: %s",
                    $bc->getReference()
                )
            );
            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);


            return $this->render('bc/new.html.twig', array(
                'form' => $form->createView(),
                'form_titre' => '<i class="ace-icon fa fa-money"></i>&nbsp;Nouvelle BC',
                'bc' => $bc,
                'retour' => 'retour',
                'next' => $modifNext,
            ));
        }

        return $this->render('bc/new.html.twig', array(
            'form' => $form->createView(),
            'form_titre' => '<i class="ace-icon fa fa-money"></i>&nbsp;Ajout de detail BC',
            'bc' => $bc,
            'next' => $modifNext,
        ));
    }

    /**
     * generate real vente from BC
     *
     * @Route("/{id}/generer-ventes", name="bc_generer_ventes", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param BonCommande $bc
     *
     * @return Response
     * @throws Exception
     */
    public function genererVente(Request $request, BonCommande $bc)
    {
        $genvente = new GenererVente();
        $preVentes = array();
        foreach ($bc->getVentes()->toArray() as $value) {
            $preVentes[] = $value;
        }
        $genvente->setVentes($preVentes);
        $form = $this->createForm(GenFactType::class, $genvente, [
            'action' => $this->generateUrl('bc_generer_ventes', ['id' => $bc->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            /** @var Vente $ventePre */
            foreach ($genvente->getVentes() as $ventePre) {
                $ventePre->setEtat('non-facture');
                $ventePre->setType('normal');
                $em->persist($ventePre);
            }
            $bc->setEtat("vendu");
            $em->persist($bc);
            $em->flush();

            $logs = new Logs();
            $logs->setDescription(
                sprintf(
                    "Generation de vente à partir du BC ref: %s",
                    $bc->getReference()
                )
            );
            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);


            $this->addFlash(
                'info',
                'Votre géneratioin a été effectué avec succée'
            );

            $data = array();
            $data['cible'] = "#list_vente";

            return new Response(json_encode($data), 201, array('Content-Type' => 'application/json'));
        }

        return $this->render('bc/generer.html.twig', [
            'form' => $form->createView(),
            'nbr' => count($preVentes)
        ]);
    }

    /**
     * Creates a form to delete a Tiers entity.
     *
     * @param BonCommande $bc
     *
     * @return FormInterface The form
     */
    private function createDeleteForm(BonCommande $bc)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bc_delete', array('id' => $bc->getId())))
            ->setMethod('POST')
            ->getForm();
    }

    /**
     * Delete BC
     *
     * @Route("/{id}/delete", name="bc_delete", methods={"POST"})
     *
     * @param Request $request
     * @param BonCommande $bc
     * @return Response
     */
    public function delete(Request $request, BonCommande $bc)
    {
        $form = $this->createDeleteForm($bc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($bc);
            $em->flush();

            $logs = new Logs();
            $logs->setDescription(
                sprintf(
                    "Suppression du BC ref: %s",
                    $bc->getReference()
                )
            );
            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);


            $this->addFlash(
                'info',
                'Votre bon de commande a été supprimé avec succée'
            );
        } else {
            $this->addFlash(
                'danger',
                'Une erreur s\'est produit lors de la suppression'
            );
        }

        $data = array();
        $data['cible'] = "#bc";

        return new Response(json_encode($data), 201, array('Content-Type' => 'application/json'));
    }

    /**
     * Confirm delete Action
     *
     * @Route("/{id}/delete", name="bc_confirm", methods={"GET"})
     *
     * @param BonCommande $bc
     * @return Response
     */
    public function confirm(BonCommande $bc)
    {
        $form = $this->createDeleteForm($bc);
        return $this->render('confirm.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Creates a form to delete a Tiers entity.
     *
     * @param BonCommande $bc
     *
     * @return FormInterface The form
     */
    private function createValiderForm(BonCommande $bc)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bc_valider', array('id' => $bc->getId())))
            ->setMethod('POST')
            ->getForm();
    }

    /**
     * Confirm validation
     *
     * @Route("/{id}/valider", name="bc_confirm_valider", methods={"GET"})
     *
     * @param BonCommande $bc
     * @return Response
     */
    public function confirmValid(BonCommande $bc)
    {
        $form = $this->createValiderForm($bc);
        return $this->render('confirm.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function modif(BonCommande $bc, Request $request)
    {

        return $this->render('bc/new.html.twig');
    }
}
