<?php

namespace App\Controller;

use App\Controller\BaseController;
use App\Repository\Admin\LigneComptaRepository;
use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ComptaController
 * @package App\Controller
 *
 * @Route("/compta")
 */
class ComptaController extends BaseController
{
    /**
     * @Route("/", name="compta_index")
     */
    public function index()
    {
        return $this->render('admin/compta/index.html.twig', [
            'icon' => 'fa fa-book',
            'title' => 'Comptabilite',
            'today' => new DateTime(),
        ]);
    }

    /**
     * @Route("/{debut}/{fin}", name="compta_journal", defaults={"debut" = 0, "fin" = 0})
     *
     * @param LigneComptaRepository $repository
     * @param $debut
     * @param $fin
     *
     * @return Response
     * @throws Exception
     */
    public function journalAction(LigneComptaRepository $repository,$debut, $fin)
    {
        $journals = $repository->getJournal($debut, $fin);
        return $this->render('admin/compta/journal.html.twig', [
            'journals' => $journals,
            'dateDebutJ' => new DateTime($debut),
            'dateFinJ' => new DateTime($fin)
        ]);
    }
}
