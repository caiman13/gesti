<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 05/09/2017
 * Time: 00:56
 */

namespace App\Controller\Recouvrement;

use App\Entity\Admin\LigneCompta;
use App\Controller\BaseController;
use App\Entity\Logs;
use App\Entity\Vente\PaiementFacture;
use App\Entity\Vente\Vente;
use App\Manager\FactureManager;
use App\Manager\LogsManager;
use App\Manager\PaiementManager;
use App\Repository\Admin\TauxComRepository;
use App\Repository\Admin\TiersRepository;
use App\Repository\Reglement\AvoirRepository;
use App\Repository\Reglement\NoteCreditRepository;
use App\Repository\Vente\FactureRepository;
use App\Services\AvoirEtNoteService;
use App\Services\FactureService;
use App\Services\FactureUtility;
use App\Services\PaiementService;
use DateTime;
use Exception;
use App\Entity\Reglement\Avoir;
use App\Entity\Reglement\InfoVente;
use App\Entity\Reglement\NoteCredit;
use App\Form\Reglement\AvoirType;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Snappy\Pdf;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use App\Entity\Vente\Facture;
use App\Entity\Vente\Paiement;
use App\Entity\Vente\Scan;
use App\Form\Vente\PaiementType;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FactureController
 * @package App\Controller\Recouvrement
 *
 * @Route("/facture")
 */
class FactureController extends BaseController
{
    /**
     * @var TiersRepository
     */
    private $tiersRepository;
    /**
     * @var FactureRepository
     */
    private $factureRepository;
    /**
     * @var AvoirRepository
     */
    private $avoirRepository;
    /**
     * @var NoteCreditRepository
     */
    private $creditRepository;

    /**
     * @var FactureManager
     */
    private $factureManager;

    /**
     * FactureController constructor.
     *
     * @param PaginatorInterface $paginator
     * @param Pdf $pdf
     * @param TiersRepository $tiersRepository
     * @param FactureRepository $factureRepository
     * @param AvoirRepository $avoirRepository
     * @param NoteCreditRepository $creditRepository
     * @param FactureManager $factureManager
     * @param LogsManager $logsManager
     */
    public function __construct(
        PaginatorInterface $paginator,
        Pdf $pdf,
        TiersRepository $tiersRepository,
        FactureRepository $factureRepository,
        AvoirRepository $avoirRepository,
        NoteCreditRepository $creditRepository,
        FactureManager $factureManager,
        LogsManager $logsManager
    ) {
        parent::__construct($paginator, $pdf, $logsManager);
        $this->tiersRepository = $tiersRepository;
        $this->factureRepository = $factureRepository;
        $this->avoirRepository = $avoirRepository;
        $this->creditRepository = $creditRepository;
        $this->factureManager = $factureManager;
    }

    /**
     * Index page
     *
     * @Route("/", name="facture_index")
     *
     * @return Response
     * @throws Exception
     */
    public function index()
    {
        $clients = $this->tiersRepository->findBy(
            ['discriminator' => 'client'],
            ['raisonSociale' => 'ASC', 'nom' => 'ASC']
        );

        return $this->render(
            'facture/index.html.twig',
            [
                'clients' => $clients,
                'today' => new DateTime(),
            ]
        );
    }

    /**
     * List of facture
     *
     * @Route("/list/{etat}/{id_cli}/{debut}/{fin}",
     *     name="facture_list",
     *     defaults={
     *     "id_cli" = 0,
     *     "debut" = 0,
     *     "fin" = 0
     *     })
     *
     * @param $etat
     * @param $id_cli
     * @param $debut
     * @param $fin
     *
     * @return Response
     */
    public function list($etat, $id_cli, $debut, $fin)
    {
        $payed = ($etat == 'payee') ? true : false;
        if ($id_cli != 0) {
            $client = $this->tiersRepository->find($id_cli);
            $factures = $this->factureRepository->getByDateCliAndEtat($client, $payed, $debut, $fin);
        } else {
            $factures = $this->factureRepository->getByDateEtat($payed, $debut, $fin);
        }

        return $this->render(
            'facture/list.html.twig',
            [
                'factures' => $factures,
                'etat' => $etat,
            ]
        );
    }

    /**
     * List all annulé facture
     *
     * @Route("/list-facture-annule/{id_cli}/{debut}/{fin}",
     *     name="facture_list_annule",
     *     defaults={
     *     "id_cli" = 0,
     *     "debut" = 0,
     *     "fin" = 0
     *     })
     *
     * @param $id_cli
     * @param $debut
     * @param $fin
     *
     * @return Response
     */
    public function listAnnule($id_cli, $debut, $fin)
    {
        if ($id_cli != 0) {
            $client = $this->tiersRepository->find($id_cli);
            $factures = $this->factureRepository->getAnnuleByCliByDate($client, $debut, $fin);
        } else {
            $factures = $this->factureRepository->getAnnuleByDate($debut, $fin);
        }

        return $this->render(
            'facture/list-annule.html.twig',
            [
                'factures' => $factures,
            ]
        );
    }

    /**
     * Print facture to PDF
     *
     * @Route("/{id}/print-pdf", name="facture_pdf")
     *
     * @param Facture $facture
     *
     * @return Response
     */
    public function pdfprint(Facture $facture)
    {
        $ets = $this->tiersRepository->find(1);
        $filename = sprintf('facture-%s.pdf', $facture->getTier());

        $html = $this->renderView(
            'facture/facture.html.twig',
            [
                'facture' => $facture,
                'ets' => $ets,
            ]
        );

        $footer = $this->renderView(
            'footer.html.twig',
            [
                'ets' => $ets,
            ]
        );

        return new Response(
            $this->pdf->getOutputFromHtml($html, ['footer-html' => $footer]),
            200,
            [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            ]
        );
    }

    /**
     * Rembourser facture
     *
     * @Route("/{id}/rembourser", name="facture_remboursement")
     *
     * @param Request $request
     * @param Facture $facture
     *
     * @return Response
     * @throws Exception
     */
    public function rembourser(Request $request, Facture $facture)
    {
        $em = $this->getDoctrine()->getManager();
        $avoir = new Avoir();
        $avoir->setMontant((float)$facture->getRemboursable());
        $ventes = $facture->getVenteRemboursable();

        $avoir->setClient($facture->getTier());
        foreach ($ventes as $vente) {
            $infoVente = new InfoVente($vente);
            $avoir->addVente($infoVente);
        }
        $form = $this->createForm(
            AvoirType::class,
            $avoir,
            [
                'action' => $this->generateUrl('facture_remboursement', ['id' => $facture->getId()]),
            ]
        );
        /*($avoir);*/
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($avoir);
            $em->flush();
            $today = new DateTime();
            $facture->setMontatRembourse($avoir->getMontant());
            $avoir->setReference($avoir->getId().'-AVR/'.$today->format('y'));
            $avoir->setFacture($facture);
            $em->flush();
            $infoVentes = $avoir->getVentes()->toArray();
            foreach ($infoVentes as $infoVente) {
                /** @var Vente $vente */
                foreach ($ventes as $vente) {
                    if ($vente->getNumBillet() == $infoVente->getTicketNumber()) {
                        $vente->setRemboursement((float)$infoVente->getMontant());
                        $em->persist($vente);
                        $em->flush();
                    }
                }
            }
            $ligne = $avoir->getLignes($facture);
            foreach ($ligne as $item) {
                $em->persist($item);
                $em->flush();
            }
            $logs = new Logs();
            $logs->setDescription(sprintf("Remboursement de la facture Ref: %s", $facture->getReference()));
            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);

            $data = [];
            $data['cible'] = "#non-paye";

            $this->addFlash(
                'info',
                'Remboursement effectué avec succée!'
            );

            return new Response(json_encode($data), 201, ['Content-Type' => 'application/json']);
            /*($this->getUser());*/
            /*($ligne);*/
        }

        return $this->render(
            '/facture/avoir.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Show Facture
     *
     * @Route("/{id}/voir", name="facture_show")
     *
     * @param Facture $facture
     *
     * @return Response
     */
    public function show(Facture $facture)
    {
        if ($facture->isPayed()) {
            $cible = "#paye";
        } else {
            if ($facture->isAnnule()) {
                $cible = "#annule";
            } else {
                $cible = "#non-paye";
            }
        }

        return $this->render('facture/show_facture.html.twig', ['facture' => $facture, 'cible' => $cible]);
    }

    /**
     * Confirmer annulation de facture
     *
     * @Route("/{id}/annuler", name="facture_confirm", methods={"GET"})
     *
     * @param Facture $facture
     *
     * @return Response
     */
    public function confirm(Facture $facture)
    {
        $form = $this->createAnnuleForm($facture);

        return $this->render(
            'confirm.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Effectuer annulation de facture
     *
     * @Route("/{id}/annuler", name="facture_annuler", methods={"POST"})
     *
     * @param Request $request
     * @param Facture $facture
     *
     * @return Response
     * @throws Exception
     */
    public function annuler(Request $request, Facture $facture)
    {
        $form = $this->createAnnuleForm($facture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->factureManager->annuler($facture);
            $logs = new Logs();
            $logs->setDescription(sprintf("Annulation de la facture Ref: %s", $facture->getReference()));
            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);
            $this->addFlash(
                'info',
                'Votre facture a été annulé avec succée'
            );
        }

        $data = [];
        $data['cible'] = "#annule";

        return new Response(json_encode($data), 201, ['Content-Type' => 'application/json']);
    }

    /**
     * @param Facture $facture
     *
     * @return FormInterface The form
     */
    private function createAnnuleForm(Facture $facture)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('facture_annuler', ['id' => $facture->getId()]))
            ->setMethod('POST')
            ->getForm();
    }

    /**
     * Télécharger les pièces jointes
     *
     * @Route("/{id}/get-piece", name="facture_get_piece")
     *
     * @param Facture $facture
     *
     * @return BinaryFileResponse
     */
    public function getPiece(Facture $facture)
    {
        $dir = $this->getParameter('upload_directory');
        /*file_get_content();*/
        $response = new BinaryFileResponse($dir.'/'.$facture->getScan()->getFile());
        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();

        // Set the mimetype with the guesser or manually
        if ($mimeTypeGuesser->isSupported()) {
            // Guess the mimetype of the file according to the extension of the file
            $response->headers->set('Content-Type', $mimeTypeGuesser->guess($dir.'/'.$facture->getScan()->getFile()));
        } else {
            // Set the mimetype of the file manually, in this case for a text file is text/plain
            $response->headers->set('Content-Type', 'text/plain');
        }
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $facture->getScan()->getFile()
        );

        return $response;
    }

    /**
     * Ajouter piece jointe à la facture
     *
     * @Route("/{id}/add-piece", name="facture_add_piece")
     *
     * @param Request $request
     * @param Facture $facture
     *
     * @return Response
     * @throws Exception
     */
    public function addPiece(Request $request, Facture $facture)
    {
        if ($request->isMethod('POST')) {
            $file = $request->files->get('file');
            /*($file); die;*/
            $fileName = $file->getClientOriginalName();
            /*($fileName); die;*/

            $file->move(
                $this->getParameter('upload_directory'),
                $fileName
            );

            $scan = new Scan();

            $scan->setFile($fileName);
            $facture->setScan($scan);
            $em = $this->getDoctrine()->getManager();
            $em->persist($scan);
            $em->persist($facture);
            $em->flush();

            $logs = new Logs();
            $logs->setDescription(sprintf("Ajout de piece jointe de la facture Ref: %s", $facture->getReference()));
            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);

            $data['cible'] = "#show-".$facture->getId();
            $this->addFlash(
                'info',
                'Pièce jointe ajouté avec succée!'
            );

            return new Response(json_encode($data), 201, ['Content-Type' => 'application/json']);
        }

        return $this->render(
            'facture/add_piece.html.twig',
            [
                'action' => $this->generateUrl('facture_add_piece', ['id' => $facture->getId()]),
            ]
        );
    }

    /**
     * Paiement groupé de factutre
     *
     * @Route("/group-paiement", name="facture_groupe_paiement")
     *
     * @param FactureService $factureService
     * @param Request $request
     *
     * @param PaiementManager $paiementManager
     *
     * @return Response
     * @throws Exception
     */
    public function groupPaiement(
        FactureService $factureService,
        Request $request,
        PaiementManager $paiementManager,
        TauxComRepository $tauxComRepository)
    {
        $idFactures = $request->request->get('idFactures');

        if (!is_array($idFactures)){
            $idFactures = explode(',', $idFactures);
        }

        if (!$factureService->checkFactureCli($idFactures)){
            return $this->render(
                'alert.html.twig',
                [
                    'messages' => 'Les factures ne sont pas du même client! Veuillez choisir des facture appartenant aux même client',
                ]
            );
        }
        //'idPaiements' => implode(",", $idPays),

        $factures = $this->factureRepository->getFactureByIds($idFactures, false);

        $paiement = PaiementService::buildPaiement($factures, $factureService);

        $soldes = $factureService->getTotalMontant($factures);

        $client = $factureService->getCliFromGroupedFacture($factures);

        $avoirAuth = $this->avoirRepository->checkCli($client);
        $noteAuth = $this->creditRepository->checkCli($client);

        $form = $this->createForm(
            PaiementType::class,
            $paiement,
            [
                'action' => $this->generateUrl('facture_groupe_paiement'),
                'client' => $client,
                'avoir' => $avoirAuth,
                'note' => $noteAuth,
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $uploadDirectory = $this->getParameter('upload_directory');
            $paiementManager->savePaiement($paiement, $uploadDirectory, $tauxComRepository, $soldes);

            $refs = [];

            foreach ($factures as $facture) {
                $refs[] = $facture->getReference();
            }

            $sRefs = implode(', ', $refs);

            $logs = new Logs();
            $logs->setDescription(sprintf("Paiement des factures Ref: %s", $sRefs));
            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);
            $data['cible'] = "#paye";

            $this->addFlash(
                'info',
                'Facture payé avec succée!'
            );

            return new Response(json_encode($data), 201, ['Content-Type' => 'application/json']);
        }
        return $this->render("facture/addpay.html.twig", [
            'idFactures' => implode(",", $idFactures),
            'form'       => $form->createView(),
            'facturer'   => 'facturer'
        ]);
    }
}
