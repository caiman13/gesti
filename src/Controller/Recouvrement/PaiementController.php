<?php

namespace App\Controller\Recouvrement;

use App\Controller\BaseController;
use App\Entity\Admin\Tiers;
use App\Entity\Logs;
use App\Manager\LogsManager;
use App\Manager\PaiementManager;
use App\Repository\Admin\TauxComRepository;
use App\Repository\Admin\TiersRepository;
use App\Repository\Vente\FactureRepository;
use App\Repository\Vente\PaiementRepository;
use App\Services\AvoirEtNoteService;
use App\Services\FactureService;
use App\Services\PaiementService;
use DateTime;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Snappy\Pdf;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use App\Entity\Vente\Paiement;
use App\Form\Vente\PaiementType;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PaiementController
 * @package App\Controller\Recouvrement
 *
 * @Route("/paiement")
 */
class PaiementController extends BaseController
{
    /**
     * @var PaiementManager
     */
    private $manager;

    /**
     * PaiementController constructor.
     *
     * @param PaginatorInterface $paginator
     * @param Pdf $pdf
     * @param PaiementManager $manager
     * @param LogsManager $logsManager
     */
    public function __construct(
        PaginatorInterface $paginator,
        Pdf $pdf,
        PaiementManager $manager,
        LogsManager $logsManager
    ) {
        parent::__construct($paginator, $pdf, $logsManager);
        $this->manager = $manager;
    }

    /**
     * index of paiement
     *
     * @Route("/", name="paiement_index")
     *
     * @param TiersRepository $tiersRepository
     *
     * @return Response
     * @throws Exception
     */
    public function indexAction(TiersRepository $tiersRepository)
    {
        $clients = $tiersRepository->findBy(['discriminator' => 'client'], ['raisonSociale' => 'ASC', 'nom' => 'ASC']);

        return $this->render(
            'paiement/index.html.twig',
            [
                'clients' => $clients,
                'today' => new DateTime(),
            ]
        );
    }

    /**
     * Listes des paiement
     *
     * @Route("/list-paiement/{etat}/{id_cli}/{debut}/{fin}",
     *     name="paiement_list",
     *     defaults={
     *     "id_cli" = 0,
     *     "debut" = 0,
     *     "fin" = 0
     *     })
     *
     * @param $etat
     * @param $id_cli
     * @param $debut
     * @param $fin
     *
     * @param TiersRepository $tiersRepository
     * @param PaiementRepository $paiementRepository
     *
     * @return Response
     */
    public function list(
        $etat,
        $id_cli,
        $debut,
        $fin,
        TiersRepository $tiersRepository,
        PaiementRepository $paiementRepository
    ) {
        $valide = ($etat == 'valide') ? true : false;
        if ($id_cli != 0) {
            $client = $tiersRepository->find($id_cli);
            $paiements = $paiementRepository->getByDateCliAndEtat(
                $client,
                $valide,
                $debut,
                $fin
            );
        } else {
            $paiements = $paiementRepository->getByDateEtat($valide, $debut, $fin);
        }

        return $this->render(
            'paiement/list.html.twig',
            [
                'paiements' => $paiements,
                'type' => $etat,
            ]
        );
    }

    /**
     * @param Paiement $paiement
     *
     * @return FormInterface
     */
    private function getSupprimerForm(Paiement $paiement)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('paiement_supprimer', ['id' => $paiement->getId()]))
            ->setMethod('POST')
            ->getForm();
    }

    /**
     * Modifier une paiement
     *
     * @Route("/{id}/modifier", name="paiement_modifier")
     *
     * @param Request $request
     * @param Paiement $paiement
     *
     * @param FactureService $factureService
     *
     * @param TauxComRepository $tauxComRepository
     *
     * @return Response
     * @throws Exception
     */
    public function modifier(
        Request $request,
        Paiement $paiement,
        FactureService $factureService,
        TauxComRepository $tauxComRepository
    ) {
        $paiement = PaiementService::setFacturesRef($paiement);

        $paiementClone = clone $paiement;

        $form = $this->createForm(
            PaiementType::class,
            $paiement,
            [
                'action' => $this->generateUrl('paiement_modifier', ['id' => $paiement->getId()]),
            ]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $this->manager->annulePaiement($paiementClone);

            $factures = PaiementService::getFactureByPaiements($paiementClone);

            $soldes = $factureService->getTotalMontant($factures);

            $uploadDirectory = $this->getParameter('upload_directory');

            $this->manager->savePaiement($paiement, $uploadDirectory, $tauxComRepository, $soldes);

            $logs = new Logs();
            $logs->setDescription(
                sprintf(
                    "Modification de la paiement du client : %s du %s",
                    $paiement->getClient(),
                    $paiement->getDatePaiement()->format('d/m/Y')
                )
            );
            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);

            $data = [];
            $data['cible'] = ($paiement->getValide()) ? "#valide" : "#non_valide";

            return new Response(json_encode($data), 201, ['Content-Type' => 'application/json']);
        }

        return $this->render(
            'facture/addpay.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Supprimer une paiement
     *
     * @Route("/{id}/supprimer", name="paiement_supprimer")
     *
     * @param Request $request
     * @param Paiement $paiement
     *
     * @return Response
     * @throws Exception
     */
    public function supprimer(Request $request, Paiement $paiement)
    {
        $form = $this->getSupprimerForm($paiement);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = [];

            $logs = new Logs();
            $logs->setDescription(
                sprintf(
                    "Suppression de la paiement du client : %s du %s",
                    $paiement->getClient(),
                    $paiement->getDatePaiement()->format('d/m/Y')
                )
            );

            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);

            $this->manager->deletePaiement($paiement);
            $data['cible'] = ($paiement->getValide()) ? "#valide" : "#non_valide";

            $this->addFlash(
                'info',
                'Votre paiement a été supprimé avec succée'
            );

            return new Response(json_encode($data), 201, ['Content-Type' => 'application/json']);
        }

        return $this->render(
            'confirm.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Télécharger pièce jointe d'une paiement
     *
     * @Route("/{id}/get-piece", name="paiement_get_piece")
     *
     * @param Paiement $paiement
     *
     * @return BinaryFileResponse
     */
    public function getPiece(Paiement $paiement)
    {
        $dir = $this->getParameter('upload_directory');

        $response = new BinaryFileResponse($dir.'/'.$paiement->getScan()->getFile());
        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();

        // Set the mimetype with the guesser or manually
        if ($mimeTypeGuesser->isSupported()) {
            // Guess the mimetype of the file according to the extension of the file
            $response->headers->set('Content-Type', $mimeTypeGuesser->guess($dir.'/'.$paiement->getScan()->getFile()));
        } else {
            // Set the mimetype of the file manually, in this case for a text file is text/plain
            $response->headers->set('Content-Type', 'text/plain');
        }
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $paiement->getScan()->getFile()
        );

        return $response;
    }

    /**
     * A group validation of paiements
     *
     * @Route("/group-validation", name="paiments_group_valid", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param PaiementRepository $paiementRepository
     *
     * @return Response
     * @throws Exception
     */
    public function groupValidation(Request $request, PaiementRepository $paiementRepository)
    {
        $idPays = $request->request->get('idPays');

        if (!is_array($idPays)) {
            $idPays = explode(",", $idPays);
        }

        $paiements = $paiementRepository->getAllPaiementIn($idPays);

        $form = $this->groupValidationForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $this->manager->groupValidation($paiements);

            $logs->setDescription(
                sprintf(
                    "Validation de paiement"
                )
            );

            $logs->setUser($this->getUser());
            $this->logsManager->save($logs);


            $data['cible'] = "#valide";
            $this->addFlash(
                'info',
                'Votre paiement a été validé avec succée'
            );

            return new Response(json_encode($data), 201, ['Content-Type' => 'application/json']);

        }

        return $this->render(
            'paiement/groupConfirm.html.twig',
            [
                'idPaiements' => implode(",", $idPays),
                'paiements' => $paiements,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     *
     * Show paiement
     *
     * @Route("/{id}/show", name="paiement_show")
     *
     * @param Paiement $paiement
     *
     * @return Response
     */
    public function show(Paiement $paiement)
    {
        if ($paiement->getValide()) {
            $cible = "#valide";
        } else {
            $cible = "#non_valide";
        }

        return $this->render(
            'paiement/show.html.twig',
            [
                'paiement' => $paiement,
                'cible' => $cible,
            ]
        );
    }

    /**
     *
     *
     * @return FormInterface
     */
    private function groupValidationForm()
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('paiments_group_valid'))
            ->setMethod('POST')
            ->getForm();
    }

    /**
     * Json API to get the avoir or note Montant
     *
     * @Route("/avoir-note-montant/{id}/{type}", name="paiement_avoir_note_montant")
     *
     * @param string $id
     * @param string $type
     * @param AvoirEtNoteService $service
     *
     * @return JsonResponse
     */
    public function getMontantAvoirNote(string $id, string $type, AvoirEtNoteService $service)
    {
        $montant = $service->getMontantAvoirNote($id, $type);

        return $this->json(["montat" => $montant]);
    }

    /**
     * Get Recu de paiement
     *
     * @Route("/recu-paiement/{id}", name="paiement_get_recu")
     *
     * @param Request $request
     * @param Paiement $paiement
     *
     * @return Response
     */
    public function recuPaiement(Request $request, Paiement $paiement, TiersRepository $tiersRepository)
    {
        $ets = $tiersRepository->find(1);
        $filename = sprintf('recu-paiement-%s.pdf', $paiement->getClient());

        $html = $this->renderView(
            'paiement/recu.html.twig',
            [
                'paiement' => $paiement,
                'ets' => $ets,
            ]
        );

        $footer = $this->renderView(
            'footer.html.twig',
            [
                'ets' => $ets,
            ]
        );

        return new Response(
            $this->pdf->getOutputFromHtml($html, ['footer-html' => $footer]),
            200,
            [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            ]
        );
    }
}
