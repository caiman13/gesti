<?php

namespace App\Repository;

use App\Entity\Logs;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Logs|null find($id, $lockMode = null, $lockVersion = null)
 * @method Logs|null findOneBy(array $criteria, array $orderBy = null)
 * @method Logs[]    findAll()
 * @method Logs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Logs::class);
    }

    /**
     * @param User $user
     * @param $debut
     * @param $fin
     *
     * @return Logs[] Returns an array of Logs objects
     */
    public function getByUserAndDate($debut, $fin, User $user = null)
    {
        $qb = $this->createQueryBuilder('l');

        $qb = $this->__whereDate($qb, $debut, $fin);

        if ($user) {
            $qb
                ->andwhere('l.user = :user')
                ->setParameter('user', $user);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param QueryBuilder $qb
     * @param $debut
     * @param $fin
     * @param bool $isAnd
     *
     * @return QueryBuilder
     */
    private function __whereDate(QueryBuilder $qb, $debut, $fin, $isAnd = false)
    {
        $condition = 'l.date BETWEEN :firstDate AND :lastDate';

        $beginField = 'firstDate';
        $endField = 'lastDate';

        if ($isAnd) {
            $qb->andWhere($condition);
        } else {
            $qb->where($condition);
        }

        return $qb->setParameter($beginField, $debut)
            ->setParameter($endField, $fin);
    }

    // /**
    //  * @return Logs[] Returns an array of Logs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Logs
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
