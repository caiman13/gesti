<?php

namespace App\Repository\Reglement;

use App\Entity\Admin\Tiers;
use App\Entity\Reglement\NoteCredit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method NoteCredit|null find($id, $lockMode = null, $lockVersion = null)
 * @method NoteCredit|null findOneBy(array $criteria, array $orderBy = null)
 * @method NoteCredit[]    findAll()
 * @method NoteCredit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NoteCreditRepository extends ServiceEntityRepository
{
    /**
     * NoteCreditRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, NoteCredit::class);
    }

    /**
     * Get Note by periode and client
     *
     * @param $etat
     * @param $id_cli
     * @param $debut
     * @param $fin
     *
     * @return mixed
     */
    public function getByEtaTCliPeriode($etat, $id_cli, $debut, $fin)
    {
        $qb = $this->createQueryBuilder('nc');
        $qb
            ->where('nc.payed = :etat')
            ->setParameter('etat', $etat)
        ;
        if ($id_cli != 0){
            $qb
                ->join('nc.client', 'c')
                ->andWhere('c.id = :ic_cli')
                ->setParameter('ic_cli', $id_cli)
            ;
        }
        $qb
            ->andWhere('nc.dateNote BETWEEN :firstDate AND :lastDate')
            ->setParameter('firstDate',$debut)
            ->setParameter('lastDate',$fin)
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * Check if client has non-payed Note
     *
     * @param Tiers $client
     * @return bool
     */
    public function checkCli(Tiers $client)
    {
        return ($this->findBy(['client' => $client, 'payed' => false]))?true:false;
    }
}
