<?php

namespace App\Repository\Reglement;

use App\Entity\Reglement\InfoVente;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InfoVente|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfoVente|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfoVente[]    findAll()
 * @method InfoVente[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfoVenteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InfoVente::class);
    }
}
