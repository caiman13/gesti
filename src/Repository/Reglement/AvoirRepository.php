<?php

namespace App\Repository\Reglement;

use App\Entity\Admin\Tiers;
use App\Entity\Reglement\Avoir;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Avoir|null find($id, $lockMode = null, $lockVersion = null)
 * @method Avoir|null findOneBy(array $criteria, array $orderBy = null)
 * @method Avoir[]    findAll()
 * @method Avoir[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AvoirRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Avoir::class);
    }

    /**
     * get Avoir list by etat, client and periode
     *
     * @param $etat
     * @param $id_cli
     * @param $debut
     * @param $fin
     *
     * @return mixed
     */
    public function getByEtaTCliPeriode($etat, $id_cli, $debut, $fin)
    {
        $qb = $this->createQueryBuilder('a');
        $qb
            ->where('a.payed = :etat')
            ->setParameter('etat', $etat)
        ;
        if ($id_cli != 0){
            $qb
                ->join('a.client', 'c')
                ->andWhere('c.id = :ic_cli')
                ->setParameter('ic_cli', $id_cli)
            ;
        }
        $qb
            ->andWhere('a.dateAvoir BETWEEN :firstDate AND :lastDate')
            ->setParameter('firstDate',$debut)
            ->setParameter('lastDate',$fin)
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * Check Avoir client
     *
     *
     * @param Tiers $client
     * @return bool
     */
    public function checkCli(Tiers $client)
    {
        return ($this->findBy(['client' => $client, 'payed' => false]))?true:false;
    }
}
