<?php

namespace App\Repository\Admin;

use App\Entity\Admin\PaiementMode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PaiementMode|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaiementMode|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaiementMode[]    findAll()
 * @method PaiementMode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaiementModeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PaiementMode::class);
    }

    // /**
    //  * @return Paiement_mode[] Returns an array of Paiement_mode objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Paiement_mode
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
