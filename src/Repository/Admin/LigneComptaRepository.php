<?php

namespace App\Repository\Admin;

use App\Entity\Admin\LigneCompta;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LigneCompta|null find($id, $lockMode = null, $lockVersion = null)
 * @method LigneCompta|null findOneBy(array $criteria, array $orderBy = null)
 * @method LigneCompta[]    findAll()
 * @method LigneCompta[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LigneComptaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LigneCompta::class);
    }

    // /**
    //  * @return LigneCompta[] Returns an array of LigneCompta objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LigneCompta
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param $debut
     * @param $fin
     *
     * @return LigneCompta[] array of ligne compta
     */
    public function getJournal($debut, $fin)
    {
        return $this->createQueryBuilder('lc')
            ->where('lc.dateLigne BETWEEN :firstDate AND :lastDate')
            ->setParameters(
                [
                    'firstDate' => $debut,
                    'lastDate' => $fin,
                ]
            )
            ->getQuery()
            ->getResult();
    }
}
