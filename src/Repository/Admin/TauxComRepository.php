<?php

namespace App\Repository\Admin;

use App\Entity\Admin\TauxCom;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TauxCom|null find($id, $lockMode = null, $lockVersion = null)
 * @method TauxCom|null findOneBy(array $criteria, array $orderBy = null)
 * @method TauxCom[]    findAll()
 * @method TauxCom[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TauxComRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TauxCom::class);
    }

    // /**
    //  * @return TauxCom[] Returns an array of TauxCom objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TauxCom
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
