<?php

namespace App\Repository\Vente;

use App\Entity\Vente\Vente;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Vente|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vente|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vente[]    findAll()
 * @method Vente[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VenteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Vente::class);
    }

    /**
     * @param $active
     * @param $debut
     * @param $fin
     * @param int $id_tiers
     *
     * @return Vente[]
     */
    public function findByClientAndActive($active, $debut, $fin, $id_tiers = 0)
    {
        $qb = $this->createQueryBuilder('v');

        /*dump($active); die;*/

        $qb
            ->where('v.etat = :active')
            ->setParameter('active', $active)
            ->andWhere('v.dateVente BETWEEN :firstDate AND :lastDate')
            ->setParameter('firstDate', $debut)
            ->setParameter('lastDate', $fin);

        if ($id_tiers != 0) {
            $qb
                ->join('v.compagieAerienne', 'c')
                ->andWhere('c.id = :idTiers')
                ->setParameter('idTiers', $id_tiers);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $debut
     * @param $fin
     *
     * @return array
     */
    public function venteParCieParPeriode($debut, $fin)
    {
        $qb = $this->createQueryBuilder('v');
        $qb
            ->select('c.raisonSociale')
            ->join('v.compagieAerienne', 'c')
            ->where('v.dateVente BETWEEN :firstDate AND :lastDate')
            ->setParameter('firstDate', $debut)
            ->setParameter('lastDate', $fin)
            ->addSelect('SUM(v.montantTransport+v.taxe) as montant_total')
            ->addSelect('COUNT(v.id) as nombre_vente')
            ->groupBy('c.raisonSociale')
            ->orderBy('montant_total');


        return $qb->getQuery()->getResult();
    }

    /**
     * @param $etat
     * @param $id_cie
     * @param $debut
     * @param $fin
     *
     * @return Vente[]
     */
    public function tousVenteParCieParPeriode($etat, $id_cie, $debut, $fin)
    {
        $qb = $this->createQueryBuilder('v');
        $qb
            ->join('v.compagieAerienne', 'c')
            ->where('v.reglee = :etat')
            ->setParameter('etat', $etat)
            ->andWhere('v.dateVente BETWEEN :firstDate AND :lastDate')
            ->setParameter('firstDate', $debut)
            ->setParameter('lastDate', $fin)
            ->andWhere('c.id = :id_cie')
            ->setParameter('id_cie', $id_cie);

        /*ump($qb->getQuery()); die;*/

        return $qb->getQuery()->getResult();

    }

    /**
     * @param $debut
     * @param $fin
     *
     * @return array
     */
    public function venteTotalparPeriode($debut, $fin)
    {
        $qb = $this->createQueryBuilder('v');
        $qb
            ->where('v.dateVente BETWEEN :firstDate AND :lastDate')
            ->setParameter('firstDate', $debut)
            ->setParameter('lastDate', $fin)
        ;


        $qb
            ->select('SUM(v.montantTransport+v.taxe) as montant_total')
            ->addSelect('COUNT(v.id) as nombre_vente')
            ->groupBy('v.id')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $debut
     * @param $fin
     * @param $etat
     *
     * @return array
     */
    public function venteTotalparPeriodeParEtat($debut, $fin, $etat)
    {
        $qb = $this->createQueryBuilder('v');
        $qb
            ->where('v.dateVente BETWEEN :firstDate AND :lastDate')
            ->setParameter('firstDate', $debut)
            ->setParameter('lastDate', $fin)
            ->andWhere('v.etat = :etat')
            ->setParameter('etat', $etat);


        $qb
            ->addSelect('SUM(v.montantTransport+v.taxe) as montant_total')
            ->addSelect('COUNT(v.id) as nombre_vente')
            ->groupBy('v.id')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $ids
     *
     * @return Vente[]
     */
    public function getVentesByIds(array $ids)
    {
        $qb = $this->createQueryBuilder('v');

        $qb
            ->where('v.id IN (:lisID)')
            ->setParameter('lisID', $ids);

        return $qb->getQuery()->getResult();
    }
}
