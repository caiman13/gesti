<?php

namespace App\Repository\Vente;

use App\Entity\Admin\Tiers;
use App\Entity\Vente\Facture;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Facture|null find($id, $lockMode = null, $lockVersion = null)
 * @method Facture|null findOneBy(array $criteria, array $orderBy = null)
 * @method Facture[]    findAll()
 * @method Facture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FactureRepository extends ServiceEntityRepository
{
    /**
     * FactureRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Facture::class);
    }

    /**
     * @param QueryBuilder $qb
     * @param $debut
     * @param $fin
     * @param bool $isAnd
     * @return QueryBuilder
     */
    private function __whereDate(QueryBuilder $qb, $debut, $fin, $isAnd = false)
    {       /*$mot = "123456789";
        $bout = substr($mot,0,4);
        dump($bout);*/
        $condition = 'f.dateFacture BETWEEN :firstDate AND :lastDate';

        $beginField = 'firstDate';
        $endField = 'lastDate';

        if ($isAnd) {
            $qb->andWhere($condition);
        } else {
            $qb->where($condition);
        }

        return $qb->setParameter($beginField, $debut)
            ->setParameter($endField, $fin);
    }


    /**
     * @param Tiers $client 'lastDate',
     * @param $etat
     * @param $debut
     * @param $fin
     * @return mixed
     */
    public function getByDateCliAndEtat(Tiers $client, $etat, $debut, $fin)
    {
        $qb = $this->createQueryBuilder('f');
        $qb
            ->where('f.tier = :client')
            ->setParameter('client', $client);

        $qb = $this->__whereDate($qb, $debut, $fin, true);

        $qb
            ->andWhere('f.payed = :etat')
            ->setParameter('etat', $etat)
            ->andWhere('f.annule = :annule')
            ->setParameter('annule', false)
            ->orderBy('f.dateFacture', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getLastReference()
    {
        $now = new DateTime();
        $annee = $now->format('y');

        $qb = $this->createQueryBuilder('f');
        $qb
            ->select('f.reference')
            ->where($qb->expr()->like('f.reference', ':year'))
            ->setParameter('year', '%' . $annee)
            ->andWhere('f.annule = :value')
            ->setParameter('value', false)
            ->orderBy('f.id', 'DESC');

        return current($qb->getQuery()->getResult());
    }

    /**
     * @return string
     * @throws Exception
     */
    public function generateRef()
    {
        $now = new DateTime();
        $sufixref = '/HSMV/' . $now->format('y');


        $lastRef = $this->getLastReference();
        if ($lastRef == false) {
            $reference = '0001' . $sufixref;
        } else {
            $bout = substr($lastRef['reference'], 0, 4);
            $int_ext = (int)$bout + 1;
            $reparse_val = (string)$int_ext;
            $longeur1 = strlen($reparse_val);
            $numfinal = '';
            switch ($longeur1) {
                case 1:
                    $numfinal = "000" . $int_ext;
                    break;
                case 2:
                    $numfinal = "00" . $int_ext;
                    break;
                case 3:
                    $numfinal = "0" . $int_ext;
                    break;
                case 4:
                    $numfinal = "" . $int_ext;
                    break;
            }
            $reference = $numfinal . $sufixref;
        }
        return $reference;
    }

    /**
     * @param $etat
     * @param $debut
     * @param $fin
     * @return mixed
     */
    public function getByDateEtat($etat, $debut, $fin)
    {
        $qb = $this->createQueryBuilder('f');

        $qb = $this->__whereDate($qb, $debut, $fin);

        $qb = $this->__whereDate($qb, $debut, $fin, true);

        $qb
            ->andWhere('f.payed = :etat')
            ->setParameter('etat', $etat)
            ->andWhere('f.annule = :annule')
            ->setParameter('annule', false)
            ->orderBy('f.dateFacture', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $debut
     * @param $fin
     * @return mixed
     */
    public function getAnnuleByDate($debut, $fin)
    {
        $qb = $this->createQueryBuilder('f');

        $qb = $this->__whereDate($qb, $debut, $fin);

        $qb
            ->andWhere('f.annule = :etat')
            ->setParameter('etat', true)
            ->orderBy('f.dateFacture', 'DESC');

        return $qb->getQuery()->getResult();

    }

    /**
     * @param Tiers $client
     * @param $debut
     * @param $fin
     * @return mixed
     */
    public function getAnnuleByCliByDate(Tiers $client, $debut, $fin)
    {
        $qb = $this->createQueryBuilder('f');
        $qb
            ->where('f.tier = :client')
            ->setParameter('client', $client);

        $qb
            ->andWhere('f.annule = :etat')
            ->setParameter('etat', true)
            ->orderBy('f.dateFacture', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $debut
     * @param $fin
     * @return mixed
     */
    public function getTotallfactreByperiode($debut, $fin)
    {
        $qb = $this->createQueryBuilder('f');
        $qb
            ->select('COUNT(f.id) as total')
            ->where('f.dateFacture BETWEEN :firstDate AND :lastDate')
            ->setParameter('firstDate', $debut)
            ->setParameter('lastDate', $fin);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $debut
     * @param $fin
     * @return mixed
     */
    public function getEchuFacture($debut, $fin)
    {
        $qb = $this->createQueryBuilder('f');
        $qb
            ->where('f.dateEcheance BETWEEN :firstDate AND :lastDate')
            ->setParameter('firstDate', $debut)
            ->setParameter('lastDate', $fin)
            ->andWhere('f.payed = :payed')
            ->setParameter('payed', false)
            ->andWhere('f.annule = :annule')
            ->setParameter('annule', false)
            ->orderBy('f.reference');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $ids
     * @param bool $idAndCli
     *
     * @return Facture[]
     */
    public function getFactureByIds(array $ids, $idAndCli = true)
    {
        $qb = $this->createQueryBuilder('f');

        if ($idAndCli){
            $qb
                ->select('f.id as id, t.id as idCli')
                ->join('f.tier', 't');
        }

        $qb
            ->where('f.id IN (:lisID)')
            ->setParameter('lisID', $ids);

        return $qb->getQuery()->getResult();
    }
}
