<?php

namespace App\Repository\Vente;

use App\Entity\Vente\PaiementFacture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PaiementFacture|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaiementFacture|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaiementFacture[]    findAll()
 * @method PaiementFacture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaiementFactureRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PaiementFacture::class);
    }

    // /**
    //  * @return PaiementFacture[] Returns an array of PaiementFacture objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PaiementFacture
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
