<?php

namespace App\Repository\Vente;

use App\Entity\Vente\Facture_annule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Facture_annule|null find($id, $lockMode = null, $lockVersion = null)
 * @method Facture_annule|null findOneBy(array $criteria, array $orderBy = null)
 * @method Facture_annule[]    findAll()
 * @method Facture_annule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class Facture_annuleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Facture_annule::class);
    }

    // /**
    //  * @return Facture_annule[] Returns an array of Facture_annule objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Facture_annule
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
