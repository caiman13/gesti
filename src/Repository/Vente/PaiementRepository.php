<?php

namespace App\Repository\Vente;

use App\Entity\Admin\Tiers;
use App\Entity\Vente\Paiement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Paiement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Paiement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Paiement[]    findAll()
 * @method Paiement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaiementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Paiement::class);
    }

    public function getByDateCliAndEtat(Tiers $client, $etat, $debut, $fin)
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->where('p.client = :client')
            ->setParameter('client',$client)
            ->andWhere('p.datePaiement BETWEEN :firstDate AND :lastDate')
            ->setParameter('firstDate',$debut)
            ->setParameter('lastDate',$fin)
            ->andWhere('p.valide = :etat')
            ->setParameter('etat', $etat)
            ->orderBy('p.datePaiement', 'DESC')
        ;

        return $qb->getQuery()->getResult();
    }

    public function  getByDateEtat($etat, $debut, $fin)
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->where('p.datePaiement BETWEEN :firstDate AND :lastDate')
            ->setParameters(['firstDate' => $debut, 'lastDate' => $fin])
            ->andWhere('p.valide = :etat')
            ->setParameter('etat', $etat)
            ->orderBy('p.datePaiement', 'DESC')
        ;
        return $qb->getQuery()->getResult();
    }

    public function getTotalPaementperiode($debut, $fin)
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->where('p.datePaiement BETWEEN :firstDate AND :lastDate')
            ->setParameters(['firstDate' => $debut, 'lastDate' => $fin])
            ->addSelect('COUNT(p.id) as total')
            ->groupBy('p.id')
        ;
        return $qb->getQuery()->getScalarResult();
    }

    public function getTotalPaementperiodeEtat($debut, $fin)
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->where('p.datePaiement BETWEEN :firstDate AND :lastDate')
            ->setParameters(['firstDate' => $debut, 'lastDate' => $fin])
            ->select('COUNT(p.id) as total')
            ->addSelect('CASE WHEN(p.valide = 0) THEN \'non_valide\' ELSE \'valide\' END as etat')
            ->groupBy('p.valide')
        ;
        return $qb->getQuery()->getResult();
    }

    public function getByTypeByEtatByCli($etat, $id_cli, $debut, $fin, $modPay)
    {
        $valide = ($etat == 'valide')?true:false;
        $qb = $this->createQueryBuilder('p');
        $qb
            ->where('p.datePaiement BETWEEN :firstDate AND :lastDate')
            ->setParameters(['firstDate' => $debut, 'lastDate' => $fin])
            ->andWhere('p.valide = :etat')
            ->setParameter('etat', $valide)
        ;
        if($id_cli != 0){
            $qb
                ->join('p.facture','f')
                ->join('f.tier', 'tier')
                ->andWhere('tier.id = :idtier')
                ->setParameter('idtier', $id_cli)
            ;
        }
        /* dump($valide);*/
        if($modPay != 0)
        {
            $qb
                ->join('p.moyenPay','m')
                ->andWhere('m.id = :idpay')
                ->setParameter('idpay', $modPay)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    public function getAllPaiementIn(array $inCondition)
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->where('p.id IN (:lisID)')
            ->setParameter('lisID', $inCondition);

        return $qb->getQuery()->getResult();
    }
}
