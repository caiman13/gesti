<?php

namespace App\Repository\Vente;

use App\Entity\Vente\BonCommande;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BonCommande|null find($id, $lockMode = null, $lockVersion = null)
 * @method BonCommande|null findOneBy(array $criteria, array $orderBy = null)
 * @method BonCommande[]    findAll()
 * @method BonCommande[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BonCommandeRepository extends ServiceEntityRepository
{
    /**
     * BonCommandeRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BonCommande::class);
    }

    /**
     * @param QueryBuilder $qb
     * @param $debut
     * @param $fin
     * @return QueryBuilder
     */
    private function __wheredate(QueryBuilder $qb, $debut, $fin)
    {
        return $qb->where('bc.dateBC BETWEEN :firstDate AND :lastDate')
            ->setParameter('firstDate', $debut)
            ->setParameter('lastDate', $fin);
    }

    /**
     * @param QueryBuilder $qb
     * @param $etat
     * @return QueryBuilder
     */
    private function __whereEtat(QueryBuilder $qb, $etat)
    {
        return $qb->andWhere('bc.etat = :etat')
            ->setParameter('etat', $etat);
    }

    /**
     * @param $debut
     * @param $fin
     * @return array
     */
    public function totalBcPeriode($debut, $fin)
    {
        $qb = $this->createQueryBuilder('bc');

        $qb = $this->__wheredate($qb, $debut, $fin);

        $qb
            ->select('COUNT(bc.id) as totalBc')
        ;

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param $debut
     * @param $fin
     * @param $etat
     * @return array
     */
    public function totalBcPeriodeEtat($debut, $fin)
    {
        $qb = $this->createQueryBuilder('bc');

        $qb = $this->__wheredate($qb, $debut, $fin);

        $qb
            ->select('COUNT(bc.id) as totalBc')
            ->addSelect('bc.etat')
            ->groupBy('bc.etat')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $debut
     * @param $fin
     * @param string $etat
     * @return mixed
     */
    public function bcEmmisJoinFournisseurs($debut, $fin, $etat = 'vendu')
    {
        $qb = $this->createQueryBuilder('bc');
        $qb
            ->select('f.raisonSociale')
            ->join('bc.tier', 'f');

        $qb = $this->__wheredate($qb, $debut, $fin);

        $qb = $this->__whereEtat($qb, $etat);

        $qb
            ->groupBy('f.raisonSociale')
            ->orderBy('f.raisonSociale');

        $qb
            ->addSelect('SUM(bc.montantTransport+bc.montantTaxe) as montant_total')
            ->addSelect('COUNT(bc.id) as nombre');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $etat
     * @param $id_cli
     * @param $debut
     * @param $fin
     * @return mixed
     */
    public function bcByFRNSByDateByEtat($etat, $id_cli, $debut, $fin)
    {
        $qb = $this->createQueryBuilder('bc');
        $qb
            ->where('bc.etat = :etat')
            ->setParameter('etat', $etat)
            ->join('bc.tier', 'frns')
            ->andWhere('frns.id = :id_cli')
            ->setParameter('id_cli', $id_cli)
            ->andWhere('bc.dateBC BETWEEN :firstDate AND :lastDate')
            ->setParameter('firstDate', $debut)
            ->setParameter('lastDate', $fin);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getLastReference()
    {
        $now = new \DateTime();
        $annee = $now->format('y');

        $qb = $this->createQueryBuilder('b');
        $qb
            ->select('b.reference')
            ->where($qb->expr()->like('b.reference', ':year'))
            ->setParameter('year', '%' . $annee)
            /*->andWhere('f.annule = :value')
            ->setParameter('value', true)*/
            ->orderBy('b.id', 'DESC');

        return current($qb->getQuery()->getResult());
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function generateRef()
    {
        $now = new \DateTime();
        $sufixref = '/HSMV/' . $now->format('y');

        $lastRef = $this->getLastReference();
        if ($lastRef == false) {
            $reference = 'BC-0001' . $sufixref;
        } else {
            $bout = substr($lastRef['reference'], 3, 4);
            $int_ext = (int)$bout + 1;
            $reparse_val = (string)$int_ext;
            $longeur1 = strlen($reparse_val);
            $numfinal = '';
            switch ($longeur1) {
                case 1:
                    $numfinal = "000" . $int_ext;
                    break;
                case 2:
                    $numfinal = "00" . $int_ext;
                    break;
                case 3:
                    $numfinal = "0" . $int_ext;
                    break;
                case 4:
                    $numfinal = "" . $int_ext;
                    break;
            }
            $reference = 'BC-' . $numfinal . $sufixref;
        }
        return $reference;
    }
}
