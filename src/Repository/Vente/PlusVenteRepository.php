<?php

namespace App\Repository\Vente;

use App\Entity\Vente\Plus_vente;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Plus_vente|null find($id, $lockMode = null, $lockVersion = null)
 * @method Plus_vente|null findOneBy(array $criteria, array $orderBy = null)
 * @method Plus_vente[]    findAll()
 * @method Plus_vente[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlusVenteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Plus_vente::class);
    }

    // /**
    //  * @return Plus_vente[] Returns an array of Plus_vente objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Plus_vente
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
