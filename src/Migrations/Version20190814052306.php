<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190814052306 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE logs (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, description VARCHAR(255) NOT NULL, date DATE NOT NULL, INDEX IDX_F08FC65CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql('ALTER TABLE logs ADD CONSTRAINT FK_F08FC65CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('DROP TABLE mail_param');
        $this->addSql('ALTER TABLE reglement CHANGE cie_id cie_id INT DEFAULT NULL');
        $this->addSql(
            'ALTER TABLE note_credit CHANGE facture_id facture_id INT DEFAULT NULL, CHANGE client_id client_id INT DEFAULT NULL'
        );
        $this->addSql(
            'ALTER TABLE avoir CHANGE client_id client_id INT DEFAULT NULL, CHANGE facture_id facture_id INT DEFAULT NULL, CHANGE reference reference VARCHAR(255) DEFAULT NULL'
        );
        $this->addSql('ALTER TABLE ligne_compte CHANGE plan_compte plan_compte VARCHAR(255) DEFAULT NULL');
        $this->addSql(
            'ALTER TABLE tiers CHANGE responsable_id responsable_id INT DEFAULT NULL, CHANGE societe_id societe_id INT DEFAULT NULL, CHANGE logo_id logo_id INT DEFAULT NULL, CHANGE nom nom VARCHAR(255) DEFAULT NULL, CHANGE prenom prenom VARCHAR(255) DEFAULT NULL, CHANGE raisonSociale raisonSociale VARCHAR(255) DEFAULT NULL, CHANGE mail mail VARCHAR(255) DEFAULT NULL, CHANGE type_soc type_soc VARCHAR(255) DEFAULT NULL, CHANGE type_cli type_cli VARCHAR(255) DEFAULT NULL, CHANGE num_stat num_stat VARCHAR(255) DEFAULT NULL, CHANGE num_nif num_nif VARCHAR(255) DEFAULT NULL, CHANGE num_rcs num_rcs VARCHAR(255) DEFAULT NULL, CHANGE num_cif num_cif VARCHAR(255) DEFAULT NULL, CHANGE date_cif date_cif DATE DEFAULT NULL, CHANGE num_compte num_compte VARCHAR(255) DEFAULT NULL'
        );
        $this->addSql(
            'ALTER TABLE vente CHANGE currency_code currency_code VARCHAR(255) DEFAULT NULL, CHANGE fees_id fees_id INT DEFAULT NULL, CHANGE classes_id classes_id INT DEFAULT NULL, CHANGE facture_id facture_id INT DEFAULT NULL, CHANGE cie_id cie_id INT DEFAULT NULL, CHANGE bc_id bc_id INT DEFAULT NULL, CHANGE num_billet num_billet VARCHAR(255) DEFAULT NULL, CHANGE book_ref book_ref VARCHAR(255) DEFAULT NULL, CHANGE montant_transport montant_transport INT DEFAULT NULL, CHANGE taxe taxe INT DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT NULL'
        );
        $this->addSql(
            'ALTER TABLE facture_annule CHANGE client_id client_id INT DEFAULT NULL, CHANGE reference reference VARCHAR(255) DEFAULT NULL'
        );
        $this->addSql(
            'ALTER TABLE paiement CHANGE curency curency VARCHAR(255) DEFAULT NULL, CHANGE mode_id mode_id INT DEFAULT NULL, CHANGE note_credit_id note_credit_id INT DEFAULT NULL, CHANGE avoir_id avoir_id INT DEFAULT NULL, CHANGE file_scan_id file_scan_id INT DEFAULT NULL, CHANGE client_id client_id INT DEFAULT NULL, CHANGE related_note_id related_note_id INT DEFAULT NULL, CHANGE montant_recu montant_recu VARCHAR(255) DEFAULT NULL, CHANGE num_cheque num_cheque VARCHAR(255) DEFAULT NULL, CHANGE ref_operation ref_operation VARCHAR(255) DEFAULT NULL, CHANGE autre_info autre_info VARCHAR(255) DEFAULT NULL, CHANGE nom_banque nom_banque VARCHAR(255) DEFAULT NULL'
        );
        $this->addSql(
            'ALTER TABLE facture CHANGE client_id client_id INT DEFAULT NULL, CHANGE file_scan_id file_scan_id INT DEFAULT NULL, CHANGE reference reference VARCHAR(255) DEFAULT NULL, CHANGE montat_rembourse montat_rembourse DOUBLE PRECISION DEFAULT NULL, CHANGE change_taux change_taux VARCHAR(255) DEFAULT NULL, CHANGE date_echeance date_echeance DATE DEFAULT NULL'
        );
        $this->addSql(
            'ALTER TABLE bonCommande CHANGE fournissuer_id fournissuer_id INT DEFAULT NULL, CHANGE cur_iso cur_iso VARCHAR(255) DEFAULT NULL, CHANGE reference reference VARCHAR(255) DEFAULT NULL, CHANGE pnr pnr VARCHAR(255) DEFAULT NULL, CHANGE pnr_propre pnr_propre VARCHAR(255) DEFAULT NULL, CHANGE montant_transport montant_transport VARCHAR(255) DEFAULT NULL, CHANGE montant_taxe montant_taxe VARCHAR(255) DEFAULT NULL, CHANGE fee_agv fee_agv VARCHAR(255) DEFAULT NULL'
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE mail_param (id INT AUTO_INCREMENT NOT NULL, server VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, username VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, pass VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, port VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' '
        );
        $this->addSql('DROP TABLE logs');
        $this->addSql(
            'ALTER TABLE avoir CHANGE client_id client_id INT DEFAULT NULL, CHANGE facture_id facture_id INT DEFAULT NULL, CHANGE reference reference VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci'
        );
        $this->addSql(
            'ALTER TABLE bonCommande CHANGE fournissuer_id fournissuer_id INT DEFAULT NULL, CHANGE cur_iso cur_iso VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE reference reference VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE pnr pnr VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE pnr_propre pnr_propre VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE montant_transport montant_transport VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE montant_taxe montant_taxe VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE fee_agv fee_agv VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci'
        );
        $this->addSql(
            'ALTER TABLE facture CHANGE client_id client_id INT DEFAULT NULL, CHANGE file_scan_id file_scan_id INT DEFAULT NULL, CHANGE reference reference VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE montat_rembourse montat_rembourse DOUBLE PRECISION DEFAULT \'NULL\', CHANGE change_taux change_taux VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE date_echeance date_echeance DATE DEFAULT \'NULL\''
        );
        $this->addSql(
            'ALTER TABLE facture_annule CHANGE client_id client_id INT DEFAULT NULL, CHANGE reference reference VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci'
        );
        $this->addSql(
            'ALTER TABLE ligne_compte CHANGE plan_compte plan_compte VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci'
        );
        $this->addSql(
            'ALTER TABLE note_credit CHANGE facture_id facture_id INT DEFAULT NULL, CHANGE client_id client_id INT DEFAULT NULL'
        );
        $this->addSql(
            'ALTER TABLE paiement CHANGE curency curency VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE mode_id mode_id INT DEFAULT NULL, CHANGE client_id client_id INT DEFAULT NULL, CHANGE note_credit_id note_credit_id INT DEFAULT NULL, CHANGE avoir_id avoir_id INT DEFAULT NULL, CHANGE file_scan_id file_scan_id INT DEFAULT NULL, CHANGE related_note_id related_note_id INT DEFAULT NULL, CHANGE montant_recu montant_recu VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE num_cheque num_cheque VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE ref_operation ref_operation VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE autre_info autre_info VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE nom_banque nom_banque VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci'
        );
        $this->addSql('ALTER TABLE reglement CHANGE cie_id cie_id INT DEFAULT NULL');
        $this->addSql(
            'ALTER TABLE tiers CHANGE responsable_id responsable_id INT DEFAULT NULL, CHANGE societe_id societe_id INT DEFAULT NULL, CHANGE logo_id logo_id INT DEFAULT NULL, CHANGE nom nom VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE prenom prenom VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE raisonSociale raisonSociale VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE mail mail VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE type_soc type_soc VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE type_cli type_cli VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE num_stat num_stat VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE num_nif num_nif VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE num_rcs num_rcs VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE num_cif num_cif VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE date_cif date_cif DATE DEFAULT \'NULL\', CHANGE num_compte num_compte VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci'
        );
        $this->addSql(
            'ALTER TABLE vente CHANGE currency_code currency_code VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE fees_id fees_id INT DEFAULT NULL, CHANGE classes_id classes_id INT DEFAULT NULL, CHANGE facture_id facture_id INT DEFAULT NULL, CHANGE cie_id cie_id INT DEFAULT NULL, CHANGE bc_id bc_id INT DEFAULT NULL, CHANGE num_billet num_billet VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE book_ref book_ref VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE montant_transport montant_transport INT DEFAULT NULL, CHANGE taxe taxe INT DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci'
        );
    }
}
