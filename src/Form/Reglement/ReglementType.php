<?php

namespace App\Form\Reglement;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReglementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('date', DateType::class,[
            'attr' => [
                'class' => 'field'
            ],
            'widget' => 'single_text',
            'label'  => 'Date de règlement',
            'data'   => new \DateTime()
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Reglement\Reglement'
        ));
    }

}
