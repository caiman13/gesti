<?php

namespace App\Form\Reglement;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InfoVenteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cieName', HiddenType::class, [
            ])
            ->add('ticketNumber', HiddenType::class, [
            ])
            ->add('nameCie', TextType::class, [
                'attr' => [
                    'disabled' => 'true'
                ],
                'label'  => 'Nom de la compagnie',
            ])
            ->add('montant', TextType::class, [
                'attr' => [
                    'class' => 'field'
                ],
                'label'  => 'Montant de remboursement',
            ])
            ->add('passager', HiddenType::class, [
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Reglement\InfoVente'
        ));
    }

    public function getBlockPrefix()
    {
        return 'InfoVenteType';
    }
}
