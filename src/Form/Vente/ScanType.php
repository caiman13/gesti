<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 30/08/2017
 * Time: 09:36
 */

namespace App\Form\Vente;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ScanType
 * @package App\Form\Vente
 */
class ScanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, array(
                'attr' => [
                    'class' => 'field form-control',
                ],
                'label' => 'Fichier à scanner (PDF)',
            ));
    }

    /*public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VenteBundle\MyObject\Scan'
        ));
    }*/
}