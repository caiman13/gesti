<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 18/09/2017
 * Time: 15:09
 */

namespace App\Form\Vente;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PVenteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('bookRef', TextType::class, [
                'attr' => [
                    'class' => 'field'
                ],
                /*'row_attr' => 'type-pers',*/
                'label' => 'Book-ref',
            ])
            ->add('numBillet', TextType::class, array(
                'attr' => [
                    'class' => 'field',
                ],
                'label' => 'Numero billet',
            ))
            ->add('passager', TextType::class, [
                'attr' => [
                    'class' => 'field'
                ],
                'label' => 'Passager'
            ])
            ->add('fees', EntityType::class, array(
                'class' => 'App\Entity\Admin\Fees',
                'attr' => [
                    'class' => 'field fee-change',
                ],
                'choice_label' => 'libCourt',
                'label' => 'Type de Vol',
            ))
            ->add('classes', EntityType::class, [
                'class' => 'App\Entity\Admin\Classes',
                'attr' => [
                    'class' => 'field',
                ],
                'choice_label' => 'classes',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Vente\Vente'
        ));
    }

    public function getBlockPrefix()
    {
        return 'preventeType';
    }
}