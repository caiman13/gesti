<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 29/08/2017
 * Time: 12:38
 */

namespace App\Form\Vente;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VenteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numBillet', TextType::class, array(
                'attr' => [
                    'class' => 'field',
                ],
                'label' => 'Numero billet',
            ))
            ->add('bookRef',TextType::class, [
                'attr' => [
                    'class' => 'field'
                ],
                /*'row_attr' => 'type-pers',*/
                'label' => 'Book-ref',
            ])
            ->add('currency',EntityType::class, [
                'class' =>'App\Entity\Admin\Currency',
                'attr' => [
                    'class' => 'field'
                ],
                'choice_label' => 'code_iso',
                'label' => 'Unité monétaire'
            ])
            ->add('montantTransport',TextType::class, [
                'attr' => [
                    'class' => 'field'
                ],
                'label' => 'Montant du transport'
            ])
            ->add('taxe',TextType::class, [
                'attr' => [
                    'class' => 'field'
                ],
                'label' => 'Montant du taxe'
            ])
            ->add('fees', EntityType::class, array(
                'class' => 'App\Entity\Admin\Fees',
                'attr' => [
                    'class' => 'field',
                ],
                'choice_label' => 'libCourt',
                'label' => 'Type de Vol',
            ))
            ->add('classes', EntityType::class, [
                'class' => 'App\Entity\Admin\Classes',
                'attr' => [
                    'class' => 'field',
                ],
                'choice_label' => 'classes',
            ])
            ->add('type',ChoiceType::class, array(
                'choices'   => array(
                    'Choissez un type' => '',
                    'Aller simple' => 'simple',
                    'Aller-retour' => 'allerRetour',

                ),
                'attr' => [
                    'class' => 'field util',
                ],
                'label' => 'Type',
                'required'  => true,
            ))
            ->add('passager', TextType::class, [
                'attr' => [
                    'class' => 'field'
                ],
                'label' => 'Voyageur'
            ])
            ->add('compagieAerienne', EntityType::class, [
                'class' => 'App\Entity\Admin\Tiers',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->where('t.typeSoc = ?1')
                        ->setParameter(1,'Compagnie aériènne');
                },
                'attr' => [
                    'class' => 'field'
                ],
                'label' => 'Compagnie aériènne'
            ])
            ->add('parcours', CollectionType::class, array(
                'entry_type'   => ParcourType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'attr' => array(
                    'class' => 'my-selector',
                ),
                'label' => 'Les itinéraires'

            ))
            ->add('typeBillet', ChoiceType::class, [
                'choices'   => array(
                    'normal' => 'normal',
                    'autre' => 'autre',

                ),
                'attr' => [
                    'class' => 'field util',
                ],
                'label' => 'Type de billet',
                'required'  => true,
            ])
            ->add('remboursable', ChoiceType::class, [
                'choices'   => array(
                    'Choisissez' => '',
                    'Non' => '0',
                    'Oui' => '1',

                ),
                'attr' => [
                    'class' => 'field util',
                ],
                'label' => 'Remboursable',
                'required'  => true,
            ])
            ->add('autreFees', TextType::class, [
                'label' => 'Aure FEES',
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Vente\Vente'
        ));
    }
}