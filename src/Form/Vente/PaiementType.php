<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 04/09/2017
 * Time: 09:10
 */

namespace App\Form\Vente;

use App\Entity\Admin\PaiementMode;
use App\Entity\Vente\Paiement;
use App\Entity\Vente\PaiementFacture;
use DateTime;
use Doctrine\ORM\EntityRepository;
use Exception;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaiementType extends AbstractType
{

    /**
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @throws Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('montantRecu', TextType::class, [
                'attr' => [
                    'class' => 'acompte text-right'
                ],
                'label'=> 'Montant'
            ])
            ->add('moyenPay', EntityType::class, [
                'class' => PaiementMode::class,
                'query_builder' => function (EntityRepository $er) use ($options) {
                    $qb = $er->createQueryBuilder('n');
                    $qb->where('1 = 1');

                    $expr = $qb->expr();
                    if($options['avoir'] === false ){
                        $qb
                            ->andWhere($expr->neq('n.id',7))
                        ;
                    }

                    if($options['note'] === false ){
                        $qb
                            ->andWhere('n.id != 6')
                            /*->andWhere($expr->neq('n.id',5))*/
                        ;
                    }

                    return $qb;
                },
                'attr' => [
                    'class' => 'field'
                ],
                'choice_label' => 'libelle',
                'label' => 'Mode de paiement'
            ])
            ->add('refOperation', TextType::class, [
                'attr' => [
                    'class' => 'carte',
                ],
                'label' => 'Reference de l\'opération',
            ])
            ->add('numCheque', TextType::class, [
                'attr' => [
                    'class' => 'cheque',
                ],
                'label' => 'Numéro du chèque',
            ])
            ->add('nomBanque', TextType::class, [
                'attr' => [
                    'class' => 'cheque',
                ],
                'label' => 'Nom de la banque'
            ])
            ->add('datePaiement', DateType::class , [
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'field',
                ],
                'label' => 'Date de paiement',
                'data' => new DateTime('now')
            ])
            ->add('montantDevise', TextType::class, [
                'attr' => [
                    'class' => 'devise',
                ],
                'label' => 'Montant devise'
            ])
            ->add('change', TextType::class,[
                'attr' => [
                    'class' => 'devise',
                ],
                'label' => 'Taux de change'
            ])
            ->add('noteCredit', EntityType::class, [
                'class' => 'App\Entity\Reglement\NoteCredit',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    $qb = $er->createQueryBuilder('n')
                        ->where('n.payed = ?1')
                        ->setParameter(1,false);
                    if(isset($options['client'])){
                        $qb
                            ->andWhere('n.client = :client')
                            ->setParameter('client', $options['client'])
                        ;
                    }
                    return $qb;
                },
                'attr' => [
                    'class' => 'note-cred'
                ],
                'label' => 'Note de credit'
            ])
            ->add('avoir', EntityType::class, [
                'class' => 'App\Entity\Reglement\Avoir',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    $qb = $er->createQueryBuilder('n')
                        ->where('n.payed = ?1')
                        ->setParameter(1,false);
                    if(isset($options['client'])){
                        $qb
                            ->andWhere('n.client = :client')
                            ->setParameter('client', $options['client'])
                        ;
                    }
                    return $qb;
                },
                'attr' => [
                    'class' => 'avoir'
                ],
                'label' => 'Avoir'
            ])
            ->add('currency', EntityType::class, [
                'class' => 'App\Entity\Admin\Currency',
                'choice_label' => 'code_iso',
                'attr' => [
                    'class' => 'devise'
                ],
                'label' => 'Unité monetaire'
            ])
            ->add('autreInfo', TextType::class, [
                'attr' => [
                    'class' => 'autre'
                ],
                'data' => '',
                'label' => 'Autre info'
            ])
            ->add('fileScan', FileType::class, [
                'attr' => [
                    'class' => 'cheque carte form-controle'
                ],
                'data' => '',
                'label' => 'Pièce jointe'
            ])
            ->add('paiementFactures', CollectionType::class, array(
                'entry_type'   => PaiementFactureType::class,
                'allow_add' => false,
                'allow_delete' => false,
                'prototype' => true,
                'attr' => array(
                    'class' => 'my-selector',
                ),
                'label' => 'Les factures'
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Paiement::class,
            'client'     => null,
            'avoir'      => null,
            'note'       => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'PaiementType';
    }
}