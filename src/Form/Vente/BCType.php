<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 18/09/2017
 * Time: 09:28
 */

namespace App\Form\Vente;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BCType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tier', EntityType::class, [
                'class' => 'App\Entity\Admin\Tiers',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->where('t.discriminator = ?1')
                        ->setParameter(1,'fournisseurs')
                        ->andWhere('t.linked = ?2')
                        ->setParameter('2',false);
                },
                'attr' => [
                    'class' => 'field'
                ],
                'label' => 'Fournisseurs'
            ])
            ->add('pnr', TextType::class, [
                'attr' => [
                    'class' => 'field'
                ],
                'label' => 'PNR'
            ])
            ->add('pnrPropre', TextType::class, [
                'label' => 'PNR propre'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Vente\BonCommande'
        ));
    }
}