<?php

namespace App\Form\Vente;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParcourType extends AbstractType
{
    private $class_choice = [
        'A' => 'A',
        'B' =>'B',
        'C' => 'C',
        'D' => 'D',
        'E' => 'E',
        'F' => 'F',
        'G' => 'G',
        'H' => 'H',
        'I' => 'I',
        'J' => 'J',
        'K' => 'K',
        'L' => 'L',
        'M' => 'M',
        'N' => 'N',
        'O' => 'O',
        'P' => 'P',
        'Q' => 'Q',
        'R' => 'R',
        'S' => 'S',
        'T' => 'T',
        'U' => 'U',
        'V' => 'V',
        'W' => 'W',
        'X' => 'X',
        'Y' => 'Y',
        'Z' => 'Z',
        'NA' => 'NA',
    ];
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fromName', TextType::class ,[
                'attr' => [
                    'class' => 'field autocomplete'
                ],
                'label' => 'Depart'
            ])
            ->add('toName', TextType::class, [
                'attr' => [
                    'class' => 'field autocomplete'
                ],
                'label' => 'Destination'
            ])
            ->add('classeBlt', ChoiceType::class, [
                'choices'   => $this->class_choice,
                'attr' => [
                    'class' => 'field select2',
                ],
                'label' => 'Classe',
                'required'  => true,
            ])
            ->add('dateParcour', DateType::class, [
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'field'
                ],
                'label' => 'Date du parcours'
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Vente\Parcour'
        ));
    }

    public function getBlockPrefix()
    {
        return 'ParcourType';
    }
}
