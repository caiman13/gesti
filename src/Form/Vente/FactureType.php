<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 04/09/2017
 * Time: 06:34
 */

namespace App\Form\Vente;

use DateTime;
use Exception;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FactureType extends AbstractType
{

    /**
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $today = new DateTime('now');
        $builder
            /*->add('tier', EntityType::class, [
                'class' => 'App\Entity\Admin\Tiers',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->where('t.discriminator = ?1')
                        ->setParameter(1,'client');
                },
                'attr' => [
                    'class' => 'field'
                ],
                'label' => 'Client'

            ])*/
            ->add('idLi', ChoiceType::class, [
                'choices'   => $options['cli_choice'],
                'attr' => [
                    'class' => 'field util',
                ],
                'label' => 'Client',
                'required'  => true,
            ])

            /*->add('tauxChange', TextType::class, [
                'attr' => [
                    'class' => 'field'
                ],
                'label'=> 'Taux de change'
            ])*/
            ->add('typePaiement', ChoiceType::class, [
                    'choices'   => array(
                        'A crédit' => 'credit',
                        'Au comptant' => 'comptant',

                    ),
                    'attr' => [
                        'class' => 'field util',
                    ],
                    'label' => 'Type de paiement',
                    'required'  => true,
            ])
            ->add('dateEcheance', DateType::class, [
                'attr' => [
                    'class' => 'acompte'
                ],
                'widget' => 'single_text',
                'label'  => 'Date d\'échéance',
                'data'   => $today->modify('+15 day')
            ])
            /*->add('paiements', CollectionType::class, [
                'entry_type'   => PaiementType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'attr' => [
                    'class' => 'my-selector',
                ],
            ])*/
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Vente\Facture',
            'cli_choice' => null,
        ));
    }
}