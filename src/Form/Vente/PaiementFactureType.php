<?php

namespace App\Form\Vente;

use App\Entity\Vente\Facture;
use App\Entity\Vente\PaiementFacture;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaiementFactureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('factureref', TextType::class,[
                'attr'  => [
                    'readonly' => 'true',
                    'class'    => 'text-right'
                ],
                'label' => 'Factue N°',
            ])
            ->add('montant', TextType::class, [
                'attr' => [
                    'class' => 'field montant-facture text-right'
                ],
                'label' => 'Montant',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PaiementFacture::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'PayementFactureType';
    }
}
