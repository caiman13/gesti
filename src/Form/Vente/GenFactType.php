<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 18/09/2017
 * Time: 14:11
 */

namespace App\Form\Vente;

use App\MyObject\GenererVente;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GenFactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ventes', CollectionType::class , [
                'entry_type'   => PVenteType::class,
                'allow_add' => false,
                'allow_delete' => false,
                'prototype' => true,
                'by_reference' => false,
                'entry_options' => array('label' => false),
                'attr' => array(
                    'class' => 'my-selector',
                ),
                'label' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => GenererVente::class
        ));
    }
}