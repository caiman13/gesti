<?php

namespace App\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClassesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('classes',ChoiceType::class, array(
                'choices'   => array(
                    'Speciale' => 'speciale',
                    'Economique' => 'économique',
                    'Affaire' => 'affaires',

                ),
                'attr' => [
                    'class' => 'field util',
                ],
                'label' => 'Type de classe',
                'required'  => true,
            ))
            ->add('simple',TextType::class, [
                'attr' => [
                    'class' => 'field util'
                ],
                /*'row_attr' => 'type-pers',*/
                'label' => 'Tarif aller simple',
            ])
            ->add('allerRetour',TextType::class, [
                'attr' => [
                    'class' => 'field util'
                ],
                /*'row_attr' => 'type-pers',*/
                'label' => 'Tarif aller-retour',
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Admin\Classes'
        ));
    }

    public function getBlockPrefix()
    {
        return 'ClassesType';
    }
}
