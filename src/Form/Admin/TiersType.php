<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 02/08/2017
 * Time: 11:05
 */

namespace App\Form\Admin;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TiersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, array(
                'choices'   => array(
                    'Société' => 'societe',
                    'Particulier' => 'personne',
                ),
                'attr' => [
                    'class' => 'field',
                ],
                'label' => 'Nature',
                'required'  => true,
            ))
            ->add('typeSoc', ChoiceType::class, array(
                'choices'   => array(
                    'Agence de voyage' => 'Agence de voyage',
                    'Compagnie aérienne' => 'Compagnie aérienne',
                ),
                'attr' => [
                    'class' => 'field',
                ],
                'label' => 'Type',
                'required'  => true,
            ))
            ->add('nom',TextType::class, [
                'attr' => [
                    'class' => 'type-pers'
                ],
                /*'row_attr' => 'type-pers',*/
                'label' => 'Nom',
            ])
            ->add('prenom',TextType::class, [
                'attr' => [
                    'class' => 'type-pers'
                ],
                'label' => 'Prenom'
            ])
            ->add('raisonSociale',TextType::class, [
                'attr' => [
                    'class' => 'type-soc'
                ],
                'label' => 'Raison sociale'
            ])
            ->add('numNIF',TextType::class, [
                'attr' => [
                    'class' => 'type-soc util-soc'
                ],
                'label' => 'NIF'
            ])
            ->add('numStat',TextType::class, [
                'attr' => [
                    'class' => 'type-soc util-soc'
                ],
                'label' => 'STAT'
            ])

            ->add('adresse',TextType::class, [
                'attr' => [
                    'class' => 'field'
                ],
                'label' => 'Adresse'
            ])
            ->add('mail', EmailType::class, [
                /*'attr' => [
                    'class' => 'field'
                ],*/
                'label' => 'Mail'
            ])
            ->add('contacts', CollectionType::class, array(
                'entry_type'   => ContactType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'attr' => array(
                    'class' => 'my-selector',
                ),
                'label' => 'Les contacts'
            ))
            ->add('typeCli', ChoiceType::class, array(
                'choices'   => array(
                    'Autre' => 'autre',
                    'Client groupe' => 'client-grp',
                ),
                'attr' => [
                    'class' => 'field',
                ],
                'label' => 'Type de client',
                'required'  => true,
            ))
            ->add('numRcs', TextType::class, [
                'attr' => [
                    'class' => 'type-soc'
                ],
                'label' => 'RCS'
            ])
            ->add('numCIF', TextType::class, [
                'attr' => [
                    'class' => 'type-soc'
                ],
                'label' => 'CIF'
            ])
            ->add('dateCIF', DateType::class, [
                'attr' => [
                    'class' => 'type-soc'
                ],
                'widget' => 'single_text',
                'label' => 'Date CIF'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Admin\Tiers'
        ));
    }

    public function getBlockPrefix()
    {
        return 'TiersType';
    }

}