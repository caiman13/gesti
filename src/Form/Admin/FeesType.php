<?php
/**
 * Created by PhpStorm.
 * User: DEV
 * Date: 25/08/2017
 * Time: 09:13
 */

namespace App\Form\Admin;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FeesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libCourt',TextType::class, [
                'attr' => [
                    'class' => 'field'
                ],
                /*'row_attr' => 'type-pers',*/
                'label' => 'LIbellé court',
            ])
            ->add('libLong',TextType::class, [
                'attr' => [
                    'class' => 'field'
                ],
                'label' => 'Libellé long'
            ])
            ->add('classes', CollectionType::class, array(
                'entry_type'   => ClassesType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'attr' => array(
                    'class' => 'my-selector',
                ),

            ))
        ;
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Admin\Fees'
        ));
    }
}