<?php

namespace App\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description',TextType::class, [
                'attr' => [
                    'class' => 'field util'
                ],
                'label' => 'Type'
            ])
            ->add('contact',TextType::class, [
                'attr' => [
                    'class' => 'field util'
                ],
                'label' => 'Contact'
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Admin\Contact'
        ));
    }

    public function getBlockPrefix()
    {
        return 'ContactType';
    }
}
