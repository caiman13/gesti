<?php

namespace App\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('raisonSociale',TextType::class,[
                'attr' => [
                    'class' => 'field util'
                ],
                'label' => 'Raison sociale'
            ])
            ->add('nom',TextType::class,[
                'attr' => [
                    'class' => 'field util'
                ],
                'label' => 'Nom'
            ])
            ->add('prenom',TextType::class,[
                'attr' => [
                    'class' => 'field util'
                ],
                'label' => 'Prenom'
            ])
            ->add('adresse',TextType::class,[
                'attr' => [
                    'class' => 'field util'
                ],
                'label' => 'Adresse'
            ])
            ->add('telephone',TextType::class,[
                'attr' => [
                    'class' => 'field util'
                ],
                'label' => 'Telephone'
            ])
            ->add('email',EmailType::class,[
                'attr' => [
                    'class' => 'field util'
                ],
                'label' => 'Mail'
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Admin\Client'
        ));
    }
}
