<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'attr' => [
                    'class' => 'field'
                ]
            ])
            ->add('prenom', TextType::class, [
                'attr' => [
                    'class' => 'field'
                ]
            ])
            ->add('username', TextType::class,
                array(
                    'label' => 'Nom d\'utilisateur',
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => [
                        'class' => 'field'
                    ]
                )
            )
            ->add('email', EmailType::class,
                [
                    'label' => 'Email',
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => [
                        'class' => 'field'
                    ]
                ]
            )
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => array(
                    'label' => 'Mot de passe',
                    'attr' => [
                        'class' => 'field'
                    ]
                ),
                'second_options' => array(
                    'label' => 'Veuillez confirmer',
                    'attr' => [
                        'class' => 'field'
                    ]
                )
            ))
            ->add('roles', ChoiceType::class, [
                    'choices' =>
                        [
                            'Administrateur' => 'ROLE_ADMIN',
                            'Vente' => 'ROLE_VENTE',
                            'Comptabilité' => 'ROLE_COMPTA',
                            'Recouvrement' => 'ROLE_RECOUVREMENT',
                        ],
                    'multiple' => true,
                    'expanded' => true,
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
