<?php


namespace App\Manager;


use App\Entity\Vente\Facture;
use App\Entity\Vente\Vente;
use App\Repository\Admin\TiersRepository;
use App\Repository\Admin\TVARepository;
use App\Repository\Vente\FactureRepository;
use App\Services\FactureService;
use App\Services\LigneComptaService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;

class FactureManager extends MyManager
{
    /**
     * @var FactureRepository
     */
    protected $repository;

    /**
     * @var FactureService
     */
    private $service;

    public function __construct(
        FactureRepository $repository,
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
        FactureService $service
    )
    {
        parent::__construct($repository,$entityManager, $logger);
        $this->service = $service;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function generateRef()
    {
        return $this->service->generateRef($this->repository);
    }

    public function groupAnnulatioin(array $factures)
    {

    }

    /**
     * Save facture data to database
     *
     * @param Facture $facture
     * @param TVARepository $TVARepository
     * @param TiersRepository $tiersRepository
     *
     * @throws Exception
     */
    public function saveFacture(Facture $facture, TVARepository $TVARepository, TiersRepository $tiersRepository)
    {
        $refs = $this->generateRef();
        $montant = 0;
        $taxe = 0;
        $montant_fees = 0;

        foreach ($facture->getVentes() as $vente) {
            $montant = $montant + (float)$vente->getTotalMontantTransport();
            $taxe = $taxe + (float)$vente->getTaxe();
            $tmp_montant_fees = $vente->getFeesTotal();
            $montant_fees = $montant_fees + $tmp_montant_fees;
            $vente->setEtat('facture');
        }

        $facture->setReference($refs);

        $autre_prod = 0;

        $facture->setMontantTransport(strval($montant));
        $facture->setMontantTaxe(strval($taxe));
        /*$montant_fees = ($vente->getType() == 'simple')? $vente->getClasses()->getSimple() : $vente->getClasses()->getSimple();*/
        $tva = current($TVARepository->findAll());
        $montant_TVA = (int)$montant_fees * (int)$tva->getTaux() / 100;
        $facture->setMontantFees(strval($montant_fees));
        $facture->setMontantTva(strval($montant_TVA));

        $facture->setTier($tiersRepository->find($facture->getIdLi()));

        $facture->setAutreProd($autre_prod);

        $this->save($facture);

        $lignes = LigneComptaService::fillFactureLigne($facture);

        foreach ($lignes as $ligne) {
            $this->save($ligne);
        }
    }

    /**
     * Annuler une facture
     *
     * @param Facture $facture
     *
     * @throws Exception
     */
    public function annuler(Facture $facture)
    {
        $facture->setAnnule(true);

        $facture->setReference('AN-'.$facture->getReference());

        $this->save($facture);

        $lignes = LigneComptaService::fillAnnulationFacture($facture);

        foreach ($lignes as $ligne) {
            $this->save($ligne);
        }

        $ventes = $facture->getVentes();
        /** @var Vente $item */
        foreach ($ventes->getIterator() as $item) {
            $item->setFacture(null);
            $item->setEtat('non-facture');

            $this->save($item);
        }
    }
}