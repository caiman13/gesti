<?php


namespace App\Manager;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use ReflectionClass;
use stdClass;

abstract class MyManager implements ManagerInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var ServiceEntityRepository
     */
    protected $repository;

    /**
     * MyManager constructor.
     *
     * @param ServiceEntityRepository $repository
     * @param EntityManagerInterface $entityManager
     *
     * @param LoggerInterface $logger
     */
    public function __construct(ServiceEntityRepository $repository, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->repository = $repository;
    }

    /**
     * @param object $entity
     *
     * @return mixed
     * @throws Exception
     */
    public function save($entity)
    {
        try {
            $reflection = new ReflectionClass($entity);
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
            $message = sprintf("%s with id %s Saved with success", $reflection->getShortName(), $entity->getId());
            $this->logger->info($message);

            return $entity->getId();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(),[
                "saved" => "error"
            ]);

            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param object $entity
     *
     * @return boolean
     * @throws Exception
     */
    public function delete($entity)
    {
        try {
            $reflection = new ReflectionClass($entity);
            $this->entityManager->remove($entity);
            $this->entityManager->flush();
            $message = sprintf("%s with id %s deleted with success", $reflection->getShortName(), $entity->getId());
            $this->logger->info($message);

            return true;

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(),[
                "saved" => "error"
            ]);

            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param string $id
     *
     * @return stdClass
     */
    public function getOne(string $id):stdClass
    {
        return $this->repository->find($id);
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param array $criteria
     *
     * @return array
     */
    public function getBy(array $criteria): array
    {
        return $this->repository->findBy($criteria);
    }
}