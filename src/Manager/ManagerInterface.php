<?php


namespace App\Manager;

use stdClass;

interface ManagerInterface
{
    /**
     * @param object $entity
     *
     * @return mixed
     */
    public function save($entity);

    /**
     * @param object $entity
     *
     * @return mixed
     */
    public function delete($entity);

}