<?php


namespace App\Manager;


use App\Entity\Admin\LigneCompta;
use App\Entity\Admin\TauxCom;
use App\Entity\Admin\Tiers;
use App\Entity\Reglement\NoteCredit;
use App\Entity\Vente\Facture;
use App\Entity\Vente\Paiement;
use App\Entity\Vente\PaiementFacture;
use App\Entity\Vente\Scan;
use App\Library\CommFacture;
use App\Repository\Admin\TauxComRepository;
use App\Repository\Vente\PaiementRepository;
use App\Services\AvoirEtNoteService;
use App\Services\FactureService;
use App\Services\LigneComptaService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;

class PaiementManager extends MyManager
{
    /**
     * @var FactureService
     */
    private $factureService;

    public function __construct(
        PaiementRepository $repository,
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
        FactureService $factureService
    ) {
        parent::__construct($repository, $entityManager, $logger);
        $this->factureService = $factureService;
    }

    /**
     * @param Paiement $paiement
     *
     * @throws Exception
     */
    public function annulePaiementFacture(Paiement $paiement): void
    {
        foreach ($paiement->getPaiementFactures() as $paiementFacture) {
            $facture = $paiementFacture->getFacture();
            $facture->setPayed(false);
            $this->save($facture);
        }
    }

    /**
     * set ligne of facture on paiement mode carte for adding commission in facture
     *
     * This method increase also the facture autreProduit by adding the commission of paiement and save the change
     *
     * @param PaiementFacture $paiementFacture
     * @param Tiers $client
     *
     * @throws Exception
     */
    private function saveLignesFactures(PaiementFacture $paiementFacture, Tiers $client)
    {
        $facture = $paiementFacture->getFacture();

        $facture->setAutreProd($paiementFacture->getCommission()+ $facture->getAutreProd());

        $lesLignes = LigneComptaService::setLigneOfPaiementfacture($paiementFacture, $client);

        foreach ($lesLignes as $ligne){
            $this->save($ligne);
        }

        if ($facture->getSolde() <= 0){
            $facture->setPayed(true);
        }

        $this->save($facture);
    }


    /**
     * @param PaiementFacture $paiementFacture
     *
     * @throws Exception
     */
    private function annuleFacturePaiement(PaiementFacture $paiementFacture)
    {
        $facture = $paiementFacture->getFacture();

        $facture->setPayed(false);

        $this->save($facture);
    }

    /**
     * Annuler les ligne compta pour le paiement
     *
     * @param PaiementFacture $paiementFacture
     * @param Tiers $client
     *
     * @throws Exception
     */
    private function annuleLigneFacture(PaiementFacture $paiementFacture, Tiers $client)
    {
        $facture = $paiementFacture->getFacture();
        $facture->setAutreProd($facture->getAutreProd() - $paiementFacture->getCommission());
        $lesLignes = LigneComptaService::setLigneOfPaiementfacture($paiementFacture, $client, true);
        foreach ($lesLignes as $ligne){
            $this->save($ligne);
        }
    }

    /**
     * Annuler toute les données mié aux paiements
     *
     * @param Paiement $paiement
     *
     * @throws Exception
     */
    public function annulePaiement(Paiement $paiement)
    {
        if ($paiement->getMoyenPay()->getId() == 7) {
            $paiement->setMontantRecu($paiement->getAvoir()->getMontant());
            $paiement->getAvoir()->setPayed(false);
        }
        if ($paiement->getMoyenPay()->getId() == 6) {
            $paiement->setMontantRecu($paiement->getNoteCredit()->getMontant());
            $paiement->getNoteCredit()->setPayed(false);
        }

        foreach ($paiement->getPaiementFactures() as $item){
            $this->annuleFacturePaiement($item);
        }

        if ($paiement->getMoyenPay()->getId() == 3) {
            foreach ($paiement->getPaiementFactures() as $item){
                $this->annuleLigneFacture($item, $paiement->getClient());
            }
        }

        $this->annulePaiementFacture($paiement);
    }

    /**
     * This methode is for saving paiement but not for updating
     *
     * @param Paiement $paiement
     * @param string $uploadDirectory
     * @param TauxComRepository $tauxComRepository
     *
     * @param $soldes
     *
     * @throws Exception
     */
    public function savePaiement(Paiement $paiement, $uploadDirectory, TauxComRepository $tauxComRepository, $soldes)
    {
        $files = $paiement->getFileScan();

        if ($paiement->getMoyenPay()->getId() == 7) {
            $paiement->setMontantRecu($paiement->getAvoir()->getMontant());
            $paiement->getAvoir()->setPayed(true);
        }
        if ($paiement->getMoyenPay()->getId() == 6) {
            $paiement->setMontantRecu($paiement->getNoteCredit()->getMontant());
            $paiement->getNoteCredit()->setPayed(true);
        }

        if ($paiement->getMoyenPay()->getId() == 3) {
            $taux = $tauxComRepository->find(1);
            $paiement->setCommission((int)$paiement->getMontantRecu() * $taux->getTaux() / 100);
            foreach ($paiement->getPaiementFactures() as $item) {
                $this->saveLignesFactures($item, $paiement->getClient());
            }
        }

        $this->save($paiement);

        if (($files != 'undefined') && (!is_null($files))) {
            $scan = new Scan();
            $fileName = 'pj_paiement_'.$paiement->getId().'.'.$files->guessExtension();
            $files->move(
                $uploadDirectory,
                $fileName
            );
            $scan->setFile($fileName);
            $this->save($scan);
            $paiement->setScan($scan);
        }

        $factures = $this->checkFactureState($paiement->getPaiementFactures()->toArray());
        //dump($factures);
        $noteCredit = AvoirEtNoteService::createNoteCredit($paiement, $soldes);

        //$this->logger->alert($noteCredit->getMontant());

        if ($noteCredit) {
            $this->save($noteCredit);
            $noteCredit = AvoirEtNoteService::setReferenceNote($noteCredit);
            $this->save($noteCredit);
            $paiement->setRelatedNote($noteCredit);
            $this->save($paiement);
        }
    }

    /**
     * @param PaiementFacture[] $paiementFactures
     *
     * @return Facture[]
     * @throws Exception
     */
    public function checkFactureState(array $paiementFactures)
    {
        $factures = [];
        foreach ($paiementFactures as $paiementFacture) {
            $facture = $paiementFacture->getFacture();
            $facture->addPaiementFacture($paiementFacture);
            $facture->checKSolde();
            $this->save($facture);
            $factures[] = $facture;
        }

        return $factures;
    }

    /**
     * @param Paiement[] $paiements
     *
     * @throws Exception
     */
    public function groupValidation(array $paiements)
    {
        foreach ($paiements as $paiement){
            $paiement->setValide(true);
            $this->save($paiement);
        }
    }

    public function deletePaiement(Paiement $paiement)
    {
        $this->annulePaiement($paiement);
        $this->delete($paiement);
    }
}