<?php


namespace App\Manager;


use App\Entity\Logs;
use App\Entity\User;
use App\Repository\LogsRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class LogsManager extends MyManager
{
    public function __construct(
        LogsRepository $repository,
        EntityManagerInterface $entityManager,
        LoggerInterface $logger
    ) {
        parent::__construct($repository, $entityManager, $logger);
    }

    /**
     * @param User $user
     * @param $debut
     * @param $fin
     *
     * @return Logs[]
     */
    public function getUserAndDate($debut, $fin, User $user = null)
    {
        return $this->repository->getByUserAndDate($debut, $fin, $user);
    }
}
