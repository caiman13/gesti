<?php


namespace App\Services;


use App\Entity\Admin\Tiers;
use App\Entity\Vente\Facture;
use App\Repository\Vente\FactureRepository;

class FactureUtility
{
    /**
     * @var FactureRepository
     */
    private $factureRepository;

    /**
     * FactureUtility constructor.
     *
     * @param FactureRepository $factureRepository
     */
    public function __construct(FactureRepository $factureRepository)
    {
        $this->factureRepository = $factureRepository;
    }

    /**
     * Check si les facture sont du même client
     *
     * @param array $idFactures
     *
     * @return bool
     */
    public function checkFactureCli(array $idFactures)
    {
        $idFacturesCli = $this->factureRepository->getFactureByIds($idFactures);

        $curentItem = current($idFacturesCli);

        foreach ($idFacturesCli as $item){
            if($curentItem['idCli'] !== $item['idCli']){
                return false;
                break;
            }
        }

        return true;
    }

    /**
     * @param Facture[] $factures
     *
     * @return int
     */
    public function getTotalMontant(array $factures)
    {
        $montantTotal = 0;
        foreach ($factures as $facture){
            $montantTotal = $montantTotal + $facture->getTotalMontant();
        }

        return $montantTotal;
    }

    /**
     * @param Facture[] $factures
     *
     * @return Tiers
     */
    public function getCliFromGroupedFacture(array $factures)
    {
        return current($factures)->getTier();
    }
}