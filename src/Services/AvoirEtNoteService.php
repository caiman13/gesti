<?php


namespace App\Services;


use App\Entity\Reglement\Avoir;
use App\Entity\Reglement\NoteCredit;
use App\Entity\Vente\Paiement;
use App\Repository\Reglement\AvoirRepository;
use App\Repository\Reglement\NoteCreditRepository;
use DateTime;

class AvoirEtNoteService
{
    /**
     * @var AvoirRepository
     */
    private $avoirRepository;

    /**
     * @var NoteCreditRepository
     */
    private $noteCreditRepository;

    /**
     * AvoirEtNoteService constructor.
     *
     * @param AvoirRepository $avoirRepository
     * @param NoteCreditRepository $noteCreditRepository
     */
    public function __construct(AvoirRepository $avoirRepository, NoteCreditRepository $noteCreditRepository)
    {
        $this->avoirRepository = $avoirRepository;
        $this->noteCreditRepository = $noteCreditRepository;
    }

    /**
     * Get montant note or avoir
     *
     * @param string $id
     * @param string $type
     *
     * @return float
     */
    public function getMontantAvoirNote(string $id, string $type): float
    {
        $repo = ($type == 'avoir')?$this->avoirRepository: $this->noteCreditRepository;

        $objectGet = $repo->find($id);

        return $objectGet->getMontant();
    }

    /**
     * Create Avoir from paiement and facture
     *
     * @param Paiement $paiement
     * @param $soldes
     *
     * @return NoteCredit|bool
     */
    public static function createNoteCredit(Paiement $paiement, $soldes)
    {
        $montantFactures = $soldes;

        if ($paiement->getMontantTotal() > $montantFactures) {
            $noteCredit = new NoteCredit();
            $noteCredit->setClient($paiement->getClient());
            $noteCredit->setMontant($paiement->getMontantTotal() - $montantFactures);

            return $noteCredit;
        }

        return false;
    }

    public static function setReferenceNote(NoteCredit $noteCredit)
    {
        $today = new DateTime();
        $noteCredit->setReference($noteCredit->getId().'-NOTE/'.$today->format('y'));

        return $noteCredit;
    }
}