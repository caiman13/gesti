<?php


namespace App\Services;


use App\Entity\Admin\LigneCompta;
use App\Entity\Admin\Tiers;
use App\Entity\Vente\Facture;
use App\Entity\Vente\PaiementFacture;
use App\Entity\Vente\Vente;
use App\Repository\Admin\LigneComptaRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;

class LigneComptaService
{
    /**
     * @var LigneComptaRepository
     */
    private $repository;

    /**
     * LigneComptaService constructor.
     *
     * @param LigneComptaRepository $repository
     */
    public function __construct(LigneComptaRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $codeJournal
     * @param DateTime $dateFacture
     * @param string $refFact
     * @param string $numCmp
     * @param int $montant
     * @param string $detail
     * @param string|null $autrePlanCli
     * @param bool $debit
     *
     * @return LigneCompta
     */
    public static function setLignes(
        string $codeJournal,
        DateTime $dateFacture,
        string $refFact,
        string $numCmp,
        int $montant,
        string $detail,
        bool $debit = true,
        ?string $autrePlanCli = null
    ): LigneCompta {
        $ligneCompta = new LigneCompta();
        $ligneCompta->setCodeJournal($codeJournal);
        $ligneCompta->setDateLigne($dateFacture);
        $ligneCompta->setRefFact($refFact);
        $ligneCompta->setNumCmpt($numCmp);
        $ligneCompta->setAutrePlanCli($autrePlanCli);
        if ($debit) {
            $ligneCompta->setMntDebit($montant);
        } else {
            $ligneCompta->setMntCredit($montant);
        }
        $ligneCompta->setDetail($detail);

        return $ligneCompta;
    }

    /**
     * @param Facture $facture
     *
     * @return LigneCompta[]
     */
    public static function fillFactureLigne(Facture $facture): array
    {
        $lesLignes = [];
        /** @var Tiers $tiers */
        $tiers = $facture->getTier();
        if ($tiers->getType() == 'personne') {
            $detail = 'PART '.$facture->getReference().' '.$facture->getTier();
        } else {
            $detail = $facture->getTier().' '.$facture->getReference().' '.$facture->getVentes()->current(
                )->getPassager();
        }

        $code_journal = 'VTE AV';
        $ligne_vente = self::setLignes(
            $code_journal,
            $facture->getDateFacture(),
            $facture->getReference(),
            $facture->getTier()->getNumCompte(),
            $facture->getTotalMontant(),
            $detail,
            true,
            $facture->getTier()->getAuteCompte()
        );

        $lesLignes[] = $ligne_vente;

        /** @var Vente $item */
        foreach ($facture->getVentes() as $item) {
            $ligne = self::setLignes(
                $code_journal,
                $facture->getDateFacture(),
                $facture->getReference(),
                $item->getCompagieAerienne()->getNumCompte(),
                $item->getTotalTarifBCFEES(),
                $detail,
                false
            );
            $lesLignes[] = $ligne;
        }
        $ligne_vente1 = self::setLignes(
            $code_journal,
            $facture->getDateFacture(),
            $facture->getReference(),
            '70810000',
            (int)$facture->getMontantFees(),
            $detail,
            false
        );

        $lesLignes[] = $ligne_vente1;

        $ligne_vente2 = self::setLignes(
            $code_journal,
            $facture->getDateFacture(),
            $facture->getReference(),
            '44571000',
            (int)$facture->getMontantTva(),
            $detail,
            false
        );

        $lesLignes[] = $ligne_vente2;

        if ($facture->getAutreProd() != 0) {
            $ligne_vente3 = self::setLignes(
                $code_journal,
                $facture->getDateFacture(),
                $facture->getReference(),
                '76800000',
                (int)$facture->getAutreProd(),
                $detail,
                false
            );
            $lesLignes[] = $ligne_vente3;
        }

        return $lesLignes;
    }

    /**
     * @param Facture $facture
     *
     * @return array
     * @throws Exception
     */
    public static function fillAnnulationFacture(Facture $facture): array
    {
        /** @var ArrayCollection $lesVentes */
        $lesVentes = $facture->getVentes();

        $detail = 'ANN ';
        if ($facture->getTier()->getType() == 'personne') {
            $detail = $detail.'PART '.$facture->getReference().' '.$facture->getTier();
        } else {
            $detail = $detail.$facture->getTier().' '.$facture->getReference().' '.$facture->getVentes()->current(
                )->getPassager();
        }

        $code_journal = 'VTE AV';
        $ligne_vente = self::setLignes(
            $code_journal,
            $facture->getDateFacture(),
            $facture->getReference(),
            $facture->getTier()->getNumCompte(),
            $facture->getTotalMontant(),
            $detail,
            false,
            $facture->getTier()->getAuteCompte()
        );

        $lesLignes[] = $ligne_vente;

        /** @var Vente $item */
        foreach ($facture->getVentes() as $item) {
            $ligne = self::setLignes(
                $code_journal,
                $facture->getDateFacture(),
                $facture->getReference(),
                $item->getCompagieAerienne()->getNumCompte(),
                $item->getTotalTarifBCFEES(),
                $detail
            );
            $lesLignes[] = $ligne;
        }
        $ligne_vente1 = self::setLignes(
            $code_journal,
            $facture->getDateFacture(),
            $facture->getReference(),
            '70810000',
            (int)$facture->getMontantFees(),
            $detail
        );

        $lesLignes[] = $ligne_vente1;

        $ligne_vente2 = self::setLignes(
            $code_journal,
            $facture->getDateFacture(),
            $facture->getReference(),
            '44571000',
            (int)$facture->getMontantTva(),
            $detail
        );

        $lesLignes[] = $ligne_vente2;

        if ($facture->getAutreProd() != 0) {
            $ligne_vente3 = self::setLignes(
                $code_journal,
                $facture->getDateFacture(),
                $facture->getReference(),
                '76800000',
                (int)$facture->getAutreProd(),
                $detail
            );
            $lesLignes[] = $ligne_vente3;
        }

        return $lesLignes;
    }

    /**
     * @param PaiementFacture $paiementFacture
     * @param Tiers $client
     * @param bool $annule
     *
     * @return LigneCompta[]
     */
    public static function setLigneOfPaiementfacture(PaiementFacture $paiementFacture, Tiers $client, $annule = false)
    {
        $facture = $paiementFacture->getFacture();
        //dump($facture->getVentes()->current()); die;
        /*($facture); die;*/
        if ($client->getType() == 'personne') {
            $detail = 'PART '.$facture->getReference().' '.$client;
        } else {
            $detail = $client.' '.$facture->getReference().' '.$facture->getVentes()->current()->getPassager();
        }

        $lesLignes = [];

        $ligne_vente = self::setLignes(
            'VTE',
            $facture->getDateFacture(),
            $facture->getReference(),
            $facture->getTier()->getNumCompte(),
            (int)$paiementFacture->getCommission(),
            $detail,
            !$annule,
            $client->getAuteCompte()
        );

        $lesLignes[] = $ligne_vente;

        $ligne_vente3 = self::setLignes(
            'VTE',
            $facture->getDateFacture(),
            $facture->getReference(),
            '76800000',
            (int)$paiementFacture->getCommission(),
            $detail,
            $annule
        );

        $lesLignes[] = $ligne_vente3;

        return $lesLignes;
    }
}