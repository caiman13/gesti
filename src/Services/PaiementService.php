<?php


namespace App\Services;


use App\Entity\Admin\Client;
use App\Entity\Vente\Facture;
use App\Entity\Vente\Paiement;
use App\Entity\Vente\PaiementFacture;
use App\Library\CommFacture;

class PaiementService
{
    /**
     * @param Facture[] $factures
     * @param FactureService $factureService
     *
     * @return Paiement
     */
    public static function buildPaiement(array $factures, FactureService $factureService) : Paiement
    {
        $paiement = new Paiement();

        $paiement->setMontantRecu($factureService->getTotalMontant($factures));

        $paiement->setClient($factureService->getCliFromGroupedFacture($factures));

        foreach ($factures as $facture){
            $facturePaiement = new PaiementFacture();
            $facturePaiement->setMontant($facture->getSolde());
            $facturePaiement->setFacture($facture);
            $facturePaiement->setFactureref();
            $paiement->addPaiementFacture($facturePaiement);
        }

        return $paiement;
    }

    /**
     * @param Paiement $paiement
     *
     * @return CommFacture[]|array
     */
    public static function generateOldPaiementFacture(Paiement $paiement) : array
    {
        $paiementactures = $paiement->getPaiementFactures();

        $commFactures = [];

        foreach ($paiementactures as $paiementFacture){
            $facture = $paiementFacture->getFacture();
            $comFact = new CommFacture($facture->getReference(), $paiementFacture->getCommission());

            $commFactures[] = $comFact;
        }

        return $commFactures;
    }

    /**
     * @param Paiement $paiement
     *
     * @return Paiement
     */
    public static function setFacturesRef(Paiement $paiement)
    {
        foreach ($paiement->getPaiementFactures() as $paiementFacture) {
            $paiementFacture->setFactureref();
        }

        return $paiement;
    }

    /**
     *
     * Get total payement from $paiementsFactures
     *
     * @param Paiement $paiement
     *
     * @return float|int
     */
    public static function getTotalMontantPayedFactures(Paiement $paiement)
    {
        $montant = 0;
        foreach ($paiement->getPaiementFactures() as $paiementFacture) {
            $montant += $paiementFacture->getTotalPaiement();
        }

        return $montant;
    }

    /**
     * @param Paiement $paiement
     *
     * @return array|Facture[]
     */
    public static function getFactureByPaiements(Paiement $paiement)
    {
        $factures = [];
        foreach ($paiement->getPaiementFactures() as $paiementFacture) {
            $factures[] = $paiementFacture->getFacture();
        }

        return $factures;
    }

}