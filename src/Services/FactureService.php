<?php


namespace App\Services;


use App\Entity\Admin\Tiers;
use App\Entity\Vente\Facture;
use App\Entity\Vente\Paiement;
use App\Entity\Vente\PaiementFacture;
use App\Repository\Vente\FactureRepository;
use DateTime;
use Exception;

class FactureService
{
    /**
     * @var FactureRepository
     */
    private $factureRepository;

    /**
     * FactureUtility constructor.
     *
     * @param FactureRepository $factureRepository
     */
    public function __construct(FactureRepository $factureRepository)
    {
        $this->factureRepository = $factureRepository;
    }

    /**
     * Check si les facture sont du même client
     *
     * @param array $idFactures
     *
     * @return bool
     */
    public function checkFactureCli(array $idFactures)
    {
        $idFacturesCli = $this->factureRepository->getFactureByIds($idFactures);

        $curentItem = current($idFacturesCli);

        foreach ($idFacturesCli as $item){
            if($curentItem['idCli'] !== $item['idCli']){
                return false;
                break;
            }
        }

        return true;
    }

    /**
     * @param Facture[] $factures
     *
     * @param bool $solde
     * @param bool $commission
     *
     * @return int
     */
    public function getTotalMontant(array $factures, $solde = true, $commission = false)
    {
        $montantTotal = 0;
        foreach ($factures as $facture) {
            if ($solde) {
                $montantTotal = $montantTotal + $facture->getSolde($commission);
            } else {
                $montantTotal = $montantTotal + $facture->getTotalMontant();
            }
        }

        return $montantTotal;
    }

    /**
     * @param Facture[] $factures
     *
     * @return Tiers
     */
    public function getCliFromGroupedFacture(array $factures)
    {
        return current($factures)->getTier();
    }

    /**
     * @param Facture[] $factures
     * @param bool $commission
     *
     * @return int
     */
    public static function getTotalPaye(array $factures, $commission = true)
    {
        $montant = 0;
        foreach ($factures as $facture) {
            $montant = $montant + $facture->getTotalPaiement($commission);
        }

        return $montant;
    }

    /**
     * @param FactureRepository $repository
     *
     * @return string
     * @throws Exception
     */
    public function generateRef(FactureRepository $repository)
    {
        $now = new DateTime();
        $sufixref = '/HSMV/' . $now->format('y');


        $lastRef = $repository->getLastReference();
        if ($lastRef == false) {
            $reference = '0001' . $sufixref;
        } else {
            $bout = substr($lastRef['reference'], 0, 4);
            $int_ext = (int)$bout + 1;

            $numfinal = str_pad($int_ext, 4, "0", STR_PAD_LEFT);

            $reference = $numfinal.$sufixref;
        }

        return $reference;
    }

    /**
     * @param Paiement $paiement
     *
     * @return float|int
     */
    public static function getSoldeFacturesByPaiement(Paiement $paiement)
    {
        $montant = 0;
        foreach ($paiement->getPaiementFactures() as $paiementFacture) {
            $montant += $paiementFacture->getTotalPaiement();
        }

        return $montant;
    }
}